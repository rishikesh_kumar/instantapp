package net.android.volley;

import android.content.Context;

import net.android.volley.toolbox.HttpHeaderParser;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by saurabhmourya on 15/9/16.
 */
public class VolleyMultipartRequest extends Request<String> {
    private final Response.Listener<String> mListener;
    private final Response.ErrorListener mErrorListener;
    private final String BOUNDARY = "WebKitFormBoundarynrHfHHBqL64QHPve";
    private final String mMimeType = "multipart/form-data; boundary=" + BOUNDARY;
    private Map<String, String> headerPart;
    private Context context;
    private final int MY_SOCKET_TIMEOUT_MS = 50000;
    private Map<String, File> file;
    private Map<String, String> mStringPart;
    private final String twoHyphens = "--";
    private final String lineEnd = "\r\n";
    private final String boundary = "WebKitFormBoundarynrHfHHBqL64QHPve";
    DataOutputStream dataOutputStream;

    public VolleyMultipartRequest(Context context, String url, Response.ErrorListener errorListener, Response.Listener<String> listener, Map<String, File> file, Map<String, String> mStringPart, Map<String, String> mHeaderPart) {
        super(Method.POST, url, errorListener);
        this.mListener = listener;
        this.mErrorListener = errorListener;
        this.context = context;
        this.file = file;
        this.mStringPart = mStringPart;
        this.headerPart = mHeaderPart;

        setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public VolleyMultipartRequest(Context context, int method, String url, Response.ErrorListener errorListener, Response.Listener<String> listener, Map<String, File> file, Map<String, String> mStringPart) {
        super(method, url, errorListener);
        this.mListener = listener;
        this.mErrorListener = errorListener;
        this.context = context;
        this.file = file;
        this.mStringPart = mStringPart;

        setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

   /* @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return (mHeaders != null) ? mHeaders : super.getHeaders();
    }*/

    @Override
    public String getBodyContentType() {
        return mMimeType;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        dataOutputStream = new DataOutputStream(bos);
        try {
            for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
                buildPart(entry.getKey(), entry.getValue());
            }
            for (Map.Entry<String, File> f : file.entrySet()) {
                addFilePart(f.getKey(), f.getValue());
            }
            dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bos.toByteArray();
    }

    private void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        PrintWriter writer;
        writer = new PrintWriter(dataOutputStream,
                true);
        String fileName = "";
        if (uploadFile != null) fileName = uploadFile.getName();
        writer.append("--" + BOUNDARY).append(lineEnd);
        writer.append(
                "Content-Disposition: form-data; name=\"" + fieldName
                        + "\"; filename=\"" + fileName + "\"")
                .append(lineEnd);
        writer.append(
                "Content-Type: "
                        + URLConnection.guessContentTypeFromName(fileName))
                .append(lineEnd);
        writer.append("Content-Transfer-Encoding: binary").append(lineEnd);
        writer.append(lineEnd);
        writer.flush();

        if (uploadFile != null) {
            FileInputStream inputStream = new FileInputStream(uploadFile);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                dataOutputStream.write(buffer, 0, bytesRead);
            }
            dataOutputStream.flush();
            inputStream.close();
        }

        writer.append(lineEnd);
        writer.flush();
    }

    private void buildPart(String parameterName, String parameterValue) throws IOException {
        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + parameterName + "\"" + lineEnd);
        dataOutputStream.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
        dataOutputStream.writeBytes(lineEnd);
        dataOutputStream.writeBytes(parameterValue + lineEnd);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            if (response != null && response.statusCode == 410) {
                return Response.error(new VolleyError(response, response.statusCode+""));
            }
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(json, getCacheEntry());

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (Exception e) {
            return Response.error(new VolleyError(response));
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = this.headerPart;
        if (headers == null || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }
        headers.put("Connection", "keep-alive");
        headers.put("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

        return headers;
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

    @Override
    public void deliverError(VolleyError error) {
        mErrorListener.onErrorResponse(error);
    }
}

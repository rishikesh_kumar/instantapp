package net.one97.paytm;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.StatFs;


import java.io.File;

/**
 * Created by Rama on 6/16/2016.
 */
public class DeviceUtils {

    private static final int MIN_DISK_CACHE_SIZE = 30 * 1024 * 1024; // 30 MB
    private static final int MAX_DISK_CACHE_SIZE = 100 * 1024 * 1024; // 100 MB

    public static long diskCacheSizeBytes(File dir, long minSize) {
        long size = minSize;
        try {
            StatFs statFs = new StatFs(dir.getAbsolutePath());
            long availableBytes = ((long) statFs.getBlockCount()) * statFs.getBlockSize();
            size = availableBytes / 50;
        } catch (IllegalArgumentException e) {
        }

        // Bound inside min/max size for disk cache.
        return Math.max(Math.min(size, MAX_DISK_CACHE_SIZE), MIN_DISK_CACHE_SIZE);
    }

    public static boolean isPackageInstalled(String packagename, Context context) {
        boolean isInstall = false;
        final PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(packagename,
                    PackageManager.GET_ACTIVITIES);
            isInstall = true;
        } catch (PackageManager.NameNotFoundException e) {
            isInstall = false;
        }

        return isInstall;
    }

}

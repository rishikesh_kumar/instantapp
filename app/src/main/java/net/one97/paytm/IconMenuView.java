package net.one97.paytm;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class IconMenuView extends RelativeLayout {

    private RelativeLayout mMainLayout;
    private ImageView mImageView;
    private OnIconMenuClickListener mIconMenuClickListener;

    public interface OnIconMenuClickListener {
        void onIconMenuClick(View view);
    }

    public IconMenuView(Context context) {
        super(context);
        init(context);
    }

    public IconMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public IconMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.lyt_icon_menu, this, true);

        mMainLayout = (RelativeLayout) findViewById(R.id.lyt_icon_menu);
        mMainLayout.setClickable(true);
        mMainLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                mIconMenuClickListener.onIconMenuClick(view);
            }
        });

        mImageView = (ImageView) findViewById(R.id.icon_menu);
        mImageView.setImageResource(R.drawable.wallet_deals_icon);
    }

    public void setIconMenuVisibility(int visibility) {
        mMainLayout.setVisibility(visibility);
    }

    public void setImageIconResource(int resId) {
        mImageView.setImageResource(resId);
    }

    public void setClickable(boolean isClickable) {
        mMainLayout.setClickable(isClickable);
    }


    public void setIconMenuViewClickListener(OnIconMenuClickListener listener) {
        mIconMenuClickListener = listener;

    }
}
package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

/**
 * Created by rohitraj on 4/7/16.
 */

public class CJRUser {

    @SerializedName("user_id")
    private int userID;

    @SerializedName("ga_id")
    private int gaID;

    @SerializedName("experiment_id")
    private String experimentID;


    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getGaID() {
        return gaID;
    }

    public void setGaID(int gaID) {
        this.gaID = gaID;
    }

    public String getExperimentID() {
        return experimentID;
    }

    public void setExperimentID(String experimentID) {
        this.experimentID = experimentID;
    }
}

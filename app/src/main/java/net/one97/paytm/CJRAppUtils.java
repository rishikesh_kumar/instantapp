/**
 * Copyright C 2012, One97 Communications Ltd. All rights reserved.
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CJRAppUtils {

    private static Context mContext;

    private static String siteId = null;

    private static String childSiteId = null;

    public static String brandTag = null;

    public static int tabCurrentPosition = -1;

    public static void setContext(Context context)
    {
        mContext= context;
    }

    public static Context getContext()
    {
       return mContext;
    }

    public static String formatNumber(String number) {
        if (number != null && !number.trim().equalsIgnoreCase("")) {
            NumberFormat formatter = new DecimalFormat("##,##,##,##,###.##");
            float floatNumber = Float.parseFloat(number);
            return formatter.format(floatNumber);
        }
        return "";
    }

    public static long convertDurationToSeconds(String inDuration) {
        long time_in_seconds = 0;
        int ihours = 0, iMinute = 0;
        if (inDuration != null && !inDuration.trim().equalsIgnoreCase("")) {
            ihours = Integer.parseInt((inDuration.substring(0, inDuration.lastIndexOf("h"))).trim());
            iMinute = Integer.parseInt((inDuration.substring(inDuration.lastIndexOf("h") + 1, inDuration.lastIndexOf("m"))).trim());
            time_in_seconds = ihours * 60 * 60 + iMinute * 60;
        }
        return time_in_seconds;
    }

    public static String convertSecondsToDuration(long inTimeInSec) {
        String Duration = "";
        int ihours = 0;
        int iminute = 0;
        if (inTimeInSec >= 3600) {
            ihours = (int) inTimeInSec / 3600;
        }
        iminute = ((int) inTimeInSec % 3600) / 60;

        if (ihours > 0)
            Duration = ihours + "h";
        else
            Duration = "0h";

        Duration += iminute + "m";

        return Duration;
    }

    private static String priceWithDecimal(Double price) {
        DecimalFormat formatter = new DecimalFormat("##,##,##,##,###.00");
        return formatter.format(price);
    }

    private static String priceWithoutDecimal(Double price) {
        DecimalFormat formatter = new DecimalFormat("##,##,##,##,###.##");
        return formatter.format(price);
    }

    public static String priceToString(String priceStr) {
        try {
            double price = Double.parseDouble(priceStr);
            return priceToString(price);
        } catch (NumberFormatException e) {
            return "";
        }
    }

    public static String priceToString(Double price) {
        String toShow = priceWithoutDecimal(price);
        if (toShow.indexOf(".") > 0) {
            return (price < 1 && price > 0) ? "0" + priceWithDecimal(price) : priceWithDecimal(price);
        } else {
            return (price < 1 && price > 0) ? "0" + priceWithoutDecimal(price) : priceWithoutDecimal(price);
        }
    }

    /**
     * This method converts dp unit to equivalent device specific value in pixels.
     *
     * @param dp      A value in dp(Device independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return An integer value to represent Pixels equivalent to dp according to device
     */
    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = (int) (dp * (metrics.densityDpi / 160f));
        return px;
    }

    public static boolean isValidPassport(String s) {
        if (TextUtils.isEmpty(s))
            return false;

        return s.matches("(([a-zA-Z]{1})\\d{7})");
    }

    public static boolean isValidPAN(String s) {
        if (TextUtils.isEmpty(s))
            return false;

        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");

        Matcher matcher = pattern.matcher(s);
        // Check if pattern matches
        return matcher.matches();

    }

    public static boolean isValidPANPersonal(String s) {
        if (TextUtils.isEmpty(s))
            return false;

        Pattern pattern = Pattern.compile("[A-Z]{3}[P]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}");

        Matcher matcher = pattern.matcher(s);
        // Check if pattern matches
        return matcher.matches();

    }

    public static boolean isValidEUIN(String text) {
        boolean digitsOnly = true;
        String startingString = text.substring(0, 1);
        String isNumber = text.substring(1, text.length());
        if (TextUtils.isEmpty(startingString) || TextUtils.isEmpty(isNumber)) {
            digitsOnly = false;
        } else if (!startingString.equalsIgnoreCase("E")) {
            digitsOnly = false;
        } else if (!TextUtils.isDigitsOnly(isNumber)) {
            digitsOnly = false;
        }
        return digitsOnly;
    }

    public static String getSiteId() {
        if(siteId == null) {
            return "1";
        }
        return siteId;
    }

    public static void setSiteId(String siteId) {
        CJRAppUtils.siteId = siteId;
    }

    public static String getChildSiteId() {
        if(childSiteId == null) {
            return "1";
        }
        return childSiteId;
    }

    public static void setChildSiteId(String childSiteId) {
        CJRAppUtils.childSiteId = childSiteId;
    }

    public static int getTabCurrentPosition() {
        return tabCurrentPosition;
    }

    public static void setTabCurrentPosition(int tabCurrentPosition) {
        CJRAppUtils.tabCurrentPosition = tabCurrentPosition;
    }

    public static String getBrandTag() {
        return brandTag;
    }

    public static void setBrandTag(String brandTag) {
        CJRAppUtils.brandTag = brandTag;
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null && activity.getCurrentFocus() != null &&
                    activity.getCurrentFocus().getWindowToken() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public static String getOrderFormattedDate(String dateTime) {
        if (dateTime != null && dateTime.trim().length() > 0) {
            String finalFormat = null;
            SimpleDateFormat SDF = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SDF.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            try {
                date = SDF.parse(dateTime);
            } catch (ParseException e) {
                e.printStackTrace();
            } // from String to date
            SimpleDateFormat S = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
            S.setTimeZone(TimeZone.getDefault());
            finalFormat = S.format(date);

            if (!TextUtils.isEmpty(finalFormat)) {
                return finalFormat.replace("-", " ");
            } else {
                return dateTime;
            }
        } else {
            // instead of returning null, return empty value.
            return "";
        }
    }


    /**
     * Checks if the device is rooted.
     *
     * @return <code>true</code> if the device is rooted, <code>false</code> otherwise.
     */
    public static boolean isRooted() {

        // get from build info
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }

        // check if /system/app/Superuser.apk is present
        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        // try executing commands
        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }

    // executes a command on the system
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }

}

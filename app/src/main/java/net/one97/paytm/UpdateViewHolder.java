package net.one97.paytm;

import android.content.Context;

/**
 * Created by Rama on 4/25/2016.
 */
public interface UpdateViewHolder {
    void updateViewHolder(Context context, CJRHomePageLayoutV2 dataModel, String mGAKey);
}

package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

/**
 * Created by rohitraj on 29/09/16.
 */
public class CJRCategoryTree implements IJRDataModel {

    public String getMurl() {
        return murl;
    }

    public void setMurl(String murl) {
        this.murl = murl;
    }

    @SerializedName("url")
    private String murl;
}

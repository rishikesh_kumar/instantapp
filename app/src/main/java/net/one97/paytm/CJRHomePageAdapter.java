package net.one97.paytm;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;


public class CJRHomePageAdapter extends FragmentStatePagerAdapter implements CJRPagerSlidingTabStrip.IconUrlTabProvider {

    CJRHomePageItem mRefData;
    FragmentManager supportFragmentManager;
    CJRCatalog mCatalog;
    SparseArray<Fragment> mPageReferences;
    private String mPushRichPageType = null;
    private String mPushRichPageId = null;
    private Context mContext;
    private HomeTabItem[] mHomeTabItems;

    public enum BottomTabs {
        HOME(0), MALL(1), QR(2), PROFILE(3), NOTIFICATION(4);
        private int tabPosition;

        BottomTabs(int tabPosition) {
            this.tabPosition = tabPosition;
        }

        public int getTabPosition() {
            return tabPosition;
        }
    }

    public CJRHomePageAdapter(Context mContext, FragmentManager supportFragmentManager,
                              CJRHomePageItem mRefData, CJRCatalog mCatalog,
                              String pushRichPageType, String pushRichPageId,
                              HomeTabItem [] homeTabItems) {
        super(supportFragmentManager);
        this.mContext = mContext;
        this.supportFragmentManager = supportFragmentManager;
        this.mRefData = mRefData;
        this.mCatalog = mCatalog;
        this.mPushRichPageType = pushRichPageType;
        this.mPushRichPageId = pushRichPageId;
        this.mHomeTabItems = homeTabItems;
        mPageReferences = new SparseArray<Fragment>();

    }

    @Override
    public int getCount() {
        return mHomeTabItems.length;
    }


    @Override
    public String getPageIconUrl(int position) {
        return "";
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (CharSequence) mHomeTabItems[position].getText();
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("CACHED_CATALOG_DATA", mCatalog);

        HomeTabItem item = mHomeTabItems[position];


        Fragment fragment = item.getFragment(bundle);
        //mPageReferences.put(position, fragment);
        return fragment;

    }


    public void updateAllFragments() {
        ActivityToFragmentListener fragment = null;
        for (int i = 0; i < mPageReferences.size(); i++) {
            fragment = (ActivityToFragmentListener) mPageReferences.get(i);
            fragment.updateFragment(true);
        }
    }

    /**
     * update specific fragment if any data specific to fragment is changed
     *
     * @param position
     */
    public void updateSpecificFragment(BottomTabs position) {
        if (mPageReferences != null && mPageReferences.size() > 0) {
            ActivityToFragmentListener fragment = (ActivityToFragmentListener) mPageReferences.get(position.getTabPosition());
            fragment.updateHeading();
        }
    }

    public Fragment getFragment(int key) {
        return mPageReferences.get(key);
    }

    public SparseArray<Fragment> getLiveFragments() {
        return mPageReferences;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        mPageReferences.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mPageReferences.remove(position);
        super.destroyItem(container, position, object);
    }
}

package net.one97.paytm;

import android.app.Activity;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tusharsharma on 14/08/17.
 */

public class BrandProductListAdapter extends CJRBaseAdapter {

    private ArrayList<CJRHomePageItem> mItemList;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private String mParent;
    private String mScreenName;
    private String mcontainerInstanceId;
    private String mchildSiteId;
    private String layoutType;
    private boolean isSendViewEvent = false;
    private int mViewType;
    private HashMap<String, Object> mSearchMap = new HashMap<>();
    private String msiteId = CJRAppUtils.getChildSiteId();
    private int mCount;
    private static float SECOND_LINE_CUTOFF = 10000;

    ArrayList<String> viewedItemIds = new ArrayList<>();


    public BrandProductListAdapter(Activity activity, ArrayList<CJRHomePageItem> itemList,
                                   String layout, String parent, String screenName, String
                                         containerInstanceID, String mchildSiteId, int viewType) {
        super();
        mItemList = itemList;
        mInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mParent = parent;
        mScreenName = screenName;
        mcontainerInstanceId = containerInstanceID;
        this.mchildSiteId = mchildSiteId;
        layoutType = layout;
        mViewType = viewType;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).hashCode();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CJRHomePageItem item = mItemList.get(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.lyt_store_product_row_item, null);
            BrandProductListAdapter.ViewHolder holder = new BrandProductListAdapter.ViewHolder();
            holder.productImage = (ImageView) convertView.findViewById(R.id.brand_row_product_image);
            holder.productName = (RoboTextView) convertView.findViewById(R.id.brand_product_name);
            holder.productDiscPrice = (RoboTextView) convertView.findViewById(R.id.brand_product_disc_price);
            holder.productPrice = (RoboTextView) convertView.findViewById(R.id.brand_product_price) ;
            holder.productDiscount = (RoboTextView) convertView.findViewById(R.id.brand_product_discount);
            holder.offerTag = (RoboTextView) convertView.findViewById(R.id.offer_tag);
            holder.productPrice2 = (RoboTextView) convertView.findViewById(R.id.brand_product_price_secondary);
            holder.productDiscount2 = (RoboTextView) convertView.findViewById(R.id.brand_product_discount_secondary);
            holder.secondaryPriceLyt = (LinearLayout) convertView.findViewById(R.id.lyt_secondary_txt) ;
            convertView.setTag(holder);
        }
        final BrandProductListAdapter.ViewHolder holder = (BrandProductListAdapter.ViewHolder) convertView.getTag();

        mItemList.get(position).setParentItem(mParent);

//        if (!mItemList.get(position).isItemViewed()) {
//            mItemList.get(position).setItemViewed();
//            if (mcontainerInstanceId != null)
//                mItemList.get(position).setmContainerInstanceID(mcontainerInstanceId);
//            else mItemList.get(position).setmContainerInstanceID("");
//
//
//            if(mchildSiteId!=null){
//
//                String slotType,viewIdentifier;
//                if(CJRAppUtils.getTabCurrentPosition()==0) slotType = "HS2VS1";
//                slotType = "HS2VS2";
//                viewIdentifier = CJRAppUtils.getBrandTag()+"_"+slotType+"_"+layoutType;
//                CJRSendGTMTag.sendBrandPromotionImpression(mItemList.get(position), mActivity, position,"/BrandStore", mchildSiteId,viewIdentifier);
//
//            }
//            else {
//                if (isSendViewEvent) {
//
//                    if (!viewedItemIds.contains(cjrHomePageItem.getItemID())) {
//                        viewedItemIds.add(cjrHomePageItem.getItemID());
////                        CJRSendGTMTag.sendPromotionImpression(mItemList.get(position), mActivity, position,
////                                mScreenName);
//
//                        CJRSendGTMTag.sendImpressionEventForWidgets(mItemList.get(position), position,
//                                layoutType, mchildSiteId, mActivity, mScreenName, "carousel-" + mViewType);
//                    }
//                }
//            }
//        }

        if(item != null) {
            Picasso.with(mActivity)
                    .load(item.getImageUrl())
                    .into(holder.productImage);

            holder.productName.setText(item.getName());
            holder.productDiscPrice.setText(item.getFormatedOfferPrice());
            holder.productPrice.setText(item.getFormatedActualPrice());
            holder.productDiscount.setText(item.getDiscountPercent());


            holder.productPrice.setPaintFlags(holder.productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.productPrice2.setPaintFlags(holder.productPrice2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if(item.getOfferTag() != null) {
                holder.offerTag.setVisibility(View.VISIBLE);
                holder.offerTag.setText(item.getOfferTag());
            } else {
                holder.offerTag.setVisibility(View.INVISIBLE);
            }

            if (item.getDiscountPercent() != null) {
                holder.productPrice.setVisibility(View.VISIBLE);
                holder.productDiscount.setVisibility(View.VISIBLE);
                if (item.getDiscountPercent().equals("0") || item.getDiscountPercent().equals("NaN")) {
                    holder.productPrice.setVisibility(View.GONE);
                    holder.productDiscount.setVisibility(View.GONE);
                } else {
                    if(item.getActualPrice() > SECOND_LINE_CUTOFF) {
                        holder.productPrice.setVisibility(View.GONE);
                        holder.productDiscount.setVisibility(View.GONE);
                        holder.secondaryPriceLyt.setVisibility(View.VISIBLE);
                        holder.productDiscount2.setVisibility(View.VISIBLE);
                        holder.productPrice2.setVisibility(View.VISIBLE);
                        holder.productDiscount2.setText("-" + item.getDiscountPercent() + "%");
                        if (item.getFormatedActualPrice() != null) {
                            if (item.getFormatedActualPrice().equalsIgnoreCase("Free")) {
                                holder.productPrice2.setText(item.getFormatedActualPrice());
                            } else {
                                holder.productPrice2.setText(mActivity
                                        .getString(R.string
                                                .grid_rs) + item.getFormatedActualPrice());
                            }
                        }
                    }
                    else {
                        holder.secondaryPriceLyt.setVisibility(View.GONE);
                        holder.productDiscount2.setVisibility(View.GONE);
                        holder.productPrice2.setVisibility(View.GONE);
                        holder.productDiscount.setText("-" + item.getDiscountPercent() + "%");
                        if (item.getFormatedActualPrice() != null) {
                            if (item.getFormatedActualPrice().equalsIgnoreCase("Free")) {
                                holder.productPrice.setText(item.getFormatedActualPrice());
                            } else {
                                holder.productPrice.setText(mActivity
                                        .getString(R.string
                                                .grid_rs) + item.getFormatedActualPrice());
                            }
                        }
                    }
                }
            }

            if (item.getFormatedOfferPrice() != null) {
                if (item.getFormatedOfferPrice().equalsIgnoreCase("Free")) {
                    holder.productDiscPrice.setText(item.getFormatedOfferPrice());
                } else {
                    holder.productDiscPrice.setText(mActivity.getString(R.string
                            .grid_rs) + item.getFormatedOfferPrice());
                }
            }

            if (item != null && !item.isItemViewed()) {
                item.setItemViewed();
                sendProductImpressionEvent(position);
            }
        }
        return convertView;
    }

    public void setItemList(ArrayList<CJRHomePageItem> itemList, String layout){
        mItemList = itemList;
        layoutType = layout;
        notifyDataSetChanged();
    }

    public ArrayList<CJRHomePageItem> getData() {
        return mItemList;
    }

    public void clearData() {
        if (mItemList != null) {
            mItemList.clear();
            notifyDataSetChanged();
        }
    }

    public static class ViewHolder {
        ImageView productImage;
        RoboTextView productName;
        RoboTextView productDiscPrice;
        RoboTextView productPrice;
        RoboTextView productDiscount;
        RoboTextView offerTag;
        LinearLayout secondaryPriceLyt;
        RoboTextView productPrice2;
        RoboTextView productDiscount2;
    }

    public void setSendViewEvent(boolean viewEvent) {
        this.isSendViewEvent = viewEvent;
    }

    public void sendProductImpressionEvent(int position) {
        try {
            ArrayList<CJRHomePageItem> Products = new ArrayList<>();
            Products.add(mItemList.get(position));
            if (Products.size() > 0) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

public class CJRIllegalCodeError implements IJRDataModel{


	private static final long serialVersionUID = 1L;

	@SerializedName("error_title")
	private String mErrorTitle;
	
	@SerializedName("error")
	private String mError;


	@SerializedName("errorCode")
	private String errorCode;

	@SerializedName("code")
	private String code;

	public String getCode()
	{
		return code;
	}

	public String getErrorCode()
	{
		return errorCode;
	}


	public String getmErrorTitle()
	{
		return mErrorTitle;
	}


	public String getError() {
		return mError;
	}

	
	

}

package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rama on 4/25/2016.
 */
public class WalletTopbarViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder, AdapterView.OnItemClickListener {

    private final CJRHorizontalListView listView;
    private final TextView title;
    private final View dummyView;

    private Context mContext;
    private CJRHomePageLayoutV2 cjrHomePageLayoutV2;
    private ArrayList<String> walletItemList = new ArrayList<String>();
    private LinearLayout walletStaticheaderoptions;
    //***************Staic wallet-header  *************
    RelativeLayout mLytRequest;
    RelativeLayout mLytPay;
    RelativeLayout mLytAddMoney;
    RelativeLayout mLytPassbook;
    private TextView mTvTip;

    public WalletTopbarViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext = mContext;

        mTvTip = (TextView) convertView.findViewById(R.id.top_bar_wallet_options_tv);
        title = (TextView) convertView.findViewById(R.id
                .txt_horizontal_links_title);
        listView = (CJRHorizontalListView) convertView.findViewById(R
                .id.horizontal_text_list);
        dummyView = convertView.findViewById(R
                .id.divider_view);
        walletStaticheaderoptions = (LinearLayout) convertView.findViewById(R.id.static_wallet_options);
        walletItemList.clear();


        mTvTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        if (WalletUtilities.isPayTabVisible(mContext)) {
            walletItemList.add("pay");
        }

        walletItemList.add("postcard");
        if (WalletUtilities.isSavingAccVisible(mContext)) {
            walletItemList.add("savings_account");
        }
        walletItemList.add("passbook");

        walletItemList.add("add");


        if (WalletUtilities.isMoneyTransferTabVisible(mContext)) {
            walletItemList.add("money_transfer");
        }


        walletItemList.add("upgrade");
        walletItemList.add("request");
        walletItemList.add("myOrders");
//        walletItemList.add("nearby");

        if (checkNearbyVisibility()) {
            walletItemList.add("nearby");
        }
        //walletItemList.add("deals");

        if (walletItemList.size() == 4) {
            //TODO make staic views visibility visible and dynamic view gone
            walletStaticheaderoptions.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        } else {
            //TODO make dynamic views visibility visible and static view gone
            walletStaticheaderoptions.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
        initStaticHeader(convertView);
    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, String
            mGAKey) {
        this.cjrHomePageLayoutV2 = pageLayout;
        //check if user is login show wallet_top_bar_heading text and if not show wallet_top_bar_heading
        updateWalletHeader(context);

        walletItemList.clear();
        if (WalletUtilities.isPayTabVisible(mContext)) {
            walletItemList.add("pay");
        }

        walletItemList.add("postcard");


        if (WalletUtilities.isSavingAccVisible(mContext)) {
            walletItemList.add("savings_account");
        }


        walletItemList.add("passbook");

        walletItemList.add("add");


        if (WalletUtilities.isMoneyTransferTabVisible(mContext)) {
            walletItemList.add("money_transfer");
        }


        walletItemList.add("upgrade");

        walletItemList.add("request");
//        walletItemList.add("myOrders");
        if (checkNearbyVisibility()) {
            walletItemList.add("nearby");
        }

        //walletItemList.add("deals");

        if (walletItemList.size() == 4) {
            //TODO make staic views visibility visible and dynamic view gone
            walletStaticheaderoptions.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        } else {
            //TODO make dynamic views visibility visible and static view gone
            walletStaticheaderoptions.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
        //on data set change just notified the adapter to remove the repetition of GA
        if (listView.getAdapter() == null) {
            CJRHorizontalWalletAdapter adapter = new CJRHorizontalWalletAdapter((Activity) mContext, walletItemList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);
        } else {
            ((CJRHorizontalWalletAdapter) listView.getAdapter()).notifyDataSetChanged();
        }
        setStaticHeaderClickListener();


    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (walletItemList.get(position)) {

        }
    }


    private boolean checkNearbyVisibility() {
        boolean isNearByVisible = true;
        try {
            String visibilityStatus = "true";

            return !visibilityStatus.equalsIgnoreCase("false");
        } catch (Exception e) {
            e.printStackTrace();
            return isNearByVisible;
        }
    }


    public int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public void setRobotoRegularFont(Context context, TextView view,
                                     int style) {
        if (view == null)
            return;

        if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            view.setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.NORMAL));
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }

    }

    private void initStaticHeader(View convertView) {
        mLytPay = (RelativeLayout) convertView.findViewById(R.id.lyt_pay);
        mLytAddMoney = (RelativeLayout) convertView.findViewById(R.id.lyt_add_money);
        mLytPassbook = (RelativeLayout) convertView.findViewById(R.id.lyt_passbook);
        mLytRequest = (RelativeLayout) convertView.findViewById(R.id.lyt_request);
        setRobotoRegularFont(mContext, (TextView) mLytPay.findViewById(R.id
                .tv_pay_text), Typeface.NORMAL);
        setRobotoRegularFont(mContext, (TextView) mLytAddMoney.findViewById(R.id
                .tv_add_money_text), Typeface.NORMAL);

        setRobotoRegularFont(mContext, (TextView) mLytPassbook.findViewById(R.id
                .tv_passbook_text), Typeface.NORMAL);


        setRobotoRegularFont(mContext, (TextView) mLytRequest.findViewById(R.id
                .tv_request_text), Typeface.NORMAL);

    }


    private void setStaticHeaderClickListener() {
        mLytPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mLytAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mLytPassbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mLytRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    /**
     * update header string as per user login condition and
     * if user is not login this string click envent should move to login activity
     * if user is login click event should move to passbook Screen.
     *
     * @param context
     */
    private void updateWalletHeader(Context context) {

        title.setText(context.getResources().getString(R.string.wallet_top_bar_heading_not_login));
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }







}

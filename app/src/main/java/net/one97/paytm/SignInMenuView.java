package net.one97.paytm;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class SignInMenuView extends RelativeLayout {

	private RelativeLayout mMainLayout;
//	private ImageView mImage;
	private TextView mTextView;
	private OnSignInMenuClickListener mOnSignInMenuClickListener;
	private int mMargin;

	public interface OnSignInMenuClickListener {

		void onSignInMenuClick(View view);
	}

	public SignInMenuView(Context context) {
		super(context);
		
		init(context);
	}

	public SignInMenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public SignInMenuView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	private void init(Context context) {
		mMainLayout = new RelativeLayout(context);
		mMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, context.getResources().getDisplayMetrics());
		LayoutParams mainParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
//		mMainLayout.setBackgroundColor(Color.DKGRAY);
		
//		mImage = new ImageView(context);
		mTextView = new TextView(context);
		mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		setEditText("Sign In");
		if (context != null)
			mTextView.setTextColor(context.getResources().getColor(R.color.paytm_blue));
		else
			mTextView.setTextColor(Color.rgb(00, 183, 227));
		//CJRAppUtility.setRobotoMediumFont(context, mTextView, Typeface.NORMAL);
		mTextView.setGravity(Gravity.CENTER);
		
//		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		mMainLayout.addView(mImage, params);
		
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mMainLayout.addView(mTextView, params);
		
		addView(mMainLayout, mainParams);
		
		mMainLayout.setClickable(true);
		setEditTextMargin(0,0,mMargin,0);
		mMainLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				mOnSignInMenuClickListener.onSignInMenuClick(view);
			}
		});
//		mMainLayout.setVisibility(View.GONE);
		
	}
	
	public int getTextWidth() {
		Rect bounds = new Rect();
		String text = mTextView.getText().toString();
		Paint textPaint = mTextView.getPaint();
		textPaint.getTextBounds(text,0,text.length(),bounds); 
		
		int width = bounds.width() + (2 * mMargin);
		
		return width;
	}
	
	public void setMainLayoutParams(int gridUnit, int height) {
		LayoutParams listViewParams = (LayoutParams) mMainLayout.getLayoutParams();
		listViewParams.width = gridUnit;
		listViewParams.height = height;
		
		LayoutParams textParams = (LayoutParams) mTextView.getLayoutParams();
		textParams.addRule(RelativeLayout.CENTER_VERTICAL);
		textParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
		//textParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		
		/*LayoutParams imageParams = (LayoutParams) mImage.getLayoutParams();
		imageParams.addRule(RelativeLayout.CENTER_VERTICAL);*/
	}
	
	public void setMainLayoutPadding(int left, int top, int right, int bottom) {
		mMainLayout.setPadding(left, top, right, bottom);
	}
	
	private void setEditTextMargin(int left, int top, int right, int bottom) {
		LayoutParams textParams = (LayoutParams) mTextView.getLayoutParams();
		textParams.leftMargin = left;
		textParams.topMargin = top;
		textParams.rightMargin = right;
		textParams.bottomMargin = bottom;
	}
	
//	public void setMainLayoutBackground(int resid) {
//		mMainLayout.setBackgroundResource(resid);
//	}

	/*public void setImageBackground(int resid) {
		mImage.setBackgroundResource(resid);
	}*/

	public void setEditTextBackground(int resid) {
		mTextView.setBackgroundResource(resid);
	}
	
	public void setEditTextColor(int color) {
		mTextView.setTextColor(color);
	}

	public void setEditText(String text) {
		if (text != null) {
			mTextView.setVisibility(View.VISIBLE);
			mMainLayout.setVisibility(View.VISIBLE);
			mTextView.setText(text);
		} else {
			mTextView.setVisibility(View.GONE);
		}
	}
	
	public void setClickable(boolean isClickable) {
		mMainLayout.setClickable(isClickable);
	}
	
	public String getSignInText() {
		return mTextView.getText().toString();
	}

	public void setSignInTextVisibility(int visibility) {
		mMainLayout.setVisibility(visibility);
	}
	
	public void setOnSignInMenuViewClickListener(OnSignInMenuClickListener listener) {
		mOnSignInMenuClickListener = listener;
		
	}
}
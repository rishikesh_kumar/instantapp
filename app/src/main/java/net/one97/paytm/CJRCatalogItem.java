

package net.one97.paytm;

import android.text.TextUtils;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CJRCatalogItem extends CJRItem {
	
	private static final long serialVersionUID = 1L;
	
	private boolean mIsSelected;
	
	@SerializedName("image_data")
   	private String mImageData;
	
	@SerializedName("image_url")
	private String mImageIconUrl;
	
	transient ArrayList<String> mParentName;

	public int position;

	
	@SerializedName("name")
   	private String mName;
	
	@SerializedName("url")
   	private String mUrl;

	@SerializedName("seourl")
	private String mSeoUrl;
	
	@SerializedName("url_type")
   	private String mUrlType;
	
	@SerializedName("tag_label_name")
   	private String mTagLabel;
	
	@SerializedName("tag_label_color")
   	private String mTagColor;
	
	@SerializedName("items")
	private ArrayList<CJRCatalogItem> mSubItems;
	
	@SerializedName("brand")
   	private String mBrand;
	
 	public String getTagLabel() {
		return mTagLabel;
	}

	public String getTagColor() {
		return mTagColor;
	}

	public ArrayList<CJRCatalogItem> getSubItems() {
		return mSubItems;
	}
	
	public String getImageData() {
		return this.mImageData;
	}
	
	public String getImageIconUrl(){
		return mImageIconUrl;
	}
	
	@Override
	public String getURLType() {
		return mUrlType;
	}
	@Override
	public String getURL() {
		if(mUrl != null && !TextUtils.isEmpty(mUrl)) {
			return mUrl;
		}else{
			return mSeoUrl;
		}
	}
	@Override
	public String getName() {
		return mName;
	}

	@Override
	public String getBrand() {
		// TODO Auto_generated method stub
		return mBrand;
	}

	public boolean isSelected() {
		return mIsSelected;
	}

	public void setSelected(boolean isSelected) {
		mIsSelected = isSelected;
	}
	
	public void setName(String name) {
		mName = name;
	}

    public void setTagLabel(String tagLabel) {
        mTagLabel = tagLabel;
    }

    public void setTagLabelColor(String tagLabelColor) {
        mTagColor = tagLabelColor;
    }
	
	public void setImageIconUrl(String imageIconUrl){
		mImageIconUrl = imageIconUrl;
	}
	
	public void setURLType(String urlType) {
		mUrlType = urlType;
	}
	
	public void setImageData(String imageData) {
		mImageData = imageData;
	}
	
	public void setBrand(String brand) {
		mBrand = brand;
	}
	
	@Override
	public boolean equals(Object object) {
		try {
			CJRCatalogItem item = (CJRCatalogItem) object;
			return mName.equalsIgnoreCase(item.getName());
		} catch (Exception e) {
			return false;
		}
		
	}

	public void setUrl(String url) {
		mUrl = url;
	}

	@Override
	public String getItemID() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SerializedName("label")
	private String mLabel;


	public void setmRelatedCategories(ArrayList<CJRRelatedCategory> mRelatedCategories) {
		this.mRelatedCategories = mRelatedCategories;
	}

	@SerializedName("related_category")
	private ArrayList<CJRRelatedCategory> mRelatedCategories = new ArrayList<CJRRelatedCategory>();


	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return mLabel;
	}

	@Override
	public String getListName() {
		return null;
	}

	@Override
	public int getListPosition() {
		return position;
	}

	@Override
	public String getSearchType() {
		return null;
	}

	@Override
	public String getSearchCategory() {
		return null;
	}

	@Override
	public String getSearchTerm() {
		return null;
	}

	@Override
	public String getSearchResultType() {
		return null;
	}

	public void setLabel(String label) {
		 mLabel = label;
	}
	
	@SerializedName("category_id")
	private String mCategoryId;

	public String getCategoryId() {
		return mCategoryId;
	}

	public void setCategoryId(String categoryId) {
		mCategoryId = categoryId;
	}

	public void setParentName(ArrayList<String> name) {
		mParentName = name;
	}
	
	public ArrayList<String> getParentName() {
		return mParentName;
	}
	
	public void setParentNameItems(ArrayList<String> name) {
		mParentName = name;
		if(mSubItems != null && mSubItems.size() > 0) {
			int size = mSubItems.size();
			for(int i = 0; i < size; i++) {
				ArrayList<String> parent = new ArrayList<String>();
				parent.addAll(mParentName);
				parent.add(mName);
				mSubItems.get(i).setParentNameItems(parent);
			}
		}
	}
	public String getListId() {
		return mListId;
	}

	@Override
	public String getmContainerInstanceID() {
		return "";
	}

	@Override
	public String getParentID() {
		return "";
	}

	public String getSource() {
		return null;
	}

	public void setListId(String mListId) {
		this.mListId = mListId;
	}

	private String mListId="";

	@Override
	public String getSearchABValue() {
		return null;
	}
}

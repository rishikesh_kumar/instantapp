package net.one97.paytm;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by anup on 17/10/16.
 */
public class CJRHorizontalWalletAdapter extends CJRBaseAdapter {
    private final ArrayList<String> mItemList;
    private final LayoutInflater mInflater;
    private final Activity mActivity;

    public CJRHorizontalWalletAdapter(Activity mContext, ArrayList<String> walletItemList) {

        super();
        mItemList = walletItemList;
        mInflater = LayoutInflater.from(mContext);
        mActivity = mContext;
    }

    public  int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (activity != null && !activity.isFinishing()) {
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int i) {
        return mItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
         return mItemList.get(i).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.horizontal_wallet_list_item, null);
            holder.textName = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.labelTag = (TextView) convertView.findViewById(R.id.tv_label);
            holder.smart_list_root = (RelativeLayout) convertView.findViewById(R.id
                    .smart_list_root);

            convertView.setTag(holder);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();

        if (true) {
            int width = getScreenWidth(mActivity);
            double itemWidth = width / 6.0;
            ViewGroup.LayoutParams layout = holder.smart_list_root.getLayoutParams();
            layout.width = (int) itemWidth;
        }
        switch (mItemList.get(position)){
            case "upgrade":
                holder.textName.setText(mActivity.getString(R.string.upgrade));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.kyc_home_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "savings_account":
                holder.textName.setText(mActivity.getString(R.string.top_menu_saving_acc));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.saving_account));
                if(mActivity!=null ){
                    holder.labelTag.setVisibility(View.VISIBLE);
                    holder.labelTag.setText(mActivity.getString(R.string.beta));
                }else{
                    holder.labelTag.setVisibility(View.INVISIBLE);
                }
                break;
            case "pay":
                holder.textName.setText(mActivity.getString(R.string.top_menu_pay));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.top_bar_pay_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "add":
                holder.textName.setText(mActivity.getString(R.string.top_menu_add_money));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.top_bar_add_money_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "passbook":
                holder.textName.setText(mActivity.getString(R.string.top_menu_passbook));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.homepage_passbook_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "nearby":
                holder.textName.setText(mActivity.getString(R.string.top_menu_nearby));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.nearby_symbol));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "request":
                holder.textName.setText(mActivity.getString(R.string.top_menu_request_money));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.top_bar_request_money_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "deals":
                holder.textName.setText(mActivity.getString(R.string.deals_home));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.deals_home_page_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "myOrders":
                holder.textName.setText(mActivity.getString(R.string.event_my_orders));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.my_orders_home_page_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "money_transfer":
                holder.textName.setText(mActivity.getString(R.string.money_transfer));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.transfer_money_home_icon));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
            case "postcard":
                holder.textName.setText(mActivity.getString(R.string.top_menu_postcard));
                holder.itemImage.setImageDrawable(ContextCompat.getDrawable(mActivity,R.drawable.postcard));
                holder.labelTag.setVisibility(View.INVISIBLE);
                break;
        }


     return convertView;
    }

    public static class ViewHolder {
        TextView textName;
        ImageView itemImage;
        TextView labelTag;
        RelativeLayout smart_list_root;
    }
}

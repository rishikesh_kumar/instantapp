package net.one97.paytm;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.android.volley.VolleyError;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class AJRMainActivity extends CJRActionBarBaseActivity {

    public static final String UNREAD_NOTIFICATION_COUNT = "unread_notification_count";
        private static final long LOCATION_UPDATE_INTERVAL = TimeUnit.MINUTES.toMillis(10);
    /**
     * Intent action sent as a local broadcast to update the channel.
     */
    public static String ACTION_UPDATE_CHANNEL = "com.urbanairship.push.sample" +
            ".ACTION_UPDATE_CHANNEL";
    private String TAG = AJRMainActivity.class.getSimpleName();

    private String mPushRichPageType;
    private String mPushRichPageId;
    private String mSavedLanguage;

    private boolean doubleBackToExitPressedOnce;
    private boolean isFirstonPrepare = false;
    private boolean smoothScroll = true;
    private boolean isHomePageDataUpdated;
    private boolean mIsActivityResumed = false;

    private CJRHomePageItem mRefData;
    private CJRCustomHomeViewPager view_pager_homepage;

    //  private PendingPushModel pendingPushModel;
    private CJRHomePageAdapter mHomeFragmentAdapter;
    private Context mContext;

    private Handler backPressedHandler = null;
    private String isAlarmRegistered;
    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior<View> bottomSheetBehavior;
    private boolean isCustProductListResponsePending = false;
    private boolean isBankServiceApiResponsePending = false;
    private HomeTabItem[] homeTabItems;
    private HashMap<String, View> homeTabViewsMap = new HashMap<>();
    private TextView txtNotificationCount;

    private int actionBarHeight = 0;
    private int mGridUnit;
    private int currentPosition;
    private int previousTotalScroolledRV;
    private boolean isSavedTncCallMade;
    private boolean isTNCButtonClicked;
    float scrollThreasholdForToolBarDecoyVisibility = 0;
    private LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CJRVolley.init(this);
        mFrameLayout = (FrameLayout) findViewById(R.id.content_frame);

        mContext = getApplicationContext();
        initLayoutViews();
        Toolbar myToolbar = (Toolbar) findViewById(R.id.common_toolbar);
        myToolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(myToolbar);

        setUpToolBar();

        initMainPage();

        TypedValue tv = new TypedValue();
        if (this.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            scrollThreasholdForToolBarDecoyVisibility = 3 * actionBarHeight;
        }
    }


    public  int getScreenGridUnit() {
        return getScreenWidth()
                / CJRConstants.NUMBER_OF_BOX_IN_ROW;
    }

    public  int getScreenGridUnitBy32() {
        return getScreenWidth() / 32;
    }


    protected void setUpToolBar() {
        int gridUnit = (int) (getScreenGridUnit() * .6);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {

            android.app.ActionBar.LayoutParams layoutParams = new android.app.ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            layoutParams.height = gridUnit
                    * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;
            setHomeIconEnabled(true);

            if (smoothScroll) {
                mActionBar.setDisplayHomeAsUpEnabled(true);
                mActionBar.setDisplayShowTitleEnabled(false);
                mActionBar.setDisplayShowHomeEnabled(true);
                setSearchButtonVisibility(true);

                mActionBar.setDisplayShowCustomEnabled(true);
                mActionBar.setCustomView(R.layout.action_bar_title);
                mActionBar.setElevation(2);
                //setSearchButtonVisibility(true);
                mActionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.white)));
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(true);
                mActionBar.setDisplayShowTitleEnabled(false);
                mActionBar.setDisplayShowHomeEnabled(false);

                // setSearchButtonVisibility(false);

                setSearchButtonVisibility(false);


                mActionBar.setDisplayShowCustomEnabled(true);
                mActionBar.setCustomView(R.layout.lyt_new_action_bar);
                LinearLayout searchContainer = (LinearLayout) findViewById(R.id.lyt_search_container);
                mActionBar.setElevation(2);

                mActionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.transparent)));
            }


        }
    }


    private void initLayoutViews() {

        mFrameLayout.addView(getLayoutInflater().inflate(R.layout.activity_main, null));
        setTitle(new StringBuilder().toString());
        mProgessBarItemLoading = (ProgressBar) findViewById(R.id.item_loading_progressbar);
        rel_homepage_loading = (RelativeLayout) findViewById(R.id.rel_homepage_loading);
        mGridUnit = getScreenGridUnit();
        initBottomBarTab();
    }


    private void initBottomBarTab() {
        //Init the hometabs items.
        homeTabItems = HomeTabProvider.getHomeTabs(this);
        LinearLayout tabContainer = (LinearLayout) findViewById(R.id.parent_layout_bottom);

        for (int i = 0; i < homeTabItems.length; i++) {

            HomeTabItem item = homeTabItems[i];
            View view = getLayoutInflater().inflate(R.layout.home_tab_item_layout, null, false);
            view.setTag(item);

            ImageView tabImage = (ImageView) view.findViewById(R.id.tab_img);
            tabImage.setImageDrawable(item.getIcon());

            TextView textView = (TextView) view.findViewById(R.id.tab_txt);
            textView.setText(item.getText());
            textView.setTextColor(ContextCompat.getColorStateList(this, item.getTextColorResource()));

            view.findViewById(R.id.notification_count)
                    .setVisibility(item.showNotificationCountView() ? View.VISIBLE : View.GONE);

            if (item.showNotificationCountView()) {
                txtNotificationCount = (TextView) view.findViewById(R.id.notification_count);
            }

            homeTabViewsMap.put(item.getUrlType(), view);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);

            view.setLayoutParams(param);
            tabContainer.addView(view, i);
        }

        loadMainPage();
    }

    private void loadMainPage() {
        initializeUI();
    }

    /**
     * Shows Bottom sheet to user to open the bank account
     */



    private void initializeUI() {
    }


    public void initMainPage() {
        try {
            initializeViewPager();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMenuItemVisibility() {
        setSearchButtonVisibility(smoothScroll);
        setNotificationViewVisibility(true);
        //TODO: Chck if required for all Activities and move it to setNotificationViewVisibility
        setNotification();
        setIconMenuViewVisibility(false);
        setHomeIconEnabled(true, R.drawable.action_bar_logo_with_bg);
    }



    private void selectBottomTabBarItem(String urlType) {
        if (!TextUtils.isEmpty(urlType)) {
            //deselect all tab items first
            for (View view : homeTabViewsMap.values()) {
                view.setSelected(false);
                view.findViewById(R.id.tab_img).setSelected(false);
                view.findViewById(R.id.tab_txt).setSelected(false);
            }

            // Get the selected view
            View selectedView = homeTabViewsMap.get(urlType);
            selectedView.setSelected(true);
            selectedView.findViewById(R.id.tab_img).setSelected(true);
            selectedView.findViewById(R.id.tab_txt).setSelected(true);


            setMenuItemVisibility();

            switch (urlType) {
                case CJRConstants.URL_TYPE_MAIN:
                    setEditViewVisibility(false);
                    setUpdatesNotificationVisibility(false);
                    setQRIconVisibility(false);
                    setEditWidth(0);
                    break;

                case CJRConstants.URL_TYPE_MALL:
                    setEditViewVisibility(false);
                    setUpdatesNotificationVisibility(false);
                    setEditWidth(0);
                    setQRIconVisibility(false);
                    break;

                case CJRConstants.URL_TYPE_BAZAAR:
                    setEditViewVisibility(false);
                    setEditWidth(0);
                    setQRIconVisibility(false);
                    break;

                case CJRConstants.URL_TYPE_CONTACT_US:
                    setSearchButtonVisibility(false);
                    setUpdatesNotificationVisibility(false);
                    getSupportActionBar().show();
                    setNotificationViewVisibility(false);
                    setEditViewVisibility(false);
                    setQRIconVisibility(false);
                    setEditWidth(0);
                    break;

                case CJRConstants.URL_TYPE_MY_PROFILE:
                    setSearchButtonVisibility(false);
                    setNotificationViewVisibility(false);
                    setUpdatesNotificationVisibility(false);
                    setEditViewVisibility(false);
                    setQRIconVisibility(false);
                    setEditWidth(0);
                    break;

                case CJRConstants.URL_TYPE_UPDATES:
                    setSearchButtonVisibility(false);
                    setNotificationViewVisibility(false);
//                    setUpdatesNotificationVisibility(CJRAppUtility.isUserSignedIn(this));
                    setUpdatesNotificationVisibility(false);
                    handleClearAllVisibility();
                    getSupportActionBar().show();
                    setQRIconVisibility(false);
                    break;
            }
        }
    }


    private void handleClearAllVisibility() {

        if (true) {
            setEditViewVisibility(true);
            setEditWidth(mGridUnit * 4);
        } else {
            setEditViewVisibility(false);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!isFirstonPrepare) {
            setEditViewVisibility(false);
            isFirstonPrepare = true;
        }
        setEditText(getString(R.string.clear_all));
        setEditWidth(mGridUnit * 4);
//        setMenuUpdatesCount();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        if (mProgessBarItemLoading != null)
            mProgessBarItemLoading.setVisibility(View.GONE);

        super.onResume();
    }

        @Override
    public void onResponse(IJRDataModel dataModel) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    private void initializeViewPager() {
        selectBottomTabBarItem("main");
        view_pager_homepage = (CJRCustomHomeViewPager) findViewById(R.id.view_pager_homepage);
        view_pager_homepage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                updateSelectedFragment(currentPosition);

//                if(!smoothScroll && homeTabItems != null) {
//                    if(homeTabItems[position].getUrlType().equals(CJRConstants.URL_TYPE_MALL)) {
//                        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources()
//                                .getColor(R.color.transparent)));
//                    } else {
//                        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources()
//                                .getColor(R.color.white)));
//                    }
//                }

                if (!smoothScroll && homeTabItems != null) {
                    if (homeTabItems[position].getUrlType().equals(CJRConstants.URL_TYPE_MALL)) {
                        mActionBar.setDisplayHomeAsUpEnabled(true);
                        mActionBar.setDisplayShowTitleEnabled(false);
                        mActionBar.setDisplayShowHomeEnabled(false);

                        setSearchButtonVisibility(false);

                        mActionBar.setDisplayShowCustomEnabled(true);
                        mActionBar.setCustomView(R.layout.lyt_new_action_bar);
                        mActionBar.setElevation(2);
                        //mSearchView.setVisibility(View.GONE);
                        LinearLayout searchContainer = (LinearLayout) findViewById(R.id.lyt_search_container);
                        //searchContainer.setOnClickListener(searchBarClickListener);

                        setSearchButtonVisibility(false);
                        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.menu));
                        if (mActionbarHelper.mCartView != null) {
                            mActionbarHelper.setWhiteBagIcon();
                        }


                        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                                .getColor(R.color.transparent)));
                        if (previousTotalScroolledRV >= 0) {
                           // onVerticalScrolled(previousTotalScroolledRV);
                        }
                    } else {

                        mActionBar.setDisplayHomeAsUpEnabled(true);
                        mActionBar.setDisplayShowTitleEnabled(false);
                        mActionBar.setDisplayShowHomeEnabled(true);


                        setSearchButtonVisibility(false);
                        mActionBar.setDisplayShowCustomEnabled(true);
                        mActionBar.setCustomView(R.layout.action_bar_title);
                        mActionBar.setElevation(2);
                        if (mActionbarHelper.mCartView != null) {
                            mActionbarHelper.setDarkThemeforCartIcon();
                        }
                        //setSearchButtonVisibility(true);
                        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.black_menu));
                        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                                .getColor(R.color.white)));
                       // onVerticalScrolled(0);

                    }

                    if (homeTabItems[position].getUrlType().equals(CJRConstants.URL_TYPE_BAZAAR)) {
                        setSearchButtonVisibility(true);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mHomeFragmentAdapter = new CJRHomePageAdapter(mContext, getSupportFragmentManager(), mRefData, null, mPushRichPageType, mPushRichPageId, homeTabItems);
        view_pager_homepage.setAdapter(mHomeFragmentAdapter);
        view_pager_homepage.setOffscreenPageLimit(4);
        view_pager_homepage.post(new Runnable() {
            @Override
            public void run() {
                if (view_pager_homepage != null) {
                    view_pager_homepage.setCurrentItem(0, smoothScroll);
                }
            }
        });
        hideProgess();

    }

    public void hideProgess() {
        if (rel_homepage_loading != null) {
            rel_homepage_loading.setVisibility(View.GONE);
        }
    }

    void updateSelectedFragment(int index) {
        if (mHomeFragmentAdapter == null)
            return;

        mCurrentTabIndex = index;
        Fragment fragment = mHomeFragmentAdapter.getFragment(index);
//        if (fragment instanceof FJRUpdateHomeScreen) {
//            ((FJRUpdateHomeScreen)fragment).init();
//        }



//        if (mCurrentTabIndex == 4) {
//            getSupportActionBar().setIcon(R.drawable.updates_action_bar_logo_with_bg);
//            getSupportActionBar().setLogo(R.drawable.updates_action_bar_logo_with_bg);
////            setHomeIconEnabled(true, );
//        } else {
        setHomeIconEnabled(true);
//        }

    }


}
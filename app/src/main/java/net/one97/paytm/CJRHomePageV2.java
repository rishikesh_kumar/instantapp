
package net.one97.paytm;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CJRHomePageV2 extends CJRDataModelItem {

    private static final long serialVersionUID = 1L;

    @SerializedName("page")
    public ArrayList<CJRHomePageDetailV2> mPage = new ArrayList<CJRHomePageDetailV2>();

    @SerializedName("pages")
    public ArrayList<CJRHomePageDetailV2> mPages = new ArrayList<CJRHomePageDetailV2>();

    public ArrayList<CJRHomePageDetailV2> getmPages() {
        return mPages;
    }

    public void setmPages(ArrayList<CJRHomePageDetailV2> mPages) {
        this.mPages = mPages;
    }

    public ArrayList<CJRHomePageDetailV2> getmPage() {
        return mPage;
    }

    public void setmPage(ArrayList<CJRHomePageDetailV2> mPage) {
        this.mPage = mPage;
    }

    @SerializedName("context")
    public CJRContext cjrContext;

    @SerializedName("mobile_layout")
    public ArrayList<CJRHomePageLayoutV2> mMobileLayout = new ArrayList<CJRHomePageLayoutV2>();
    @SerializedName("ga_key")
    public String mGAKey;
    @SerializedName("placeholder_image_url")
    public String mPlaceHolderImageUrl;
    public boolean isOrderDetailsExpanded;
    @SerializedName("error")
    private String mErrorMsg;
    @SerializedName("page_id")
    private String mID = "";
    @SerializedName("old_category_id")
    private String mOldCategoryId;
    @SerializedName("footer_image_url")
    private String mFooterImage;
    private String mGATitle;
    //@SerializedName("page_id")
    private String entityAssociatedWith;
    @SerializedName("description")
    private String description;
    @SerializedName("alt_image_url")
    private String alt_image_url;
    @SerializedName("url_key")
    private String urlKey;
    @SerializedName("image_url")
    private String image_url;
    @SerializedName("name")
    private String banner_name;

    @SerializedName("page_name")
    private String pageName;

    @SerializedName("api_version")
    private String apiVersion;



    public String getErrorMsg() {
        return mErrorMsg;
    }

    public String getOldCategoryId() {
        return mOldCategoryId;
    }

    public String getID() {
        return mID;
    }

    public String getGAKey() {
        return mGAKey;
    }

    public ArrayList<CJRHomePageLayoutV2> getMobileLayoutList() {
        return mMobileLayout;
    }

    @Override
    public String getName() {
        // TODO Auto_generated method stub
        return null;
    }

    public String getFooterImage() {
        return mFooterImage;
    }

    public void setFooterImageUrl(String url) {
        mFooterImage = url;
    }

    public String getPlaceHolderImageUrl() {
        return mPlaceHolderImageUrl;
    }

    public String getGATitle() {
        return mGATitle;
    }

    public void setGATitle(String title) {
        this.mGATitle = title;
    }

    public String getEntityAssociatedWith() {
        return mID;
    }

    public void setEntityAssociatedWith(String entityAssociatedWith) {
        this.entityAssociatedWith = entityAssociatedWith;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getUrlKey() {
        return urlKey;
    }

    public String getAlt_image_url() {
        return alt_image_url;
    }

    public String getDescription() {
        return description;
    }

    public String getBannerName() {
        return banner_name;
    }

    public boolean isOrderDetailsExpanded() {
        return isOrderDetailsExpanded;
    }

    public void setIsOrderDetailsExpanded(boolean isOrderDetailsExpanded) {
        this.isOrderDetailsExpanded = isOrderDetailsExpanded;
    }

    public ArrayList<CJRHomePageItem> getParentListForItem(String layoutName, CJRHomePageItem
            item) {

        ArrayList<CJRHomePageItem> itemList = new ArrayList<>();

        for(CJRHomePageDetailV2 c : mPage) {
            ArrayList<CJRHomePageLayoutV2> mHomePageLayoutList = c.getHomePageLayoutList();
            for (CJRHomePageLayoutV2 pageLayout : mHomePageLayoutList) {
                ArrayList<CJRHomePageItem> homeItemList = pageLayout.getHomePageItemList();
                if (homeItemList.contains(item)) {
                    CJRHomePageItem currentItem = homeItemList.get(homeItemList.indexOf(item));
                    if (currentItem != null && currentItem.getParentItem() != null && item
                            .getParentItem() != null && currentItem.getParentItem().equalsIgnoreCase
                            (item.getParentItem())) {
                        itemList = pageLayout.getHomePageItemList();
                        return itemList;
                    }
                }
            }

        }
        return  null;
    }

    public CJRHomePageLayoutV2 getLayoutFromItem(CJRHomePageItem item) {
        try {
            for(CJRHomePageDetailV2 c : mPage) {
                ArrayList<CJRHomePageLayoutV2> mHomePageLayoutList = c.getHomePageLayoutList();

                for (CJRHomePageLayoutV2 pageLayout : mHomePageLayoutList) {
                    ArrayList<CJRHomePageItem> homeItemList = pageLayout.getHomePageItemList();
                    if (homeItemList != null && item != null && homeItemList.contains(item)) {
                        CJRHomePageItem currentItem = homeItemList.get(homeItemList.indexOf(item));
                        if (currentItem != null && currentItem.getURLType() != null && item
                                .getURLType() != null && currentItem.getURLType().equalsIgnoreCase
                                (item.getURLType())) {
                            return pageLayout;
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getPageName(){

        return pageName;
    }
}

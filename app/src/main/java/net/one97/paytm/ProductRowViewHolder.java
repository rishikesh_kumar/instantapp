package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rama on 4/25/2016.
 */
public class ProductRowViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder,
        AdapterView.OnItemClickListener, CJRHorizontalListView.OnScrollStateChangedListener {

    TextView title;
    CJRHorizontalListView listView;
    private TextView mViewAll;
    int mGridUnit;
    Context mContext;
    private String containerInstanceID = "";
    private String mchildSiteId="";
    private String mGAKey="";
    private CJRHomePageLayoutV2 cjrHomePageLayoutV2;
    private BrandProductListAdapter mProductListRowAdapter;

    public ProductRowViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext=mContext;

        title = (TextView) convertView.findViewById(R.id
                .txt_row_title);
        listView = (CJRHorizontalListView) convertView.findViewById
                (R.id.row_product_list);
        mViewAll = (TextView) convertView.findViewById(R.id.right_heading_tv);
//        seeAllArrow = (ImageView) convertView.findViewById(R.id.img_see_all_arrow);
     }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, String mGAKey) {

        final String itemName = pageLayout.getName();
        this.cjrHomePageLayoutV2 = pageLayout;
        title.setText(itemName);

        String containerInstanceID="";


        if (!TextUtils.isEmpty(pageLayout.getSeeAllUrl())) {
            mViewAll.setTextColor(context.getResources().getColor(R.color.blue));
//            seeAllArrow.setVisibility(View.VISIBLE);

            final String finalContainerInstanceID = containerInstanceID;
            mViewAll.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                }
            });

//            CJRHomePageItem pageItem = new CJRHomePageItem();
//            pageItem.setName("See All View");
//            int size = pageLayout.getHomePageItemList().size()-1;
//            if(pageLayout.getHomePageItemList().get(size).getName()!=null&&!pageLayout
//                    .getHomePageItemList().get(size).getName().equals("See All View")) {
//                pageLayout.getHomePageItemList().add(pageItem);
//            }
        } else {
            title.setTextColor(context.getResources().getColor(R.color.text_color_collection_heading));
        }

        ArrayList<CJRHomePageItem> cjrHomePageItems = pageLayout.getHomePageItemList();
        CJRHomePageItem cjrHomePageItem = null;
        if(cjrHomePageItems!=null && cjrHomePageItems.size()>0) cjrHomePageItem = cjrHomePageItems.get(0);

        if(cjrHomePageItem!=null && cjrHomePageItem.getMproducts()!=null && cjrHomePageItem.getMproducts().size()>0){
            //cjrHomePageItems.clear();
            cjrHomePageItems = cjrHomePageItem.getMproducts();
        }

        if(containerInstanceID!=null && !containerInstanceID.equals("")) {
            for (CJRHomePageItem item : cjrHomePageItems) {
                   item.setmContainerInstanceID(containerInstanceID);
            }
        }
        String layoutType = (null != mchildSiteId) ? StringUtils.concat(
                cjrHomePageLayoutV2.getmLayoutType(), "_", cjrHomePageLayoutV2.getmListTitleName()) :
                cjrHomePageLayoutV2.getLayout();
        mProductListRowAdapter = new BrandProductListAdapter((Activity) mContext,
                cjrHomePageItems, layoutType, itemName, mGAKey + "-", containerInstanceID, mchildSiteId, 2);
        listView.setAdapter(mProductListRowAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnScrollStateChangedListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String containerInstanceID;

        if(cjrHomePageLayoutV2 !=null && cjrHomePageLayoutV2.getDatasources()!=null && cjrHomePageLayoutV2.getDatasources().size()>0)
        containerInstanceID  = cjrHomePageLayoutV2.getDatasources().get(0).getmContainerInstanceID();
        else containerInstanceID = "";

        CJRHorizontalListView listView = (CJRHorizontalListView) parent;
        CJRHomePageItem item = (CJRHomePageItem) listView
                .getItemAtPosition(position);
        if(item.getName()!=null&&item.getName().equals("See All View")){

        } else {

        }
    }

    @Override
    public void onScrollStateChanged(ScrollState scrollState) {
        if(scrollState == ScrollState.SCROLL_STATE_IDLE){
            //// TODO: 21/08/17 tushar please update the ga for old widgets also as now its using
            // new widget adapter
//            mProductListRowAdapter.sendProductImpressionEvent();
        }
    }
}

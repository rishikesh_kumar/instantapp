package net.one97.paytm;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by rohit on 21/3/17.
 */

public interface HomeTabItem {

    /**
     * Return the list drawable to be used
     * for displaying this icon.
     *
     * @return resource identifier for drawable
     */
    Drawable getIcon();

    /**
     * Return string to be used to be used
     * for tab text.
     *
     * @return string resource identifier for tab title
     */
    String getText();


    /**
     * Return the id of color to be used
     * for displaying this text.
     *
     * @return resource identifier for textColor
     */
    int getTextColorResource();

    /**
     * Flag to indicate whether to show notification count or not.
     *
     * @return
     */
    boolean showNotificationCountView();

    /**
     * Returns the fragment to be used by view pager associated with this home tab item.
     */
    Fragment getFragment(Bundle bundle);

    /**
     * Return position of this home tab item
     *
     * @return
     */
    int getPosition();

    /**
     * Returns the URL type of this home tab item
     *
     * @return
     */
    String getUrlType();

    /**
     * Returns the tag to be used for gtm.
     *
     * @return
     */
    String getGtmEventTag();

    /**
     * Sets the tab text string
     * @param mText
     */
    void setmText(String mText);

    /**
     * Sets the landing url to be used for the tab click
     * @param mUrl
     */
    void setmUrl(String mUrl, boolean refresh);

    /**
     * Sets the Urltype of the tab
     * @param mUrlType
     */
//    void setmUrlType(String mUrlType);

    /**
     * Sets the drawable selector to be used for the tab icon
     * @param mSelector
     */
    void setmSelector(StateListDrawable mSelector);

}

package net.one97.paytm;

/**
 * Created by bheemesh on 4/1/16.
 */
public interface ActivityToFragmentListener {

    void updateFragment(boolean updateAllFragment);

    void updateHeading();

}

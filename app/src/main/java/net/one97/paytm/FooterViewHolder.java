package net.one97.paytm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Rama on 4/25/2016.
 */
public class FooterViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder {

    Context mContext;
    private ImageView mFaceBookImg;
    private ImageView mTwitterImg;
    private ImageView mInstagram;
    private ImageView mYoutubeImg;
    private ImageView mPinintrest;

    public FooterViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext=mContext;
        mFaceBookImg = (ImageView) convertView.findViewById(R.id.img_facebook);
        mTwitterImg = (ImageView) convertView.findViewById(R.id.img_twitter);
        mInstagram = (ImageView) convertView.findViewById(R.id.img_instagram);
        mYoutubeImg = (ImageView) convertView.findViewById(R.id.img_youtube);
        mPinintrest = (ImageView) convertView.findViewById(R.id.img_pinintrest);

    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, final String
            mGAKey) {

        mFaceBookImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mTwitterImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mYoutubeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mPinintrest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}


package net.one97.paytm;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;


public class RoboTextView extends TextView {

	public RoboTextView(Context context) {
		super(context);
		if(!isInEditMode())
		init();
	}

	public RoboTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		//init();
		if(!isInEditMode())
		setAttribute(context, attrs);
	}

	public RoboTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//init();
		if(!isInEditMode())
		setAttribute(context, attrs);
	}


	public  int getOSVersion() {
		return Build.VERSION.SDK_INT;
	}

	private void setAttribute(Context context, AttributeSet attrs) {
		int font;
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.RoboTextView);
		try {
			font = ta.getInteger(R.styleable.RoboTextView_fontType, 0);
		} finally {
			ta.recycle();
		}
		if (font != 0) {
			switch (font) {

			case 1:
				if(getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF_LIGHT, Typeface.NORMAL));
				} else {
					setTypeface(null, Typeface.NORMAL);
				}
//				try {
//					
//					if (getTypeface() == null
//							|| (getTypeface() != null && !getTypeface().equals(
//									Typefaces.get(getContext(),
//											CJRConstants.FONT_ROBOTO_LIGHT))))
//						setTypeface(Typefaces.get(getContext(),
//								CJRConstants.FONT_ROBOTO_LIGHT));
//				} catch (Exception e) {
//					// In case of exception does not set type face.
//				}
				break;

			case 2:
				if(getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.NORMAL));
				} else {
					setTypeface(null, Typeface.NORMAL);
				}
//				try {
//					if (getTypeface() == null
//							|| (getTypeface() != null && !getTypeface().equals(
//									Typefaces.get(getContext(),
//											CJRConstants.FONT_ROBOTO_REGULAR))))
//						setTypeface(Typefaces.get(getContext(),
//								CJRConstants.FONT_ROBOTO_REGULAR));
//				} catch (Exception e) {
//					// In case of exception does not set type face.
//				}
				break;

			case 3:
				if(getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF_MEDIUM, Typeface.NORMAL));
				} else {
					setTypeface(null, Typeface.BOLD);
				}
//				try {
//					if (getTypeface() == null
//							|| (getTypeface() != null && !getTypeface().equals(
//									Typefaces.get(getContext(),
//											CJRConstants.FONT_ROBOTO_MEDIUM))))
//						setTypeface(Typefaces.get(getContext(),
//								CJRConstants.FONT_ROBOTO_MEDIUM));
//				} catch (Exception e) {
//					// In case of exception does not set type face.
//				}
				break;

			case 4:
				if(getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.BOLD));
				} else {
					setTypeface(null, Typeface.BOLD);
				}
//				try {
//					if (getTypeface() == null
//							|| (getTypeface() != null && !getTypeface().equals(
//									Typefaces.get(getContext(),
//											CJRConstants.FONT_ROBOTO_BOLD))))
//						setTypeface(Typefaces.get(getContext(),
//								CJRConstants.FONT_ROBOTO_BOLD));
//				} catch (Exception e) {
//					// In case of exception does not set type face.
//				}
				break;

                case 5:
                    if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.ITALIC));
                    } else {
                        setTypeface(null, Typeface.ITALIC);
                    }
                    break;

            /*case 5:
				try {
					if (getTypeface() == null
							|| (getTypeface() != null && !getTypeface().equals(
									Typefaces.get(getContext(),
											CJRConstants.FONT_HELVETICA_NEUE_LIGHT))))
						setTypeface(Typefaces.get(getContext(),
								CJRConstants.FONT_HELVETICA_NEUE_LIGHT));
				} catch (Exception e) {
					// In case of exception does not set type face.
				}
				break;
				
			case 6:
				try {
					if (getTypeface() == null
							|| (getTypeface() != null && !getTypeface().equals(
									Typefaces.get(getContext(),
											CJRConstants.FONT_HELVETICA_NEUE_REGULAR))))
						setTypeface(Typefaces.get(getContext(),
								CJRConstants.FONT_HELVETICA_NEUE_REGULAR));
				} catch (Exception e) {
					// In case of exception does not set type face.
				}
				break;
				
			case 7:
				try {
					if (getTypeface() == null
							|| (getTypeface() != null && !getTypeface().equals(
									Typefaces.get(getContext(),
											CJRConstants.FONT_HELVETICA_NEUE_BOLD))))
						setTypeface(Typefaces.get(getContext(),
								CJRConstants.FONT_HELVETICA_NEUE_BOLD));
				} catch (Exception e) {
					// In case of exception does not set type face.
				}
				break;*/

			}
		}

	}

	private void init() {
		if (!isInEditMode()) {
			// Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
			// "fonts/opensans_light.ttf");
			try {
				// In case of getTypeFace return null or it not equal to
				// required obj then only set Typeface.
				if (getTypeface() == null
						|| (getTypeface() != null && !getTypeface().equals(
								Typefaces.get(getContext(),
										CJRConstants.FONT_ROBOTO_LIGHT))))
					setTypeface(Typefaces.get(getContext(),
							CJRConstants.FONT_ROBOTO_LIGHT));
			} catch (Exception e) {
				// In case of exception does not set type face.
			}
		}
	}


}


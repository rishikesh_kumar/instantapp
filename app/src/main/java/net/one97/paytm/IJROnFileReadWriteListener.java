
package net.one97.paytm;

public interface IJROnFileReadWriteListener {

	 void onFileReadComplete(IJRDataModel dataModel, String fileHeader);
	 void onFileWriteComplete(String fileHeader);
}

package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;


/**
 * Created by Rama on 4/25/2016.
 */
public class SmallCarouselViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder,
        AdapterView.OnItemClickListener {

    TextView title;
    CJRHorizontalListView listView;
    Context mContext;
    private CJRHomePageLayoutV2 cjrHomePageLayoutV2;
    private String containerInstanceID;

    public SmallCarouselViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext = mContext;

        title = (TextView) convertView.findViewById(R.id.txt_row_title);
        listView = (CJRHorizontalListView) convertView.findViewById(R
                .id.row_product_list);
    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, String
            mGAKey) {

        final String itemName = pageLayout.getName();
        this.cjrHomePageLayoutV2 = pageLayout;

        if (itemName != null){
            title.setText(itemName);
        }

        CJRCarouselItemAdapter adapter = new CJRCarouselItemAdapter((Activity) mContext, pageLayout
                .getHomePageItemList(), pageLayout.getLayout(), itemName, mGAKey + "-",
                containerInstanceID);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        CJRHorizontalListView listView = (CJRHorizontalListView) parent;
        CJRHomePageItem item = (CJRHomePageItem) listView
                .getItemAtPosition(position);

    }
}

package net.one97.paytm;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;


/**
 * Created by rohit on 21/3/17.
 */

public class HomeTab implements HomeTabItem {

    private Context mContext;
    private int mPosition;


    private String mText;
    private Drawable mSelector;

    public HomeTab(Context context, int position) {
        mPosition = position;
        mContext = context;
        init();
    }

    private void init() {
        mText = mContext.getString(R.string.home);
        mSelector = mContext.getResources().getDrawable(R.drawable.bottom_bar_paytm_selector);
    }

    @Override
    public Drawable getIcon() {
        return mSelector;
    }

    @Override
    public String getText() {
        return mText;
    }

    @Override
    public int getTextColorResource() {
        return R.drawable.tab_bar_text_selector;
    }

    @Override
    public boolean showNotificationCountView() {
        return false;
    }

    @Override
    public Fragment getFragment(Bundle bundle) {

        FJRHomeFragment mainFragment = new FJRHomeFragment();
        bundle.putBoolean(CJRConstants.EXTRA_INTENT_TAB_POSITION_FIRST, true);
        mainFragment.setArguments(bundle);
        return mainFragment;
    }

    @Override
    public int getPosition() {
        return mPosition;
    }

    @Override
    public String getUrlType() {
        return CJRConstants.URL_TYPE_MAIN;
    }

    @Override
    public String getGtmEventTag() {
        return "deewf";
    }

    @Override
    public void setmText(String mText) {
    }

    @Override
    public void setmUrl(String mUrl, boolean refresh) {
    }

    @Override
    public void setmSelector(StateListDrawable mSelector) {
    }

}

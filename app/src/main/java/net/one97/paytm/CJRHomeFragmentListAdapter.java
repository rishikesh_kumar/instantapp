package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;


public class CJRHomeFragmentListAdapter extends RecyclerView.Adapter {
    ArrayList<CJRHomePageLayoutV2> layoutList = new ArrayList<CJRHomePageLayoutV2>();
    Context mContext;
    String mGAKey;
    String mLogoUrl, mBannerUrl, mdisplaytitle, mBadgeImageUrl,mbrandTag,mchildSiteId;

    public CJRHomeFragmentListAdapter(Activity activity, CJRHomePageV2 mHomePageItemList,
                                      String GAKey) {
        mContext = activity;
        mGAKey = GAKey;
        layoutList = getLayoutList(mHomePageItemList);
    }


    ArrayList<CJRHomePageLayoutV2> getLayoutList(CJRHomePageV2 mHomePageItemList){

        ArrayList<CJRHomePageDetailV2> cjrHomePageDetailV2s = mHomePageItemList.getmPage();
        ArrayList<CJRHomePageLayoutV2> cjrHomePageLayoutV2s =  new ArrayList<CJRHomePageLayoutV2>();
        for(CJRHomePageDetailV2 c:cjrHomePageDetailV2s) {
            cjrHomePageLayoutV2s.addAll(c.pageRowItems(false,false));
        }
        return cjrHomePageLayoutV2s;
   }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutType layoutType = LayoutType.fromIndex(viewType);
        return HomeViewHolderFactory.getHomeViewHolder(mContext, parent, layoutType);
    }


    @Override
    public int getItemViewType(int position) {
        LayoutType displayCardType = LayoutType.fromName(getContentItem(position).getLayout());
        if (displayCardType == null) {
            return -1;
        }
        return displayCardType.getIndex();
    }

    public CJRHomePageLayoutV2 getContentItem(int position) {
        return layoutList.get(position);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UpdateViewHolder updatableViewHolder = (UpdateViewHolder) holder;

        if(holder instanceof CarouselHorizontalViewHolder){
            CarouselHorizontalViewHolder view = (CarouselHorizontalViewHolder) holder;
            view.updateViewHolder(mContext, layoutList.get(position), mGAKey);
            if(mchildSiteId!=null){
                view.updateSiteID(mchildSiteId);
            }
        }
        else{
            updatableViewHolder.updateViewHolder(mContext, layoutList.get(position), mGAKey);
        }
    }

    @Override
    public int getItemCount() {
        return layoutList == null ? 0 : layoutList.size();
    }

    public void updateFromNetwork(CJRHomePageV2 mHomePageItemList) {

        layoutList = getLayoutList(mHomePageItemList);

        notifyDataSetChanged();
    }

    public void updateData(CJRHomePageV2 mHomePageItemList) {

        layoutList = getLayoutList(mHomePageItemList);

        notifyDataSetChanged();
    }

    public void updateBrandStoreData(CJRHomePageV2 mHomePageItemList,int position) {

        layoutList = getBrandLayoutList(mHomePageItemList,position);

        notifyDataSetChanged();
    }

    ArrayList<CJRHomePageLayoutV2> getBrandLayoutList(CJRHomePageV2 mHomePageItemList, int position){

        ArrayList<CJRHomePageDetailV2> cjrHomePageDetailV2s = mHomePageItemList.getmPages();
        ArrayList<CJRHomePageLayoutV2> cjrHomePageLayoutV2s =  new ArrayList<CJRHomePageLayoutV2>();

       // if(position==0) {
            CJRHomePageLayoutV2 customLayout1 = new CJRHomePageLayoutV2();
            customLayout1.setmLayoutType(LayoutType.LAYOUT_CATEGORY_COROUSEL.getName());
            cjrHomePageLayoutV2s.add(customLayout1);
        //}

        CJRHomePageDetailV2 cjrHomePageDetail = cjrHomePageDetailV2s.get(0);

        if(cjrHomePageDetail.getmHomePageSlotItemList()!=null && cjrHomePageDetail.getmHomePageSlotItemList().size()>0){

            ArrayList<CJRHomePageSlotItemV2> mHomePageSlotItemList = cjrHomePageDetail.getmHomePageSlotItemList();

            CJRHomePageSlotItemV2 cjrHomePageSlotItemV2 = mHomePageSlotItemList.get(1);

            ArrayList<CJRHomePageSlotItemV2> mHomePageSubSlotItemList = cjrHomePageSlotItemV2.getmHomePageSlotItemList();

            CJRHomePageSlotItemV2 cjrHomePageSubSlotItemV2 = mHomePageSubSlotItemList.get(position);

            if(cjrHomePageSubSlotItemV2!=null){
                cjrHomePageLayoutV2s.addAll(cjrHomePageSubSlotItemV2.pageRowItems(false));
            }
        }

        return cjrHomePageLayoutV2s;
    }




}
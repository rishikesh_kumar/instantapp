package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

public class CJRRelatedCategory extends CJRItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String listId="";

	public void setLabel(String mLabel) {
		this.mLabel = mLabel;
	}

	@SerializedName("label")
	private String mLabel;

	public void setUrl(String mUrl) {
		this.mUrl = mUrl;
	}

	@SerializedName("url")
	public String mUrl;
	
	public String mUrlType;
	
	
	public String getLabel() {
		return mLabel;
	}

	@Override
	public String getListName() {
		return null;
	}

	@Override
	public int getListPosition() {
		return -1;
	}

	@Override
	public String getSearchType() {
		return null;
	}

	@Override
	public String getSearchCategory() {
		return null;
	}

	@Override
	public String getSearchTerm() {
		return null;
	}

	@Override
	public String getSearchResultType() {
		return null;
	}

	@Override
	public String getCategoryId() {
		return null;
	}

	@Override
	public String getListId() {
		return listId;
	}

	public String getURL() {
		return mUrl;
	}

	@Override
	public String getURLType() {
		// TODO Auto-generated method stub
		return mUrlType;
	}

	
	@Override
	public String getBrand() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getItemID() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setUrlType(String urlType) {
		mUrlType = urlType;
	}

	@Override
	public String getmContainerInstanceID() {
		return "";
	}

	@Override
	public String getParentID() {
		return "";
	}

	public String getSource() {
		return null;
	}

	@Override
	public String getSearchABValue() {
		return null;
	}

}


package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;


public class CJRActionBarHelper {

    private CJRActionBarBaseActivity mActivity;

    private int mGridUnit;
    private int mWidgetPadding;
    public NotificationView mCartView,mUpdatesCountView;
    private EditView mEditLayout;
    private SignInMenuView mSignInLayout;
    private IconMenuView mIconMenuView;

    public  int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public  int getScreenGridUnit(Context context) {
        return getScreenWidth((Activity) context)
                / CJRConstants.NUMBER_OF_BOX_IN_ROW;
    }

    public  int getScreenGridUnitBy32(Context context) {
        return getScreenWidth((Activity) context) / 32;
    }



    public  int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (activity != null && !activity.isFinishing()) {
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    public void initilizeNotificationView(CJRActionBarBaseActivity activity, NotificationView
            cartView) {

        if (getOSVersion() > Build.VERSION_CODES.HONEYCOMB) {
            android.app.ActionBar.LayoutParams layoutParams = new android.app.ActionBar
                    .LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            layoutParams.height = mGridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;
        } else {
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar
                    .LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            layoutParams.height = mGridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;
        }

        mActivity = activity;
        mGridUnit = getScreenGridUnit(mActivity);
        mWidgetPadding = mGridUnit / CJRConstants.GRID_UNIT_HALF;


        if (cartView != null) {
            mCartView = cartView;
            //notoficationLayout.setMainLayoutBackground(R.drawable.bg_cell);
            cartView.setImageBackground(R.drawable.ic_cart_dark);
//		cartView.setNotificationTextBackground(R.drawable.notification_icon_bg);

//            cartView.setNotificationTextBackground(R.drawable.notification_circle);
            cartView.setNotificationTextVisibility(View.GONE);


            if (mActivity.getSupportActionBar() != null) {
                cartView.setMainLayoutParams(mGridUnit * CJRConstants
                                .NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR,
                        mActivity.getSupportActionBar().getHeight());
            }
            cartView.setNotificationTextMargin(0, mGridUnit / 5, mGridUnit / 4, 0);

        }
    }


    public void initilizeUpdatesCountView(CJRActionBarBaseActivity activity, NotificationView
            countView  ) {

        if (getOSVersion() > Build.VERSION_CODES.HONEYCOMB) {
            android.app.ActionBar.LayoutParams layoutParams = new android.app.ActionBar
                    .LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            layoutParams.height = mGridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;
        } else {
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar
                    .LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            layoutParams.height = mGridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;
        }

        mActivity = activity;
        mGridUnit = getScreenGridUnit(mActivity);
        mWidgetPadding = mGridUnit / CJRConstants.GRID_UNIT_HALF;


        if (countView != null) {
            mUpdatesCountView = countView;
            countView.setMainLayoutTag("updates_count");
            countView.setImageBackground(R.drawable.icon_notifications);
            countView.setNotificationTextVisibility(View.GONE);


            if (mActivity.getSupportActionBar() != null) {
                countView.setMainLayoutParams(mGridUnit * CJRConstants
                                .NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR,
                        mActivity.getSupportActionBar().getHeight());
            }
            countView.setNotificationTextMargin(0, mGridUnit / 5, mGridUnit / 4, 0);
        }
    }

    public void initilizeEditView(CJRActionBarBaseActivity activity, EditView editLayout) {
        mActivity = activity;
        mEditLayout = editLayout;
        mGridUnit = getScreenGridUnit(mActivity);
        mWidgetPadding = mGridUnit / CJRConstants.GRID_UNIT_HALF;

        editLayout.setMainLayoutBackground(Color.TRANSPARENT);
        editLayout.setEditTextVisibility(View.VISIBLE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams
                .MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        layoutParams.height = mGridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;

        editLayout.setMainLayoutParams(mGridUnit * CJRConstants
                        .NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR,
                mActivity.getSupportActionBar().getHeight());
        editLayout.setEditTextMargin(0, mGridUnit / 8, mGridUnit / 8, 0);

    }

    public void setEditWidth(int width) {
        if (mActivity != null && !mActivity.isFinishing()) {
            mEditLayout.setMainLayoutParams(width,
                    mActivity.getSupportActionBar().getHeight());
        }
    }

    public void setEditTextSize(int size) {
        if (mActivity != null && !mActivity.isFinishing()) {
            mEditLayout.setTextSize(size);
        }
    }

    public void initilizeSignInMenuView(CJRActionBarBaseActivity activity, SignInMenuView
            signInLayout) {
        mActivity = activity;
        mSignInLayout = signInLayout;
        int gridUnit = getScreenGridUnit(mActivity);

//		signInLayout.setMainLayoutBackground(R.drawable.bg_cell);
        signInLayout.setSignInTextVisibility(View.GONE);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams
                .MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        layoutParams.height = gridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;

        signInLayout.setMainLayoutParams(signInLayout.getTextWidth(),
                mActivity.getSupportActionBar().getHeight());


    }

    public void initializeIconMenuView(CJRActionBarBaseActivity activity, IconMenuView
            iconMenuView) {
        mIconMenuView = iconMenuView;
        int gridUnit = getScreenGridUnit(mActivity);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams
                .MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT);
        layoutParams.height = gridUnit * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;

        if (mIconMenuView != null) {
            mIconMenuView.setIconMenuVisibility(View.GONE);
        }
    }

    public void setIconMenuImageResource(int resID) {
        if (mActivity != null && !mActivity.isFinishing() && mIconMenuView != null) {
            mIconMenuView.setImageIconResource(resID);
        }
    }


    public void setSignInMenuWidth() {
        if (mActivity != null && !mActivity.isFinishing()) {
            mSignInLayout.setMainLayoutParams(mSignInLayout.getTextWidth(),
                    mActivity.getSupportActionBar().getHeight());
        }
    }

    public void setNotoficationImageBackground(int resid) {
        mCartView.setImageBackground(resid);
    }

    public void setWhiteThemeforCartIcon()
    {
        mCartView.setWhiteThemeforCartIcon();
    }

    public void setDarkThemeforCartIcon()
    {
        mCartView.setDarkThemeforCartIcon();
    }

    public void setWhiteBagIcon()
    {
        mCartView.setWhiteBagIcon();
    }

    public void setNotification(String notificationText) {
        if (mCartView != null) {
            mCartView.setNotificationText(notificationText, mGridUnit);
            mCartView.setNotificationTextVisibility(View.VISIBLE);
        }
    }

    public void setMenuUpdatesCount(String notificationText) {
        if (mUpdatesCountView != null) {
            if (!TextUtils.isEmpty(notificationText)) {
                mUpdatesCountView.setNotificationText(notificationText, mGridUnit);
                mUpdatesCountView.setNotificationTextVisibility(View.VISIBLE);
            } else {
                mUpdatesCountView.setNotificationTextVisibility(View.GONE);
            }
        }
    }

    public void animateNotification(Activity activity) {
        if (mCartView != null) {
            mCartView.animateTextLayout(activity);
        }
    }

    public void setEditText(String text) {
        if (mEditLayout != null) {
            if (text != null && text.trim().length() > 0) {
                mEditLayout.setEditText(text);
                mEditLayout.setClickable(true);
                mEditLayout.setEditTextVisibility(View.VISIBLE);
            } else {
                mEditLayout.setEditText(" ");
                mEditLayout.setClickable(false);
            }
        }
    }

    public void setSignInText(String text) {
        if (mSignInLayout != null) {
            if (text != null && text.trim().length() > 0) {
                mSignInLayout.setEditText(text);
                mSignInLayout.setClickable(true);
                mSignInLayout.setSignInTextVisibility(View.VISIBLE);
                setSignInMenuWidth();
            } else {
                mSignInLayout.setEditText(" ");
                mSignInLayout.setClickable(false);
                //setSignInMenuWidth();
            }
        }
    }

    public String getEditText() {
        if (mEditLayout != null) {
            return mEditLayout.getEditText();
        }
        return null;
    }

    public void setEditTextSize(float size) {
        if (mEditLayout != null) {
            mEditLayout.setTextSize(size);
        }
    }

    public String getSignInText() {
        if (mSignInLayout != null) {
            return mSignInLayout.getSignInText();
        }
        return null;
    }

    public void setNotificationVisibility(int visibility) {
        if (mCartView != null) {
            mCartView.setNotificationTextVisibility(visibility);
        }
    }

    public void setEditVisibility(int visibility) {
        if (mEditLayout != null) {
            mEditLayout.setEditTextVisibility(visibility);
        }
    }

    public void setSignInVisibility(int visibility) {
        if (mSignInLayout != null) {
            mSignInLayout.setSignInTextVisibility(visibility);
        }
    }

    public void setIconMenuVisibility(int visibility) {
        if (mIconMenuView != null) {
            mIconMenuView.setIconMenuVisibility(visibility);
        }
    }
}

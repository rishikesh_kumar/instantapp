package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Praveen Kumar on 12/15/2015.
 */
public class CJRHorizontalTextListAdapter extends CJRBaseAdapter {

    private ArrayList<CJRHomePageItem> mItemList;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private boolean isSendViewEvent = false;
    private String mParentListName;
    private String mScreenName;
    ArrayList<String> viewedItemIds = new ArrayList<>();
    ArrayList<String> viewedItemIdsForPromotions = new ArrayList<>();

    private  String[] smartIconName=null;

    public CJRHorizontalTextListAdapter(Activity activity, ArrayList<CJRHomePageItem> itemList,
                                        String layoutType, String parentListName, String screenName) {
        super();
        mItemList = itemList;
        mScreenName = screenName;
        mInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mParentListName = parentListName;
    }

    public void setSendViewEvent(boolean viewEvent) {
        this.isSendViewEvent = viewEvent;
    }

    public void setItemList(ArrayList<CJRHomePageItem> itemList){
        mItemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CJRHomePageItem cjrHomePageItem = mItemList.get(position);

        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.horizontal_text_list_item, null);
            holder.textName = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.labelTag = (TextView) convertView.findViewById(R.id.tv_label);
            holder.smart_list_root = (RelativeLayout) convertView.findViewById(R.id
                    .smart_list_root);

            convertView.setTag(holder);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();


        if (!TextUtils.isEmpty(cjrHomePageItem.getName())) {

           holder.textName.setText(cjrHomePageItem.getName());
        }

        if (true) {
            int width = getScreenWidth();
            double itemWidth = width / 6.0;
            ViewGroup.LayoutParams layout = holder.smart_list_root.getLayoutParams();
            layout.width = (int) itemWidth;
        }

        String imageUrl = cjrHomePageItem.getImageUrl();
        if (!TextUtils.isEmpty(imageUrl)) {
            if(imageUrl.equals("gold.png")){
                holder.itemImage.setImageResource(R.drawable.gold_coin);
            }else {

                Picasso.with(mActivity)
                        .load(imageUrl)
                        .into(holder.itemImage);
            }
        }

        // Label value container builder
        CJRLayoutParams labelParams = cjrHomePageItem.getLayoutParam();
        if (labelParams != null && !TextUtils.isEmpty(labelParams.getLabel())) {
            holder.labelTag.setVisibility(View.VISIBLE);
            holder.labelTag.setText(labelParams.getLabel());
            if ((!TextUtils.isEmpty(labelParams.getLabelColor()))) {
                setTagTextColor(holder.labelTag, labelParams.getLabelColor());
            } else {
                holder.labelTag.setTextColor(Color.WHITE);
            }

            if ((!TextUtils.isEmpty(labelParams.getLabelBgColor()))) {
                setTagBkgrndColor(holder.labelTag, labelParams.getLabelBgColor());
            } else {
                setTagBkgrndColor(holder.labelTag, "#f8f8f8");
            }
        } else {
            holder.labelTag.setVisibility(View.INVISIBLE);
        }

        if(isSendViewEvent) {
            //To send unique View event
            if (!viewedItemIds.contains(cjrHomePageItem.getItemID())) {
                viewedItemIds.add(cjrHomePageItem.getItemID());
                sendItemViewEvent(mScreenName,cjrHomePageItem,position);
                if (position>=3){
                    if (mActivity instanceof AJRMainActivity) {

                    } else {
                        sendItemsViewEventAsPromotion();
                    }
                }
            }
        }
        return convertView;
    }

    public  int getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (mActivity != null && !mActivity.isFinishing()) {
            mActivity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    //Used to send item view for homapage and mall
    private void sendItemViewEvent(String screenName, CJRHomePageItem cjrHomePageItem, int position){
        screenName = screenName.replace("/","");
        screenName = screenName.replace("-","");
        if (screenName.equalsIgnoreCase("")){
           // CJRSendGTMTag.smartIcontListView(mActivity.getApplicationContext(), cjrHomePageItem, position, mParentListName);
        }else if(screenName.equalsIgnoreCase("mall")){
            //CJRSendGTMTag.smartIcontListViewForMall(mActivity.getApplicationContext(), cjrHomePageItem, position, mParentListName);
        }else {
          //  CJRSendGTMTag.smartIcontListView(mActivity.getApplicationContext(), cjrHomePageItem, position, mParentListName);
        }
    }

    public ArrayList<CJRHomePageItem> getData() {
        return mItemList;
    }

    public void clearData() {
        if (mItemList != null) {
            mItemList.clear();
            notifyDataSetChanged();
        }
    }

    private void setTagTextColor(TextView textView, String color) {
        if (color != null && color.length() > 1) {
            if (color.charAt(0) != '#') {
                color = '#' + color;
            }
        }
        int parsedColor;
        try {
            parsedColor = Color.parseColor(color);
        } catch (IllegalArgumentException illegalArgumentException) {
            parsedColor = Color.TRANSPARENT;
        } catch (Exception exception) {
            parsedColor = Color.TRANSPARENT;
        }
        textView.setTextColor(parsedColor);
    }

    // Tag label background and rounded rectangle

    private void setTagBkgrndColor(TextView textView, String bgColor) {
        textView.setBackgroundDrawable(mActivity.getResources().getDrawable(
                R.drawable.rounded_rectangle));
        GradientDrawable drawable = (GradientDrawable) textView
                .getBackground();
        if (bgColor != null && bgColor.length() > 1) {
            if (bgColor.charAt(0) != '#') {
                bgColor = '#' + bgColor;
            }
        }
        int parsedColor;
        try {
            parsedColor = Color.parseColor(bgColor);
        } catch (IllegalArgumentException illegalArgumentException) {
            parsedColor = Color.TRANSPARENT;
        } catch (Exception exception) {
            parsedColor = Color.TRANSPARENT;
        }
        drawable.setColor(parsedColor);
        drawable.setStroke(1, parsedColor);
    }

    private float getPixelValue(Context context, int dpValue) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14,
                r.getDisplayMetrics());
        return px;
    }

    public static class ViewHolder {
        TextView textName;
        ImageView itemImage;
        TextView labelTag;
        RelativeLayout smart_list_root;
    }

    /**
     * Send items view event as promotion
     */
    public void sendItemsViewEventAsPromotion(){
        String screenName = mScreenName;
        //        screenName = screenName.replace("-","");
        if(!TextUtils.isEmpty(screenName) && screenName.charAt(screenName.length() - 1) == '-') {
            screenName = screenName.substring(0, screenName.length() - 1);
        }
        if (screenName.equalsIgnoreCase("/")){
            //CJRSendGTMTag.sendPromotionImpressionForSmartIconList(getMapOfItemsToSendGA(screenName),"",mActivity,screenName);
        }else{
           // CJRSendGTMTag.sendPromotionImpressionForSmartIconList(getMapOfItemsToSendGA(screenName),"",mActivity,screenName);
        }
    }

    private List<Map<String,Object>> getMapOfItemsToSendGA(String screenName){
        List<Map<String,Object>> maps = new ArrayList<>();
        StringBuilder promotionName= new StringBuilder(screenName);
        promotionName.append("-strip-");
        promotionName.append(mParentListName);

        for (int i=0;i<mItemList.size();i++){
                if (!viewedItemIdsForPromotions.contains(mItemList.get(i).getItemID())){
                    if (viewedItemIds.contains(mItemList.get(i).getItemID())) {
                        viewedItemIdsForPromotions.add(mItemList.get(i).getItemID());
                        Map<String, Object> tempMap = new HashMap<>();
                        tempMap.put("dimension40", mItemList.get(i).getmContainerInstanceID());
                        tempMap.put("id", mItemList.get(i).getItemID());
                        tempMap.put("name", promotionName.toString());
                        tempMap.put("creative", mItemList.get(i).getName());
                        tempMap.put("position", String.valueOf(i + 1));
                        maps.add(tempMap);
                    }
                }

        }
        return maps;
    }


}

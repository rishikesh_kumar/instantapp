
package net.one97.paytm;


public abstract class CJRItem extends CJRDataModelItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public abstract String getURLType();
	public abstract String getURL();
	public abstract String getBrand();
	public abstract String getName();
	public abstract String getItemID();
	public abstract String getLabel();
	public abstract String getListName();//dimension24
	public abstract int getListPosition();//dimension25
	public abstract String getSearchType();//dimension26
	public abstract String getSearchCategory();//dimension27
	public abstract String getSearchTerm();//dimension28
	public abstract String getSearchResultType();//dimension29
	public abstract String getSearchABValue();//dimension53
	public abstract String getCategoryId();
	public abstract String getListId();
	public abstract String getmContainerInstanceID();
	public abstract String getParentID();
	public abstract String getSource();
}

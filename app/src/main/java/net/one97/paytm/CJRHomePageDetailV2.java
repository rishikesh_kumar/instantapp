
package net.one97.paytm;



import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CJRHomePageDetailV2 {

	@SerializedName("id")
	private String mId;

	@SerializedName("layout")
	private String mLayout;

	@SerializedName("layout_details")
	private CJRLayoutDetailV2 mLayoutDetails;

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmLayout() {
		return mLayout;
	}

	public void setmLayout(String mLayout) {
		this.mLayout = mLayout;
	}

	@SerializedName("attributes")
	private CJRLayoutDetailV2 mattribute;

	@SerializedName("mobile_layout")
	public ArrayList<CJRHomePageLayoutV2> mMobileLayout = new ArrayList<CJRHomePageLayoutV2>();

	@SerializedName("slots")
	private ArrayList<CJRHomePageSlotItemV2> mHomePageSlotItemList = new ArrayList<CJRHomePageSlotItemV2>();

	public ArrayList<CJRHomePageSlotItemV2> getmHomePageSlotItemList() {
		return mHomePageSlotItemList;
	}

	public void setmHomePageSlotItemList(ArrayList<CJRHomePageSlotItemV2> mHomePageSlotItemList) {
		this.mHomePageSlotItemList = mHomePageSlotItemList;
	}

	@SerializedName("views")
	public ArrayList<CJRHomePageLayoutV2> mHomePageLayoutList = new ArrayList<CJRHomePageLayoutV2>();

	public void setmHomePageLayoutList(CJRHomePageLayoutV2 mHomePageLayoutList) {
		this.mHomePageLayoutList.add(mHomePageLayoutList);
	}

	public ArrayList<CJRHomePageLayoutV2> getHomePageLayoutList() {
		return mHomePageLayoutList;
	}

	public void setHomePageLayoutList(ArrayList<CJRHomePageLayoutV2> layoutList) {
		this.mHomePageLayoutList=layoutList;
	}

	public ArrayList<CJRHomePageItem> getItemsForLayout(String layoutName) {
		ArrayList<CJRHomePageItem> itemList = null;
		for (CJRHomePageLayoutV2 pagelayout : mHomePageLayoutList) {
			if (pagelayout.getLayout().equalsIgnoreCase(layoutName)) {
				itemList = pagelayout.getHomePageItemList();
				break;
			}
		}
		return itemList;
	}

	public ArrayList<CJRHomePageItem> getItemsForProduct(String sectionName) {
		ArrayList<CJRHomePageItem> itemList = null;
		for (CJRHomePageLayoutV2 pagelayout : mHomePageLayoutList) {
			if (pagelayout.getName().equalsIgnoreCase(sectionName)) {
				itemList = pagelayout.getHomePageItemList();
				break;
			}
		}
		return itemList;
	}

	public ArrayList<CJRHomePageLayoutV2> pageRowItems(boolean isRechargeDataNeeded, boolean showNewWidgets) {
		ArrayList<CJRHomePageLayoutV2> pageItems = new ArrayList<CJRHomePageLayoutV2>();

		if (isRechargeDataNeeded) {
			for (CJRHomePageLayoutV2 pageLayout : mMobileLayout) {
				if (pageLayout.getLayout().equalsIgnoreCase(CJRConstants.LAYOUT_HOME_PAGE_TABS)
						&& pageLayout.getHomePageItemList().size() > 0)
				{
					pageItems.add(pageLayout);
				}
			}
		}

		for (CJRHomePageLayoutV2 pageLayout : mHomePageLayoutList) {
			if (pageLayout.getLayout() == null) continue;

			LayoutType displayCardType = LayoutType.fromName(pageLayout.getLayout());

			switch (displayCardType) {

				case LAYOUT_PRODUCT_ROW:
				case LAYOUT_BRAND_PRODUCT_ROW:
				case LAYOUT_PRODUCT_GRID:
				case LAYOUT_CAROUSEL_1:
				case LAYOUT_CAROUSEL1:
				case LAYOUT_CAROUSEL_FULL_WIDTH:
				case LAYOUT_CAROUSEL_ROW:
				case LAYOUT_CAROUSEL_2:
				case LAYOUT_CAROUSEL2:
				case LAYOUT_VIDEO_COROUSEL:
				case LAYOUT_PHOTO_COROUSEL:
				case LAYOUT_SNIPPET_1:
				case LAYOUT_SNIPPET_2:
				case LAYOUT_SMART_LIST:

					if (pageLayout.getHomePageItemList().size() > 0) {
						pageItems.add(pageLayout);
					}
					break;

				case LAYOUT_TEXT_LINKS:
				case LAYOUT_HORIZONTAL_TEXT_LIST:
				case LAYOUT_VIEW_PAGER:
				case LAYOUT_WALLET_OPTIONS:
				case LAYOUT_FOOTER_SOCIAL_ICONS:
				case LAYOUT_CATEGORY_COROUSEL:
				case LAYOUT_FOOTER_SELL_PARTNER_CONTACT_ICONS:
				case LAYOUT_SUCCESS_CARD:
				case LAYOUT_PENDING_CARD:
				case LAYOUT_AUTO_CARD:
				case LAYOUT_FAILURE_CARD:
				case LAYOUT_FOOTER_CARD:
				case LAYOUT_PAYMENT_CARD:

					pageItems.add(pageLayout);
					break;

				case LAYOUT_IGNORE_TYPE:
					break;
			}

			// for new widgets
			if (showNewWidgets) {
				switch (displayCardType) {
					case LAYOUT_PRODUCT_ROW_3XN:
					case LAYOUT_PRODUCT_ROW_2XN:
					case LAYOUT_PRODUCT_ROW_1XN:
					case LAYOUT_BANNER_2XN:
					case LAYOUT_BANNER_3XN:
					case LAYOUT_SQUARE_BANNER:
					case LAYOUT_THIN_BANNER:
					case LAYOUT_COLLAGE_3X:
					case LAYOUT_COLLAGE_5X:
					case LAYOUT_H1_BANNER:
					case LAYOUT_SMART_ICON_GRID:

						if (pageLayout.getHomePageItemList().size() > 0) {
							pageItems.add(pageLayout);
						}
						break;

					case LAYOUT_ACCORDION_VIEW:

						pageItems.add(pageLayout);
						break;
					case LAYOUT_IGNORE_TYPE:
						break;
				}
			}
		}
		return pageItems;
	}

	public ArrayList<CJRHomePageLayoutV2> getPromotionImpression() {
		ArrayList<CJRHomePageLayoutV2> pageItems = new ArrayList<CJRHomePageLayoutV2>();

		for (CJRHomePageLayoutV2 pageLayout : mHomePageLayoutList) {

			LayoutType displayCardType = LayoutType.fromName(pageLayout.getLayout());

			switch (displayCardType) {

				case LAYOUT_CAROUSEL_1:
				case LAYOUT_CAROUSEL1:
				case LAYOUT_CAROUSEL_FULL_WIDTH:
				case LAYOUT_CAROUSEL_ROW:
				case LAYOUT_CAROUSEL_2:
				case LAYOUT_CAROUSEL2:
				case LAYOUT_TEXT_LINKS:
					if (pageLayout.getHomePageItemList().size() > 0) {
						pageItems.add(pageLayout);
					}
					break;
				case LAYOUT_IGNORE_TYPE:
					break;
			}
		}
		return pageItems;
	}



	public CJRHomePageLayoutV2 getLayoutFromItem(CJRHomePageItem item) {
		try {
			for (CJRHomePageLayoutV2 pageLayout : mHomePageLayoutList) {
				ArrayList<CJRHomePageItem> homeItemList = pageLayout.getHomePageItemList();
				if (homeItemList != null && item != null && homeItemList.contains(item)) {
					CJRHomePageItem currentItem = homeItemList.get(homeItemList.indexOf(item));
					if (currentItem != null && currentItem.getURLType() != null && item
							.getURLType() != null && currentItem.getURLType().equalsIgnoreCase
							(item.getURLType())) {
						return pageLayout;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public void setLayout(String mLayout) {
		this.mLayout = mLayout;
	}

	private String mFooterImageURL;

	public String getFooterImageURL() {
		return mFooterImageURL;
	}

	public void setFooterImageURL(String footerImageURL) {
		this.mFooterImageURL = footerImageURL;
	}


	public CJRCategoryTree getMcategorytree() {
		return mcategorytree;
	}

	public void setMcategorytree(CJRCategoryTree mcategorytree) {
		this.mcategorytree = mcategorytree;
	}

	@SerializedName("category_tree")
	private CJRCategoryTree mcategorytree;

	@SerializedName("badge_image_url")
	private String mBadgeImageUrl;

	public String getmBadgeImageUrl() {
		return mBadgeImageUrl;
	}

	public void setmBadgeImageUrl(String mBadgeImageUrl) {
		this.mBadgeImageUrl = mBadgeImageUrl;
	}
}
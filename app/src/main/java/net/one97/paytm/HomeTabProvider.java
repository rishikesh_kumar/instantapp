package net.one97.paytm;

import android.content.Context;

/**
 * Created by rohit on 21/3/17.
 */

public class HomeTabProvider {

    private static int NUMBER_OF_TABS = 5;

    public static HomeTabItem[] getHomeTabs(Context context) {
        HomeTabItem[] items = new HomeTabItem[NUMBER_OF_TABS];

        items[0] = new HomeTab(context, 0);
        items[1] = new HomeTab(context, 1);
        items[2] = new HomeTab(context, 2);
        items[3] = new HomeTab(context, 3);
        items[4] = new HomeTab(context, 4);

        return items;
    }
}

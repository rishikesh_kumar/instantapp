

package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CJRCatalog extends CJRDataModelItem {
	
	private static final long serialVersionUID = 1L;
	
	@SerializedName("items")
	private ArrayList<CJRCatalogItem> mCatalogItems = new ArrayList<CJRCatalogItem>();
	
   	@SerializedName("search_query_url")
   	private String mSearchQueryUrl;

   	
 	public ArrayList<CJRCatalogItem> getCatalogList() {
		return filterCatalogList();
	}
 	
 	public ArrayList<CJRCatalogItem> getCatalogList(int index) {
		return filterCatalogList(index);
	}
 	
 	private ArrayList<CJRCatalogItem> filterCatalogList() {
 		ArrayList<CJRCatalogItem> items = new ArrayList<CJRCatalogItem>();
 		
 		for(CJRCatalogItem item : mCatalogItems) {
 			if(item.getURLType() != null && item.getURLType().equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_POSTPAID_2)) {
 				continue;
			}
			if(item.getURLType() != null && item.getURLType().equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_POSTPAID_2)) {
				continue;
			}
			items.add(item);
 		}
 		items.trimToSize();
 		return mCatalogItems = items;
 	}
 	
 	private ArrayList<CJRCatalogItem> filterCatalogList(int startIndex) {
 		ArrayList<CJRCatalogItem> items = new ArrayList<CJRCatalogItem>();
 		
 		for(int i = startIndex;i<mCatalogItems.size();i++) {
			items.add(mCatalogItems.get(i));
 		}
  		
 		return  items;
 	}
 	
 	public ArrayList<CJRCatalogItem> getAllCatalogList() {
 		mCatalogItems.trimToSize();

		return mCatalogItems;
	}
 	public void setAllCatalogList(ArrayList<CJRCatalogItem> catalogList) {
 		mCatalogItems.clear();
		mCatalogItems = catalogList;
	}
 	public void setSearchQueryUrl(String searchQuery){
		mSearchQueryUrl = searchQuery;
	}
 	public String getSearchQueryUrl(){
		return this.mSearchQueryUrl;
	}

	@Override
	public String getName() {
		// TODO Auto_generated method stub
		return null;
	}
	public CJRCatalogItem getHomeItem() {
		CJRCatalogItem homeItem = null;
		for(CJRCatalogItem item : mCatalogItems) {
			if(item.getName().equalsIgnoreCase(CJRConstants.PAYTM_HOME)) {
				homeItem = item;
			}
		}
			
		return homeItem;
	}
	
//	public ArrayList<CJRCatalogItem> getFeaturedCategory() {
//		ArrayList<CJRCatalogItem> featuredItem = new ArrayList<CJRCatalogItem>();
//		for(CJRCatalogItem item : mCatalogItems) {
//			if (item.isHomeTabItem()) {
//				featuredItem.add(item);
//			}
//		}
//		return featuredItem;
//	}
	
	
	public void setParentNameItems(ArrayList<String> name) {
		if(mCatalogItems != null && mCatalogItems.size() > 0) {
			int size = mCatalogItems.size();
			for(int i = 0; i < size; i++) {
				mCatalogItems.get(i).setParentNameItems(name);
			}
		}
	}
}

package net.one97.paytm;

import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by selvin on 2015-01-28.
 */
public class WebPUtils {
	private static int isWebPSupportedCached = -1;

	public static boolean isWebPSupported() {
		if (isWebPSupportedCached == -1) {
			final byte[] src =
					Base64.decode("UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA",
							Base64.NO_WRAP);
			try {
				isWebPSupportedCached = BitmapFactory.decodeByteArray(src, 0, src.length) != null ? 1 : 0;
			} catch (Exception ex) {
				isWebPSupportedCached = 0;
				ex.printStackTrace();
			}
		}
		return isWebPSupportedCached == 1;
	}
} 
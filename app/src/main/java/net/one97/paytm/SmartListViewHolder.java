package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Bheemesh
 */
public class SmartListViewHolder extends RecyclerView.ViewHolder implements
        UpdateViewHolder, AdapterView.OnItemClickListener, CJRHorizontalListView.OnScrollStateChangedListener {

    private TextView title;
    private CJRHorizontalListView listView;
    private Context mContext;
    private CJRHomePageLayoutV2 homePageLayout;
    private String mListTypeForGA = "";

    public SmartListViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext = mContext;

        title = (TextView) convertView.findViewById(R.id
                .txt_horizontal_links_title);
        listView = (CJRHorizontalListView) convertView.findViewById(R
                .id.horizontal_text_list);

    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, String
            mGAKey) {

        final String itemName = pageLayout.getName();
        mListTypeForGA = itemName;
        this.homePageLayout = pageLayout;

        if (itemName != null) title.setText(itemName);

        downloadSmartListImages();
        //on data set change just notified the adapter to remove the repetition of GA
        if (listView.getAdapter() == null) {
            CJRHorizontalTextListAdapter adapter = new CJRHorizontalTextListAdapter((Activity) mContext,
                    pageLayout.getHomePageItemList(),"",itemName, mGAKey + "-");
            adapter.setSendViewEvent(true);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);
            listView.setOnScrollStateChangedListener(this);
        } else {
            ((CJRHorizontalTextListAdapter) listView.getAdapter()).setItemList(pageLayout.getHomePageItemList());
        }
    }


    public void downloadSmartListImages() {
        if (homePageLayout != null && homePageLayout.getHomePageItemList() != null) {
            ArrayList<CJRHomePageItem> itemList = homePageLayout.getHomePageItemList();
            int size = itemList.size();
            for (int i = 0; i < size; i++) {
                String imageUrl = itemList.get(i).getImageUrl();
                if (!TextUtils.isEmpty(imageUrl)) {
                    ImageView imageView = new ImageView(mContext);
                    Picasso.with(mContext.getApplicationContext()).load(imageUrl).into(imageView);
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String containerInstanceID = "";

        if (homePageLayout != null && homePageLayout.getDatasources() != null && homePageLayout
                .getDatasources().size() > 0)
            containerInstanceID = homePageLayout.getDatasources().get(0).getmContainerInstanceID();

        CJRHorizontalListView listView = (CJRHorizontalListView) parent;
        CJRHomePageItem item = (CJRHomePageItem) listView
                .getItemAtPosition(position);
    }

    @Override
    public void onScrollStateChanged(ScrollState scrollState) {
        if (scrollState == ScrollState.SCROLL_STATE_IDLE) {
            ((CJRHorizontalTextListAdapter) listView.getAdapter()).sendItemsViewEventAsPromotion();
        }
    }

}

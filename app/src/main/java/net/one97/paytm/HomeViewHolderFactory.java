package net.one97.paytm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Rama on 4/25/2016.
 */
public class HomeViewHolderFactory {

    public static RecyclerView.ViewHolder getHomeViewHolder(final Context mContext, ViewGroup
            parent, LayoutType layoutType) {
        switch (layoutType) {
            case LAYOUT_PRODUCT_ROW:
                return new ProductRowViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_CAROUSEL_1:
                return new CarouselHorizontalViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_CAROUSEL_ROW:
                return new CarouselHorizontalViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_CAROUSEL1:
                    return new CarouselHorizontalViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_SMART_LIST:
                return new SmartListViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_CAROUSEL_2:
                return new SmallCarouselViewHolder(mContext, createView(parent, layoutType));

            case LAYOUT_CAROUSEL2:
                    return new SmallCarouselViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_WALLET_OPTIONS:
                return new WalletTopbarViewHolder(mContext, createView(parent, layoutType));
             case LAYOUT_FOOTER_CARD:
                return new FooterCardViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_FOOTER_SOCIAL_ICONS:
                return new FooterViewHolder(mContext, createView(parent, layoutType));
            case LAYOUT_FOOTER_SELL_PARTNER_CONTACT_ICONS:
                return new SellPartnerContactViewHolder(mContext, createView(parent, layoutType));
            default:
                return new EmptyViewHolder(mContext, createView(parent, layoutType));
        }
    }


    private static View createView(ViewGroup parent, LayoutType newsCardType) {
        switch (newsCardType) {
            case LAYOUT_TEXT_LINKS:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.home_catalog_layout, parent, false);
            case LAYOUT_PRODUCT_ROW:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.horizontal_row_layout, parent, false);
            case LAYOUT_PRODUCT_GRID:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_grid_widget, parent, false);

            case LAYOUT_CAROUSEL_FULL_WIDTH:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.carousel_full_width_layout, parent, false);

            case LAYOUT_CAROUSEL_ROW:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.carousel_brandstore_horizontal_list_layout, parent, false);
            case LAYOUT_CAROUSEL_1:
            case LAYOUT_CAROUSEL1:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.carousel_horizontal_list_container_layout, parent, false);
            case LAYOUT_CAROUSEL_2:
            case LAYOUT_CAROUSEL2:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.small_carousel_layout, parent, false);
            case LAYOUT_SMART_LIST:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.smart_list_layout, parent, false);
            case LAYOUT_HORIZONTAL_TEXT_LIST:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.horizontal_text_list_layout, parent, false);
            case LAYOUT_WALLET_OPTIONS:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.top_bar_wallet_options, parent, false);
                //TODO(bheemesh) changed for testing layout
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.top_bar_wallet_options, parent, false);
            case LAYOUT_FOOTER_CARD:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.contact_us_card_layout, parent, false);
//                int layoutHeight=parent.getHeight()-parent.getChildAt(0).getHeight();
//                if(layoutHeight>500)
//                {
//                    view.getLayoutParams().height=layoutHeight;
//                }
                return view;
            case LAYOUT_FOOTER_SOCIAL_ICONS:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.lyt_footer_image, parent, false);


            case LAYOUT_FOOTER_SELL_PARTNER_CONTACT_ICONS:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.lyt_sell_partner_contact, parent, false);

            default:
                return new View(parent.getContext());
        }
    }


}

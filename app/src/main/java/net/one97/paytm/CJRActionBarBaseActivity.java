package net.one97.paytm;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.android.volley.Request;
import net.android.volley.Response;

import java.util.HashMap;

public abstract class CJRActionBarBaseActivity extends AppCompatActivity
        implements Response.Listener<IJRDataModel>, Response.ErrorListener {

    private static final int TITLE_MAX_LENGTH = 30;
    public ActionBar mActionBar;
    public SearchView mSearchView;
    protected FrameLayout mFrameLayout;
    protected ProgressDialog mProgressDialog;
    protected String mFrequentOrderUrl;
    protected boolean mIsLoadingFrequentOrders;
    protected ProgressBar mProgessBarItemLoading;
    protected RelativeLayout rel_homepage_loading;
    boolean IsDisplayShareIcon;
    private String TAG = CJRActionBarBaseActivity.class.getName();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private MenuItem mSearchMenuItem, mNotificationMenuItem, mUpdatesNotificationMenuItem, mEditMenuItem, mSignInMenuItem,
            mIconMenuItem, mShareMenuItem, mQRMenuItem;
    private TextView mTitleView, mSubTitle;
    private boolean isHomeUpEnabled;
    private boolean mIsSearchIconVisible = true;
    private boolean mIsNotificationIconVisible = true;
    private boolean mIsUpdatesNotificationIconVisible = false;
    private boolean mIsEditIconVisible = true;
    private CharSequence mTitle;
    private boolean isSearchNeeded = true;
    private boolean isNotificationNeeded = true;
    private boolean isQRNeeded = false;
    private boolean isEditViewNeeded = false;
    private boolean isIconMenuNeeded = false;
    private ImageView mTitleDropDown;
    private boolean isDropDownVisible;
    private boolean mIsSignInmenuVisible = false;
    protected int mCurrentTabIndex;
    protected final CJRActionBarHelper mActionbarHelper = new CJRActionBarHelper();

    public static HashMap<String, String> mCategoryIdUrl = new HashMap<String, String>();
    public static HashMap<String, String> mCategoryIdUrlType = new HashMap<String, String>();
    private String RECHARGE_OR_PAY_FOR_CATEGORY_ID = "9367";
    private static boolean isLogoEnabled = false;


    protected void setEditText(String text) {
        mActionbarHelper.setEditText(text);
    }

    protected void setIconMenuViewVisibility(boolean isVisible) {
        if (mIconMenuItem != null) {
            mIconMenuItem.setVisible(isVisible);
            isIconMenuNeeded = isVisible;
            mActionbarHelper.setIconMenuVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    protected void setNotification() {
            setNotification("3");

    }
    protected void setNotification(String notificationText) {
        mActionbarHelper.setNotification(notificationText);
    }

    protected void setNotificationViewVisibility(boolean isVisible) {
        if (mNotificationMenuItem != null) {
            isNotificationNeeded = isVisible;
            mNotificationMenuItem.setVisible(isVisible);
            mIsNotificationIconVisible = isVisible;
            setTitle(mTitle);
            mActionbarHelper.setNotificationVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // After update to 23 , action bar looking bigger. so reducing height
        setContentView(R.layout.action_bar_layout);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        attatchActionBar();
        setUpActionBar();
    }

    protected void setSearchButtonVisibility(boolean isVisible) {
        try {
            if (mSearchMenuItem != null) {
                isSearchNeeded = isVisible;
                if (Build.VERSION_CODES.ICE_CREAM_SANDWICH <= Build.VERSION.SDK_INT) {
                    if (!isVisible && mSearchMenuItem.isActionViewExpanded()) {
                        mSearchMenuItem.collapseActionView();
                    }
                }
                mSearchMenuItem.setVisible(isVisible);
                mIsSearchIconVisible = isVisible;
                setTitle(mTitle);
            }
        } catch (Exception e) {
        }
    }

    public  int getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (this != null && !this.isFinishing()) {
            this.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    public  int getScreenGridUnit() {
        return getScreenWidth()
                / CJRConstants.NUMBER_OF_BOX_IN_ROW;
    }



    protected void setUpActionBar() {
        int gridUnit = (int) (getScreenGridUnit() * .6);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {

            android.app.ActionBar.LayoutParams layoutParams = new android.app.ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.WRAP_CONTENT);
            layoutParams.height = gridUnit
                    * CJRConstants.NUNBER_OF_BOX_IN_HEIGHT_ACTION_BAR;

            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setDisplayShowTitleEnabled(false);
            mActionBar.setDisplayShowHomeEnabled(true);
            setHomeIconEnabled(true);

            mActionBar.setDisplayShowCustomEnabled(true);
            mActionBar.setCustomView(R.layout.action_bar_title);
            mActionBar.setElevation(2);
            mTitleView = (TextView) findViewById(android.R.id.text1);
            mTitleView.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            mTitleView.setEllipsize(TextUtils.TruncateAt.END);
            mTitleView.setTextSize(19);
            mTitleView.setMaxLines(1);

            /*if (this instanceof AJRProductDetail) {
                mTitleView.setPadding(0, 0, 4, 0);
                if (!CJRCSTUtils.showMallFlyer()) {
                    mTitleView.setCompoundDrawablePadding(6);
                    mTitleView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow_pdp_catalog, 0);
                }
                mTitleDropDown = (ImageView) findViewById(R.id.img_drop_down);
                mTitleDropDown.setVisibility(View.GONE);
                final Context context = this;
                mTitleDropDown.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (!CJRCSTUtils.showMallFlyer()) {
                            homeUpClicked();
                        }
                    }
                });

            }*/

            setRobotoBoldFont(this, mTitleView,
                    Typeface.NORMAL);
//            mActionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color
                    .white)));
        }
    }

    public  void setRobotoBoldFont(Context context, TextView view,
                                   int style) {
        if (view == null)
            return;

            view.setTypeface(null, Typeface.BOLD);
    }


    protected void setHomeIconEnabled(boolean isEnabled) {
        setHomeIconEnabled(isEnabled, R.drawable.action_bar_logo_with_bg);
    }

    protected void setHomeIconEnabled(boolean isEnabled, int homeIconResource) {

            mActionBar.setLogo(homeIconResource);
            mActionBar.setIcon(homeIconResource);
               // mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


    }


    private void setDrawableToLogo(Bitmap bitmap) {

        LayerDrawable ld = (LayerDrawable) getResources().getDrawable(R.drawable.action_bar_logo_with_bg);

        int mWid = bitmap.getWidth() / 2;
        // Drawable logoDrawable = new BitmapDrawable(getResources(), bitmap);
        // CJRAppUtility.getPixelValue(13, this);
        int W = getPixelValue(mWid, this);
        int H = getPixelValue(40, this);
        Drawable logoDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, W, H, true));

        boolean isSet = ld.setDrawableByLayerId(R.id.item_layer_action_bar_logo, logoDrawable);
        if (isSet) {
            if (isLogoEnabled) {
                mActionBar.setLogo(ld);
                mActionBar.setIcon(ld);
            }
        }

    }

    public static int getPixelValue(int dp, Context context) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, r.getDisplayMetrics());
        return px;
    }

    public void showNetworkDialog(final Request<IJRDataModel> request) {
        Builder networkDlg = new Builder(this);
        networkDlg.setTitle(getResources().getString(R.string.sign_in_to_paytm));
        networkDlg.setMessage(getResources().getString(R.string.sign_in_to_paytm));

        networkDlg.setPositiveButton(
                getResources().getString(R.string.sign_in_to_paytm),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        if (isNetworkAvailable(CJRActionBarBaseActivity.this)) {
                            CJRVolley.getRequestQueue(getApplicationContext()).add(request);
                        } else {
                            showNetworkDialog(request);
                        }
                    }
                });

        networkDlg.show();
    }

    public  boolean isNetworkAvailable(Context context) {
        if (context == null) return false;
        ConnectivityManager ConnectMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (ConnectMgr == null)
            return false;
        NetworkInfo NetInfo = ConnectMgr.getActiveNetworkInfo();
        if (NetInfo == null)
            return false;

        return NetInfo.isConnected();
    }


    public void showProgressDialog(Context context, String message) {
        if (context == null && !isFinishing())
            return;
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = new ProgressDialog(context);

        try {
            // mProgressDialog.setIcon(R.drawable.paytm_logo);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage(message);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        } catch (IllegalArgumentException e) {
            /*
             * When we dismiss dialog and finish the activity, sometimes
			 * activity get finished before dialog successfully dismisses. So
			 * that dialog is no longer attached to the view of destroyed
			 * activity.
			 */
        } catch (Exception e) {
            /*
             * When we dismiss dialog and finish the activity, sometimes
			 * activity get finished before dialog successfully dismisses. So
			 * that dialog is no longer attached to the view of destroyed
			 * activity.
			 */
        }
    }

    public void removeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && !isFinishing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }




    private void attatchActionBar() {
        final Activity activityContext = this;
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {

                InputMethodManager inputMethodManager = (InputMethodManager) activityContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activityContext.getCurrentFocus().getWindowToken(), 0);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);

        mNotificationMenuItem = menu.findItem(R.id.action_notifivation);
        NotificationView notificationLayout = (NotificationView) MenuItemCompat.getActionView(mNotificationMenuItem);
        mActionbarHelper.initilizeNotificationView(this, notificationLayout);

        mUpdatesNotificationMenuItem = menu.findItem(R.id.action_updates_notification);
        NotificationView updateCountView = (NotificationView) MenuItemCompat.getActionView(mUpdatesNotificationMenuItem);
        mActionbarHelper.initilizeUpdatesCountView(this, updateCountView);

        setUpdatesNotificationVisibility(mIsUpdatesNotificationIconVisible);

        mShareMenuItem = menu.findItem(R.id.action_share);


        mEditMenuItem = menu.findItem(R.id.action_edit);
        EditView editLayout = (EditView) MenuItemCompat
                .getActionView(mEditMenuItem);
        mActionbarHelper.initilizeEditView(this, editLayout);

        mSearchMenuItem = menu.findItem(R.id.action_search);

        *//*if(this instanceof PassbookSearchActivity){
            mSearchMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }*//*

        mSearchView = (SearchView) MenuItemCompat
                .getActionView(mSearchMenuItem);
        mSearchView.setQueryHint(getResources().getString(R.string.search));

        mSignInMenuItem = menu.findItem(R.id.action_sign_in);
        SignInMenuView signInLayout = (SignInMenuView) MenuItemCompat.getActionView
                (mSignInMenuItem);
        mActionbarHelper.initilizeSignInMenuView(this, signInLayout);
        setSignInMenuViewVisibility(false);

        mIconMenuItem = menu.findItem(R.id.action_menu_icon);
        IconMenuView iconMenuView = (IconMenuView) MenuItemCompat.getActionView(mIconMenuItem);
        mActionbarHelper.initializeIconMenuView(this, iconMenuView);

        mQRMenuItem = menu.findItem(R.id.qr_img);

        setNotification();
        return super.onCreateOptionsMenu(menu);
    }*/

    protected void setEditWidth(int width) {
        mActionbarHelper.setEditWidth(width);
    }


    public void setQRIconVisibility(boolean makeVisible) {
        if (mQRMenuItem != null) {
            isQRNeeded = makeVisible;
            mQRMenuItem.setVisible(isQRNeeded);
        }
    }

    public void setUpdatesNotificationVisibility(boolean isVisible) {
        if (mUpdatesNotificationMenuItem != null) {
//            isNotificationNeeded = isVisible;
            mUpdatesNotificationMenuItem.setVisible(isVisible);
            mIsUpdatesNotificationIconVisible = isVisible;
//            setTitle(mTitle);
            //  mActionbarHelper.setNotificationVisibility(isVisible ? View.VISIBLE : View.GONE);

        }
    }

    protected void setEditViewVisibility(boolean isVisible) {
        if (mEditMenuItem != null) {
            mEditMenuItem.setVisible(isVisible);
            mIsEditIconVisible = isVisible;
            setTitle(mTitle);
            isEditViewNeeded = isVisible;
        }
    }
}

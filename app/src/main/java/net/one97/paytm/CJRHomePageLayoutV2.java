
package net.one97.paytm;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CJRHomePageLayoutV2 extends CJRItem {
	
	private static final long serialVersionUID = 1L;
	private boolean mGDREventSent;
	private boolean isSellerStoreImpressionSent;

	@SerializedName("datasources")
	private ArrayList<CJRDataSource> datasources = new ArrayList<>();

	public ArrayList<CJRDataSource> getDatasources() {
		return datasources;
	}

	public void setDatasources(ArrayList<CJRDataSource> datasources) {
		this.datasources = datasources;
	}

	@SerializedName("items")
	private ArrayList<CJRHomePageItem> mHomePageItemList = new ArrayList<CJRHomePageItem>();
	@SerializedName("type")
	private String mLayout;
	@SerializedName("title")
	private String mName;


	@SerializedName("layout_type")
	private String mLayoutType;

	public String getmLayoutType() {
		return mLayoutType;
	}

	public void setmLayoutType(String mLayoutType) {
		this.mLayoutType = mLayoutType;
	}

	public String getmListTitleName() {
		return mListTitleName;
	}

	@SerializedName("name")
	private String mListTitleName;

	public void setmListTitleName(String mListTitleName) {
		this.mListTitleName = mListTitleName;
	}

	@SerializedName("html")
	private String mHtml;
	@SerializedName("header_imageurl")
	private String mHeaderImageData;
	@SerializedName("id")
	private String mID;


	@SerializedName("display_fields")
	private ArrayList<String> displayParams = new ArrayList<>();

	public void setmID(String mID) {
		this.mID = mID;
	}

	private boolean mIsRecommended;
	private boolean mIsViewExpanded;

	@SerializedName("see_all_url")
	private String mSeeAllUrl;
	private String mFooterImageURL;

	//Recharge Summary-- Start
	public String getPrice()
	{
		return "";
	}
	public String getThumbnailUrl()
	{
		return "";
	}
	public String getStatusText() {
		return "";
	}
	public String getPromoCode() {
		return "";
	}
	public String getPromoText() {
		return "";
	}
	public boolean isExpanded() {
		return false;
	}
	public String getStatusSubText() {
		return "";
	}
	public boolean isRetryEnabled() {
		return false;
	}
	public String getCardState() {
		return "";
	}
	public boolean isKeepTryingEnabled() {
		return false;
	}
	public boolean isPendingCancelEnabled() {
		return false;
	}
	//Recharge Summary-- End

	private String qrCode;
	private String affiliateID;
	private String deeplink;
	private String timeStamp;


	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getAffiliateID() {
		return affiliateID;
	}

	public void setAffiliateID(String affiliateID) {
		this.affiliateID = affiliateID;
	}

	public String getDeeplink() {
		return deeplink;
	}

	public void setDeeplink(String deeplink) {
		this.deeplink = deeplink;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public void setmHomePageItemList(ArrayList<CJRHomePageItem> mHomePageItemList) {
		this.mHomePageItemList = mHomePageItemList;
	}

    public boolean ismIsRecommended() {
        return mIsRecommended;
    }

	public String getmID() {
		return mID;
	}

	public ArrayList<CJRHomePageItem> getHomePageItemList() {
		for(int i=0;i<mHomePageItemList.size();i++){
			mHomePageItemList.get(i).setListId(getmID());
		}
		return mHomePageItemList;
	}

	public String getLayout() {
		if(getmLayoutType()!=null){
			return getmLayoutType();
		}
		return mLayout;
	}

	public void setLayout(String mLayout) {
		this.mLayout = mLayout;
	}
	
	public String getName() {
		StringBuilder name = new StringBuilder();
		if(!TextUtils.isEmpty(mName)) {
			name.append(mName);
		}
		else name.append(mListTitleName);
		return name.toString();//.toUpperCase();
	}

	public void setName(String mName) {
		this.mName = mName;
	}
	
	public String getHtml() {
		return mHtml;
	}

	public String getHeaderImage() {
		return mHeaderImageData;
	}

	public String getSeeAllUrl() {
		return mSeeAllUrl;
	}

	public String getURL() {
		return mSeeAllUrl;
	}

	@Override
	public String getURLType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBrand() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getItemID() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getListName() {
		return null;
	}

	@Override
	public int getListPosition() {
		return -1;
	}

	@Override
	public String getSearchType() {
		return null;
	}

	@Override
	public String getSearchCategory() {
		return null;
	}

	@Override
	public String getSearchTerm() {
		return null;
	}

	@Override
	public String getSearchResultType() {
		return null;
	}

	@Override
	public String getCategoryId() {
		return null;
	}

	public void setUrlType(String layout) {
		mLayout = layout;
	}

	public String getFooterImageURL() {
		return mFooterImageURL;
	}

	public void setFooterImageURL(String footerImageURL) {
		this.mFooterImageURL = footerImageURL;
	}

	public void setIsRecommended(boolean isRecommended) {
		mIsRecommended = isRecommended;
	}

	public void setGDREventSent() {
		mGDREventSent = true;
	}

	public boolean isGDREventSent(){
		return mGDREventSent;
	}

	public boolean isSellerStoreImpressionSent() {
		return isSellerStoreImpressionSent;
	}

	public void setSellerStoreImpressionSent(boolean mISSellerStoreImpressionSent) {
		this.isSellerStoreImpressionSent = mISSellerStoreImpressionSent;
	}
	public String getListId() {
		return mListId;
	}

	public void setListId(String mListId) {
		this.mListId = mListId;
	}

	private String mListId="";

	@Override
	public String getmContainerInstanceID() {
		return "";
	}

	@Override
	public String getParentID() {
		return "";
	}

	public String getSource() {
		return null;
	}

	@Override
	public String getSearchABValue() {
		return null;
	}

	public ArrayList<String> getDisplayParams() {
		return displayParams;
	}

	public boolean isViewExpanded() {
		return mIsViewExpanded;
	}

	public void setViewExpanded(boolean expanded) {
		mIsViewExpanded = expanded;
	}
}
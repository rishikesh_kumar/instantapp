
package net.one97.paytm;

import android.net.Uri;

import net.android.volley.AuthFailureError;
import net.android.volley.DefaultRetryPolicy;
import net.android.volley.NetworkResponse;
import net.android.volley.Request;
import net.android.volley.Response;
import net.android.volley.Response.ErrorListener;
import net.android.volley.Response.Listener;
import net.android.volley.RetryPolicy;
import net.android.volley.VolleyError;
import net.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;


import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class NetworkRequest extends Request<IJRDataModel> {

    private final Listener<IJRDataModel> mListener;
    private IJRDataModel mDataModel;
    private final Gson mGson;
    private String TAG = NetworkRequest.class.getName();
    public static final int MY_SOCKET_TIMEOUT_MS = 60000;
    private String mUrl;
    private String cacheKey = "";
    private Map<String, String> mHeaders;
    private String mRequestBody;
    private String uniqueReference="";
    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";

    public NetworkRequest(String url, Listener<IJRDataModel> listener, ErrorListener
            errorListener, IJRDataModel model, int methodType, Map<String, String> headers, String requestBody) {
        super(methodType, UrlUtils.appendSiteIdInUrl(url), errorListener);
        url = UrlUtils.appendSiteIdInUrl(url);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mUrl = url;
        setKey(mUrl);
        mHeaders = headers;
        mRequestBody = requestBody;

    }

    public NetworkRequest(String url, Listener<IJRDataModel> listener, ErrorListener
            errorListener, IJRDataModel model, int methodType) {
        super(methodType, UrlUtils.appendSiteIdInUrl(url), errorListener);
        url = UrlUtils.appendSiteIdInUrl(url);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mUrl = url;
        setKey(mUrl);
    }

    @Override
    public String getOriginUrl() {
        return cacheKey;
    }

    public void setKey(String url) {
        try {
            Uri uri = Uri.parse(url);
            Uri.Builder builder = uri.buildUpon().clearQuery();
            url = builder.build().toString();
        }catch (Exception e){

        }
        /*
        String prepareUrl = url.replaceAll("[&?]lat.*?(?=&|\\?|$)", "");
        prepareUrl = prepareUrl.replaceAll("[&?]long.*?(?=&|\\?|$)", "");*/
        cacheKey = url;
    }


    @Override
    protected void deliverResponse(IJRDataModel model) {
        mListener.onResponse(model);
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return super.setRetryPolicy(retryPolicy);
    }


    @Override
    protected Response<IJRDataModel> parseNetworkResponse(NetworkResponse response) {
        String jsonString = "";
        int statusCode = response.statusCode;
        Reader reader = null;
        String encoding="";

        try {

            encoding = response.headers.get("Content-Encoding");
            if(response.data!=null) {
                if (encoding != null && encoding.equals("gzip")) {
                    reader = GzipUtils.convertReader(response.data);
                } else jsonString = new String(response.data);
            }
            if (statusCode != CJRConstants.SUCCESS_CODE_200) {
                if (encoding != null && encoding.equals("gzip"))
                    jsonString = GzipUtils.convertString(reader);

                if (statusCode == CJRConstants.ERROR_CODE_403) {
                    statusCode = CJRConstants.ERROR_CODE_401;
                }
                if (statusCode == CJRConstants.ERROR_CODE_401 || statusCode == CJRConstants
                        .ERROR_CODE_410) {
                    throw new IllegalErrorCode(jsonString);
                }
                if (statusCode == CJRConstants.ERROR_CODE_499 || statusCode == CJRConstants
                        .ERROR_CODE_502
                        || statusCode == CJRConstants.ERROR_CODE_503 || statusCode == CJRConstants
                        .ERROR_CODE_504) {
                    throw new IllegalErrorCode(jsonString);
                }
                if (encoding != null && encoding.equals("gzip")) {
                    if (reader == null) throw new IllegalStateException();
                } else if (jsonString == null || jsonString.trim().length() == 0)
                    throw new IllegalStateException();
                try {

                    CJRIllegalCodeError errorStatus = new CJRIllegalCodeError();
                    errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());



                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            IJRDataModel dataModel = null;
                dataModel = mGson.fromJson(jsonString, mDataModel.getClass());

            return Response.success(dataModel,
                    HttpHeaderParser.parseCacheHeaders(response));

        } catch (IllegalErrorCode e) {

        } catch (Exception exception) {
            exception.printStackTrace();
            VolleyError volleyError = new VolleyError(CJRConstants.PARSE_ERROR);
            volleyError.setUrl(mUrl);
            volleyError.setUniqueReference(uniqueReference);

            /*try {
                if (encoding != null && encoding.equals("gzip") && reader!=null)
                {
                    jsonString = GzipUtils.convertString(reader);
                }

                //new CJRParsingError().parsingError(mUrl,exception,response,jsonString);

            } catch (Exception e) {
                e.printStackTrace();
            }*/


            return Response.error(volleyError);
        }
        return null;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? super.getBody() : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            return null;
        }
    }

    public String getUniqueReference() {
        return uniqueReference;
    }

    public void setUniqueReference(String uniqueReference) {
        this.uniqueReference = uniqueReference;
    }
}


package net.one97.paytm;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.VisibleForTesting;

import net.android.volley.Cache;
import net.android.volley.Network;
import net.android.volley.Request;
import net.android.volley.RequestQueue;
import net.android.volley.toolbox.BasicNetwork;
import net.android.volley.toolbox.DiskBasedCache;
import net.android.volley.toolbox.HurlStack;
import net.android.volley.toolbox.Volley;
import com.google.gson.Gson;


import java.io.File;
import java.io.Reader;

public class CJRVolley {

    private static RequestQueue mRequestQueue;
    private static RequestQueue mImageRequestQueue;
    private static String TAG = CJRVolley.class.getSimpleName();
    private static Context mContext;
    private static Gson mGson;


    @VisibleForTesting
    static final String CACHE_DIRECTORY_NAME = "paytm-volley-cache";

    private CJRVolley() {
    }

    /**
     * initialize Volley
     */
    public static void init(Context context) {
        mGson = new Gson();
        mContext = context;
        getRequestQueue(context);
        mImageRequestQueue = Volley.newRequestQueue(context, true);
        ImageCacheManager.INSTANCE.initImageCache();
    }

    public static <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setShouldCache(true);
        getRequestQueue(mContext).add(req);
    }

    public  static <T> Request<T> addSyncRequest(Request<T> req){
        return getRequestQueue(mContext).add(req);
    }

    public static RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            File volleyCacheDir = new File(context.getCacheDir().getPath() + File.separator
                    + CACHE_DIRECTORY_NAME);

            Cache cache = new DiskBasedCache(volleyCacheDir, (int) DeviceUtils.diskCacheSizeBytes
                    (volleyCacheDir, 10 * 1024 * 1024));
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
        }

        return mRequestQueue;
    }

    public static RequestQueue getImageRequestQueue() {

        if (mImageRequestQueue != null) {
            return mImageRequestQueue;
        } else {
            throw new IllegalStateException("Image RequestQueue not initialized");
        }
    }

    public static IJRDataModel getCachedData(String url, Context mContext, IJRDataModel dataModel) {
        IJRDataModel mDataModel = null;
        try {
            Cache cache = CJRVolley.getRequestQueue(mContext).getCache();
            Cache.Entry entry = cache.get(getkey(url));
            if (entry != null) {
                String jsonString = "";
                if (mGson == null)
                    mGson = new Gson();

                String encoding = entry.responseHeaders.get("Content-Encoding");
                if (entry.data != null) {
                    if (encoding != null && encoding.equals("gzip")) {

                        Reader reader = GzipUtils.convertReader(entry.data);
                        mDataModel = mGson.fromJson(reader, dataModel.getClass());
                    } else {
                        jsonString = new String(entry.data, "UTF-8");
                        mDataModel = mGson.fromJson(jsonString, dataModel.getClass());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDataModel;
    }

    private static String getkey(String url) {
        try {
            Uri uri = Uri.parse(url);
            Uri.Builder builder = uri.buildUpon().clearQuery();
            url = builder.build().toString();
        }catch (Exception e){

        }

     /*   String prepareUrl = url.replaceAll("[&?]lat.*?(?=&|\\?|$)", "");
        prepareUrl = prepareUrl.replaceAll("[&?]long.*?(?=&|\\?|$)", "");
        try {
            Uri uri = Uri.parse(prepareUrl);
            final Set<String> params = uri.getQueryParameterNames();
            final Uri.Builder newUri = uri.buildUpon().clearQuery();
            String key = "networkType";
            for (String param : params) {
                String value;
                if (!param.equals(key)) {
                    value = uri.getQueryParameter(param);
                    newUri.appendQueryParameter(param, value);
                }
            }

            prepareUrl = newUri.build().toString();
        }catch (Exception e){

        }*/
        return url;
    }

}

package net.one97.paytm;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Praveen Kumar on 12/15/2015.
 */
public class CarouselHorizontalListAdapter extends CJRBaseAdapter {

    private ArrayList<CJRHomePageItem> mItemList;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private String mParent;
    private String mScreenName;
    private String mcontainerInstanceId;
    private String mchildSiteId;
    private String layoutType;
    private boolean isSendViewEvent = false;

    ArrayList<String> viewedItemIds = new ArrayList<>();

    private String containerId;


    public CarouselHorizontalListAdapter(Activity activity, ArrayList<CJRHomePageItem> itemList,
                                         String layout, String parent, String screenName, String
                                                 containerInstanceID) {
        super();
        mItemList = itemList;
        mInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mParent = parent;
        mScreenName = screenName;
        mcontainerInstanceId = containerInstanceID;
    }

    public CarouselHorizontalListAdapter(Activity activity, ArrayList<CJRHomePageItem> itemList,
                                         String layout, String parent, String screenName, String
                                                 containerInstanceID, String mchildSiteId) {
        super();
        mItemList = itemList;
        mInflater = LayoutInflater.from(activity);
        mActivity = activity;
        mParent = parent;
        mScreenName = screenName;
        mcontainerInstanceId = containerInstanceID;
        this.mchildSiteId = mchildSiteId;
        layoutType = layout;

    }


    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CJRHomePageItem cjrHomePageItem = mItemList.get(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.carousel_horizontal_list_item, null);
            ViewHolder holder = new ViewHolder();
            holder.itemImage = (ImageView) convertView.findViewById(R.id.item_image);
            holder.progressbar = (ProgressBar) convertView.findViewById(R.id.image_progress_bar);
            convertView.setTag(holder);
        }
        final ViewHolder holder = (ViewHolder) convertView.getTag();

        mItemList.get(position).setParentItem(mParent);

        if (!mItemList.get(position).isItemViewed()) {
            mItemList.get(position).setItemViewed();
            if (mcontainerInstanceId != null)
                mItemList.get(position).setmContainerInstanceID(mcontainerInstanceId);
            else mItemList.get(position).setmContainerInstanceID("");


            if (mchildSiteId != null) {

                String slotType, viewIdentifier;
                if (CJRAppUtils.getTabCurrentPosition() == 0) slotType = "HS2VS1";
                slotType = "HS2VS2";
                viewIdentifier = CJRAppUtils.getBrandTag() + "_" + slotType + "_" + layoutType;
                CJRHomePageItem item = mItemList.get(position);
                if (item != null) {
                    Map<String, Object> sourceInfo = new HashMap<String, Object>();
                    sourceInfo.put(CJRConstants.SOURCE_CONTAINER_INSTANCE_ID_KEY, mcontainerInstanceId);
                    sourceInfo.put(CJRConstants.SOURCE_CONTAINER_ID_KEY, containerId);
                    item.setSourceInfo(sourceInfo);
                }
                //CJRSendGTMTag.sendBrandPromotionImpression(item, mActivity, position,"/BrandStore", mchildSiteId,viewIdentifier);

            } else {
                if (isSendViewEvent) {

                    if (!viewedItemIds.contains(cjrHomePageItem.getItemID())) {
                        viewedItemIds.add(cjrHomePageItem.getItemID());
                        CJRHomePageItem item = mItemList.get(position);
                        if (item != null) {
                            Map<String, Object> sourceInfo = new HashMap<String, Object>();
                            sourceInfo.put(CJRConstants.SOURCE_CONTAINER_INSTANCE_ID_KEY, mcontainerInstanceId);
                            sourceInfo.put(CJRConstants.SOURCE_CONTAINER_ID_KEY, containerId);
                            item.setSourceInfo(sourceInfo);
                        }

                    }
                }
            }
        }

        int imageWidth = (int) mActivity.getResources().getDimension(R.dimen
                .carousel_horizontal_list_width);
        int imageHeight = (int) mActivity.getResources().getDimension(R.dimen
                .carousel_horizontal_list_height);

        String imageUrl=null;
        imageUrl = mItemList.get(position).getImageUrl();

        if (imageUrl != null) {
            imageUrl = getImageUrl(mActivity, imageUrl, CJRConstants
                            .IMAGE_TYPE_HOMEPAGE,
                    imageWidth, imageHeight);

            //holder.progressbar.setVisibility(View.VISIBLE);

            Picasso.with(mActivity)
                    .load(imageUrl)
                    .into(holder.itemImage);

        }
        //else holder.progressbar.setVisibility(View.GONE);
        return convertView;
    }


    public static String getImageUrl(Context context, String defaultImageUrl,
                                     int imageType, int width, int height) {
        width = Math.abs(width);
        height = Math.abs(height);

        StringBuilder imageDimension = new StringBuilder();
        imageDimension.append(width).append("x").append(height).append("/");

//		if(CJRConstants.IMAGE_TYPE_HOMEPAGE==imageType){
//			return createImageUrlJPG(defaultImageUrl, imageDimension.toString());
//		}

        return createImageUrl(defaultImageUrl, imageDimension.toString());

		/*
         * switch (imageType) { case CJRConstants.IMAGE_TYPE_CAROUSEL_1: return
		 * createImageUrl(defaultImageUrl, getCarousel1ImageParam(activity));
		 *
		 *
		 * case CJRConstants.IMAGE_TYPE_CAROUSEL_2: return
		 * createImageUrl(defaultImageUrl, getCarousel2ImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_PRODUCT_ROW: return
		 * createImageUrl(defaultImageUrl, getProductRowImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_HEADER: return
		 * createImageUrl(defaultImageUrl, getHeaderImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_GRID: return
		 * createImageUrl(defaultImageUrl, getGridImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_PRODUCT_DETAIL: return
		 * createImageUrl(defaultImageUrl,
		 * getProductDetailImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_FOOTER: return
		 * createImageUrl(defaultImageUrl, getFooterImage(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_MERCHANT_LOGO: return
		 * createImageUrl(defaultImageUrl, getMerchantLogo(activity));
		 *
		 * default: return defaultImageUrl; }
		 */
    }


    private static String getCarousel1ImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.CAROUSEL_1_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.CAROUSEL_1_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.CAROUSEL_1_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.CAROUSEL_1_XHDPI_PARAM;

            default:
                return null;
        }
    }



    private static String getFooterImage(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.FOOTER_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.FOOTER_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.FOOTER_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.FOOTER_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getMerchantLogo(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.MERCHANT_LOGO_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.MERCHANT_LOGO_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.MERCHANT_LOGO_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.MERCHANT_LOGO_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getProductDetailImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.PRODUCT_DETAIL_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.PRODUCT_DETAIL_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.PRODUCT_DETAIL_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.PRODUCT_DETAIL_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getGridImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.GRID_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.GRID_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.GRID_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.GRID_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getHeaderImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.HEADER_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.HEADER_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.HEADER_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.HEADER_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getProductRowImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.PRODUCT_ROW_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.PRODUCT_ROW_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.PRODUCT_ROW_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.PRODUCT_ROW_XHDPI_PARAM;

            default:
                return null;
        }
    }

    public static int getResolutionType(Activity activity) {
        if (activity == null || activity.isFinishing()) {
            return 0;
        } else {
            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int density = dm.densityDpi;
            if (density == DisplayMetrics.DENSITY_LOW) {
                return CJRConstants.SCREEN_DENSITY_LOW;
            } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
                return CJRConstants.SCREEN_DENSITY_MEDIUM;
            } else if (density == DisplayMetrics.DENSITY_HIGH) {
                return CJRConstants.SCREEN_DENSITY_HIGH;
            } else {
                return CJRConstants.SCREEN_DENSITY_EXTRA_HIGH;
            }
        }
    }

    public static final String IMAGE_FORMAT_WEBP = ".webp";

    private static String createImageUrl(String imageUrl, String resolution) {
        if (imageUrl == null)
            return null;
        StringBuilder image = new StringBuilder(imageUrl);
        image.insert(image.lastIndexOf("/") + 1, resolution);

        if (WebPUtils.isWebPSupported()) {
            image.append(IMAGE_FORMAT_WEBP);
        }
        return image.toString();
    }

    public void setItemList(ArrayList<CJRHomePageItem> itemList, String layout){
        mItemList = itemList;
        layoutType = layout;
        notifyDataSetChanged();
    }

    public ArrayList<CJRHomePageItem> getData() {
        return mItemList;
    }

    public void clearData() {
        if (mItemList != null) {
            mItemList.clear();
            notifyDataSetChanged();
        }
    }

    public static class ViewHolder {
        ImageView itemImage;
        ProgressBar progressbar;
    }

    public void setSendViewEvent(boolean viewEvent) {
        this.isSendViewEvent = viewEvent;
    }

    public void setContainerId(String containerId){
        this.containerId = containerId;
    }
}

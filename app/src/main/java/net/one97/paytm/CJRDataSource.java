package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

/**
 * Created by rohitraj on 20/6/16.
 */
public class CJRDataSource implements IJRDataModel {


    @SerializedName("type")
    private String mType;
    @SerializedName("list_id")
    private String mListId;
    @SerializedName("container_id")
    private String mContainerID;
    @SerializedName("container_instance_id")
    private String mContainerInstanceID;


    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmListId() {
        return mListId;
    }

    public void setmListId(String mListId) {
        this.mListId = mListId;
    }

    public String getmContainerID() {
        return mContainerID;
    }

    public void setmContainerID(String mContainerID) {
        this.mContainerID = mContainerID;
    }

    public String getmContainerInstanceID() {
        return mContainerInstanceID;
    }

    public void setmContainerInstanceID(String mContainerInstanceID) {
        this.mContainerInstanceID = mContainerInstanceID;
    }
}

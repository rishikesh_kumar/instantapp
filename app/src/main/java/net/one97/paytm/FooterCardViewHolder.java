package net.one97.paytm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Created by Adarsh Chithran on 4/25/2016.
 */
public class FooterCardViewHolder extends RecyclerView.ViewHolder implements UpdateViewHolder {

    Context mContext;
    private TextView footerText,supportText;
    private View convertView=null;

    public FooterCardViewHolder(Context mContext, View convertView) {
        super(convertView);
        this.mContext=mContext;
        this.convertView=convertView;
        footerText = (TextView) convertView.findViewById(R.id.paytm_trust_text);
        supportText = (TextView) convertView.findViewById(R.id.paytm_contact_us);
    }

    @Override
    public void updateViewHolder(Context context, final CJRHomePageLayoutV2 pageLayout, final String
            mGAKey) {

        if (footerText != null && pageLayout.getName() != null) {
            footerText.setText(pageLayout.getName());
        }

        if(pageLayout.isExpanded())
        {
            convertView.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;
        }

        if(supportText!=null)
        {
            this.supportText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }
    }
}

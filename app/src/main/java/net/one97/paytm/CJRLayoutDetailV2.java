package net.one97.paytm;


import com.google.gson.annotations.SerializedName;

/**
 * Created by bheemesh on 4/5/16.
 *
 * @author bheemesh
 */
public class CJRLayoutDetailV2 implements IJRDataModel {
    @SerializedName("label")
    private String label;
    @SerializedName("label_text_color")
    private String labelColor;
    @SerializedName("label_bgcolor")
    private String labelBgColor;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabelColor() {
        return labelColor;
    }

    public void setLabelColor(String labelColor) {
        this.labelColor = labelColor;
    }

    public String getLabelBgColor() {
        return labelBgColor;
    }

    public void setLabelBgColor(String labelBgColor) {
        this.labelBgColor = labelBgColor;
    }
}

package net.one97.paytm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import net.android.volley.Response.ErrorListener;
import net.android.volley.Response.Listener;
import net.android.volley.VolleyError;

import java.util.ArrayList;

public abstract class FJRMainBaseFragment extends Fragment
		implements Listener<IJRDataModel>, ErrorListener,
		IJROnFileReadWriteListener,
		EditView.OnEditViewClickListener {
	protected ProgressDialog mProgressDialog;
	protected CJRCatalog mCatalog;
	protected String mFrequentOrderUrl;
	protected ProgressBar mProgessBarItemLoading;
	protected RelativeLayout homepage_loading;

	public FJRMainBaseFragment(){}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (getArguments() != null && getArguments().getSerializable("CACHED_CATALOG_DATA") != null) {
			mCatalog = (CJRCatalog) getArguments().getSerializable("CACHED_CATALOG_DATA");
		}
	}




	@Override
	public void onFileWriteComplete(String fileHeader) {

	}


	public void loadPage(String urlType, IJRDataModel dataModel,
                         String parentActivity, int position,
                         ArrayList<? extends CJRItem> itemList, boolean isFromCatalog, String origin, String mlogo, String mbanner, String mtitle, String mBadgeImageUrl, String childSiteId) {

		if (getActivity() == null || isDetached()) {
			return;
		}

		if(!TextUtils.isEmpty(urlType) && urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_NO_LINK)){
			return;
		}

		if (mProgessBarItemLoading != null) {
			mProgessBarItemLoading.setVisibility(View.VISIBLE);
		}

		//CJRActivityNavigation.loadpdpBrand(getActivity(), urlType,dataModel,parentActivity, position, itemList,  isFromCatalog, origin, null,mlogo,mbanner,mtitle,mBadgeImageUrl,childSiteId);

	}




	public void loadPage(String urlType, IJRDataModel dataModel,
                         String parentActivity, int position,
                         ArrayList<? extends CJRItem> itemList, boolean isFromCatalog, String origin) {

		if (getActivity() == null || isDetached()) {
			return;
		}

		if(!TextUtils.isEmpty(urlType) && urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_NO_LINK)){
			return;
		}

		if (mProgessBarItemLoading != null) {
			mProgessBarItemLoading.setVisibility(View.VISIBLE);
		}

		//CJRActivityNavigation.loadPage(getActivity(), urlType,dataModel,parentActivity, position, itemList,  isFromCatalog, origin, null);

	}


	public void loadBrandPdpPage(String urlType, IJRDataModel dataModel,
                                 String parentActivity, int position,
                                 ArrayList<? extends CJRItem> itemList, boolean isFromCatalog, String origin, String mlogo, String mbanner, String mtitle, String mBadgeImageUrl, String mchildSiteId) {

		if (getActivity() == null || isDetached()) {
			return;
		}

		if(!TextUtils.isEmpty(urlType) && urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_NO_LINK)){
			return;
		}

		if (mProgessBarItemLoading != null) {
			mProgessBarItemLoading.setVisibility(View.VISIBLE);
		}

		//CJRActivityNavigation.loadpdpBrand(getActivity(), urlType,dataModel,parentActivity, position, itemList,  isFromCatalog, origin, null,mlogo,mbanner,mtitle, mBadgeImageUrl,mchildSiteId);


	}


	@Override
	public void onResponse(IJRDataModel response) {

	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
	}




	public void loadData() {
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public abstract void updateData(CJRItem item);

	public void onActivityResult(int requestCode, Intent data) {
	}

	public void setOperatorClickable(boolean isClickable) {

	}

	public void clearFields() {

	}

	public void setiReffData(CJRHomePageItem item) {

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//mUserChangeListener = null;
	}


	@Override
	public void onDestroyView() {
		super.onDestroyView();
		//mUserChangeListener = null;
	}

	public abstract void onServerDataLoaded();

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}

	public void onRefreshFragment() {

	}
}


package net.one97.paytm; /**
 * Copyright C 2012, One97 Communications Ltd. All rights reserved.
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 *//*


package com.example.rohitraj.instantapp;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.provider.Settings;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Response.ErrorListener;
import Response.Listener;
import VolleyError;
import com.appsflyer.AppsFlyerLib;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.urbanairship.UAirship;

import net.one97.paytm.AJRCSTContactUs;
import net.one97.paytm.AJRContactUsSubmit;
import net.one97.paytm.AJREmbedWebView;
import net.one97.paytm.AJRGasActivity;
import net.one97.paytm.AJRItemLevelOrder;
import net.one97.paytm.AJRLandlineActivity;
import net.one97.paytm.AJRMetroCardRecharge;
import net.one97.paytm.AJRNonKYCDeeplink;
import net.one97.paytm.AJROrderSummaryActivity;
import net.one97.paytm.AJRProfileActivity;
import net.one97.paytm.AJRRechargeActivity;
import net.one97.paytm.AJRRechargeUtilityActivity;
import net.one97.paytm.AJRSearchActivity;
import net.one97.paytm.AJRSecondaryHome;
import net.one97.paytm.AJRSecuritySettings;
import net.one97.paytm.amusementpark.activity.AJRAmParkHomePage;
import net.one97.paytm.app.CJRJarvisApplication;
import net.one97.paytm.app.CJRVolley;
import net.one97.paytm.auth.activity.AJRAuthActivity;
import net.one97.paytm.auth.utils.CJRSessionManager;
import net.one97.paytm.brandStore.activity.AJRRecyclerBrandStoreActivity;
import net.one97.paytm.busticket.activity.AJRBusSearchActivity;
import net.one97.paytm.cart.activities.AJRShoppingCartActivity;
import net.one97.paytm.common.CJRGsonGetRequest;
import net.one97.paytm.common.entity.CJRContingency;
import net.one97.paytm.common.entity.CJRError;
import net.one97.paytm.common.entity.CJRItem;
import net.one97.paytm.common.entity.CJRRechargeCart;
import net.one97.paytm.common.entity.CJRSelectCityModel;
import net.one97.paytm.common.entity.CJRUserDefaultInfo;
import net.one97.paytm.common.entity.CJRUserInfo;
import net.one97.paytm.common.entity.CJRUserInfoV2;
import net.one97.paytm.common.entity.IJRDataModel;
import net.one97.paytm.common.entity.flightticket.CJRFlightSearchTabItem;
import net.one97.paytm.common.entity.recharge.CJRFrequentOrder;
import net.one97.paytm.common.entity.recharge.CJRRechargeProductList;
import net.one97.paytm.common.entity.replacement.CJRReplacementReason;
import net.one97.paytm.common.entity.shopping.CJRCartProduct;
import net.one97.paytm.common.entity.shopping.CJRHomePageItem;
import net.one97.paytm.common.entity.shopping.CJRHomePageLayoutV2;
import net.one97.paytm.common.net.CJRDefaultRequestParam;
import net.one97.paytm.common.utility.CJRAppCommonUtility;
import net.one97.paytm.common.utility.CJRConstants;
import net.one97.paytm.common.utility.CJRGTMConstants;
import net.one97.paytm.common.utility.CJRSecureSharedPreferences;
import net.one97.paytm.common.utility.CJRServerCommonUtility;
import net.one97.paytm.common.utility.LocaleHelper;
import net.one97.paytm.common.utility.PaytmLogs;
import net.one97.paytm.common.utility.WebPUtils;
import net.one97.paytm.cst.activity.AJRCSTIssueDetail;
import net.one97.paytm.cst.activity.AJRCSTOrderIssues;
import net.one97.paytm.dmrc.AJRDMRCSourceActivity;
import net.one97.paytm.events.activity.AJREventsHomePage;
import net.one97.paytm.giftcards.activity.AJRGiftCardHomePage;
import net.one97.paytm.gold.activity.AJRGoldHomeActivity;
import net.one97.paytm.gtm.GTMLoader;
import net.one97.paytm.landingpage.activity.AJRMainActivity;
import net.one97.paytm.marketplace.activity.AJRGridPageContainer;
import net.one97.paytm.movies.activity.AJRIMAXCinemasPage;
import net.one97.paytm.movies.activity.AJRLocationSelectionActivity;
import net.one97.paytm.movies.activity.AJRMoviesHomePage;
import net.one97.paytm.movies.activity.AJRlocationSelectionDialog;
import net.one97.paytm.movies.utils.CJRSeatCommonUtils;
import net.one97.paytm.orders.activity.AJRTrainOrderSummary;
import net.one97.paytm.orders.activity.NewOrderSummary;
import net.one97.paytm.paymentsBank.activity.PBKYCRequestInviteActivity;
import net.one97.paytm.paymentsBank.activity.PBNonKycInfoActivity;
import net.one97.paytm.paymentsBank.pref.AccountStatus;
import net.one97.paytm.paymentsBank.pref.BankSharedPrefs;
import net.one97.paytm.pdp.activity.AJRProductDetail;
import net.one97.paytm.smoothpay.activity.MerchantPayActivity;
import net.one97.paytm.social.activity.AJRBlogWebContent;
import net.one97.paytm.social.activity.AJRComments;
import net.one97.paytm.social.activity.AJRFeeds;
import net.one97.paytm.social.activity.AJRHoroscopeDetailActivity;
import net.one97.paytm.social.activity.AJRPagerCards;
import net.one97.paytm.social.activity.AJRStackCards;
import net.one97.paytm.social.activity.AJRStoryFeeds;
import net.one97.paytm.social.activity.AJRVideoDetailActivity;
import net.one97.paytm.trainticket.utils.CJRTrainConstants;
import net.one97.paytm.travels.activity.AJRTravelsHomeActivity;
import net.one97.paytm.upgradeKyc.activity.UpgraKycInfoActivity;
import net.one97.paytm.upgradeKyc.activity.UpgradeKycActivity;
import net.one97.paytm.wallet.activity.AJRQRActivity;
import net.one97.paytm.wallet.activity.AJRWalletActivity;
import net.one97.paytm.wallet.newdesign.acceptmoney.AcceptPaymentModeSelectActivity;
import net.one97.paytm.wallet.newdesign.activity.PaySendActivity;
import net.one97.paytm.wallet.newdesign.nearby.NearByInfoActivity;
import net.one97.paytm.wallet.newdesign.nearby.NearByMainActivity;
import net.one97.paytm.wallet.newdesign.passbook.PassbookMainActivity;
import net.one97.paytm.wallet.newdesign.utils.CustomTypefaceSpan;
import net.one97.paytm.wallet.newdesign.utils.WalletSharedPrefs;
import net.one97.paytm.wallet.utility.CJRWalletConstants;
import net.one97.paytm.wallet.utility.CJRWalletUtility;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CJRAppUtility {

    public static final String IMAGE_FORMAT_WEBP = ".webp";
    private static final String AUTHORIZE_BODY = "response_type=code&do_not_redirect=true&scope=paytm";
    private static final String AUTHORIZE_CLIENT = "&client_id=";

    private static final String VALUE_CLIENT_ID = BuildConfig.CLIENT_ID;

    private static final String VALUE_CLIENT_SECRET_CODE = BuildConfig.CLIENT_SECRET_CODE;

    private static final String AUTHORIZATION_VALUE = BuildConfig.AUTHORIZATION_VALUE;
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    //53b133105f61a99844c20733a393778d    //staging
    //690mw4l105f61a99844c20ui56ni3s78d    //production
    private static final String HMAC_CLIENT_SECRET = BuildConfig.HMAC_CLIENT_SECRET;
    public static TelephonyManager inTelephonyManager;
    public static String deviceID;
    private static String TAG = "paytm";
    private static ProgressDialog mProgressDialog;

    public static boolean isPaytmDeeplink(String deepLink) {
            return deepLink.startsWith("paytmmp://") || deepLink.startsWith("paytmmall://");
    }

    public static boolean isPaytmUrl(String urlString) {
        if (isValidUrl(urlString)) {
            try {
                URL url = new URL(urlString);
                Pattern p = Pattern.compile(CJRConstants.PAYTM_URL_REGEX);
                Matcher m = p.matcher(url.getProtocol() + "://" + url.getHost());
                return m.find();
            } catch (MalformedURLException e) {
                if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug)
                    e.printStackTrace();
            }
        }
        return false;
    }

    public static String getHostName(String urlString) {
        try {
            URL url = new URL(urlString);
            return url.getHost();
        } catch (MalformedURLException e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug)
                e.printStackTrace();
        }
        return null;
    }

    public static boolean isValidUrl(String urlString) {
        boolean valid = false;
        try {
            URL url = new URL(urlString);
            valid = true;
        } catch (MalformedURLException e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug)
                e.printStackTrace();
        } finally {
            return valid;
        }
    }

    public static boolean isBankAccountIssued(Context context) {
        return AccountStatus.ISSUED == BankSharedPrefs.getBankAccStatus(context);
    }

    public static boolean canMakeURL(String urlString) {
        Pattern p = Pattern.compile(CJRConstants.GENERAL_URL_REGEX);
        Matcher m = p.matcher(urlString);
        return m.find();
    }

    public static boolean isSameAppDeeplink(String deepLink, Context context) {
        return deepLink.startsWith(context.getString(R.string.schema_name));
    }

    public static String getAppPackageNameToInstall(Context context, String deeplink) {
        String uri = getUriOfApp(deeplink);

        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return "";
        } catch (PackageManager.NameNotFoundException e) {
        }
        return uri;
    }

    private static String getUriOfApp(String deeplink) {
        String uri = "";
        if (deeplink.startsWith("paytmmp")) {
            uri =  "net.one97.paytm";
        } else if (deeplink.startsWith("paytmmall")) {
            uri = "com.paytmmall";
        }
        return uri;
    }


    public static class DateComparator implements Comparator<CJRFrequentOrder> {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        public int compare(CJRFrequentOrder a, CJRFrequentOrder b) {

            try {
                String aLastpaid = a.getPaidOn();
                String bLastPaid = b.getPaidOn();

                Date aDate = sdf.parse(aLastpaid);
                Date bDate = sdf.parse(bLastPaid);

                if (aDate.after(bDate)) {
                    return 1;
                } else if (aDate.before(bDate)) {
                    return -1;
                } else {
                    return 0;
                }


            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }

        }
    }

    @SuppressWarnings("deprecation")
    public static void setViewWidth(Activity activity, View view, float widthPer) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        int width = display.getWidth(); // deprecated
        view.getLayoutParams().width = (int) (width * widthPer);
    }

    @SuppressWarnings("deprecation")
    public static void setViewHeight(Activity activity, View view,
                                     float heightPer) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        int height = display.getHeight(); // deprecated
        view.getLayoutParams().height = (int) (height * heightPer);
    }

    public static int getPixelValue(int dp, Context context) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, r.getDisplayMetrics());
        return px;
    }

    public static int getDpValue(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String getDeviceID(Context context) {
        return ((TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
    }

    public static void log(String tag, String inMsg) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(inMsg)) return;
        CJRAppCommonUtility.log(tag, inMsg);
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (activity != null && !activity.isFinishing()) {
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        return height;
    }

    public static String getDeviceName() {
        return Build.MANUFACTURER;
    }

    public static int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static void disableAlert() {
        CustomDialog.disableDialog();
    }

    public static void showAlert(Context context, String title, String message) {
        if (context == null || message == null) return;
        String ssoToken = CJRServerUtility.getSSOToken(context);
        if (!TextUtils.isEmpty(ssoToken)) {
            ssoToken = "/" + ssoToken;
            if (message.contains(ssoToken)) {
                message = message.replace(ssoToken, "");
            }
        }
        String cartId = CJRServerUtility.getCartID(context);
        if (!TextUtils.isEmpty(cartId)) {
            cartId = "/" + cartId;
            if (message.contains(cartId)) {
                message = message.replace(cartId, "");
            }
        }
        CustomDialog.showAlert(context, title, message);
        //		if (((Activity) context).isFinishing() == false) {
        //			AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //			builder.setTitle(title).setMessage(message).setCancelable(false);
        //			builder.setPositiveButton(android.R.string.ok,
        //					new DialogInterface.OnClickListener() {
        //						public void onClick(DialogInterface dialog, int which) {
        //							dialog.cancel();
        //						}
        //					});
        //			builder.show();
        //		}
    }

    public static boolean isPackageInstall(String packagename, Context context) {
        boolean isInstall = false;
        final PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(packagename,
                    PackageManager.GET_ACTIVITIES);
            isInstall = true;
        } catch (PackageManager.NameNotFoundException e) {
            isInstall = false;
        }

        return isInstall;
    }

	*/
/*public static boolean isVersionMarshmallowAndAbove() {
        return (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
	}*//*


    public static String getPackageName(Context context) {
        String packageName = "";
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            packageName = packageInfo.packageName;
            return packageName;
        } catch (PackageManager.NameNotFoundException ex) {

        }
        return packageName;
    }

    public static InputFilter getFilterForAmount(final int maxDigitsBeforeDecimalPoint, final int maxDigitsAfterDecimalPoint) {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                StringBuilder strBuilder = new StringBuilder(dest);
                strBuilder.append(source);
                if (strBuilder.toString().equals("."))
                    return "";

                StringBuilder builder = new StringBuilder(dest);
                builder.replace(dstart, dend, source
                        .subSequence(start, end).toString());
                if (!builder.toString().matches(
                        "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

                )) {
                    if (source.length() == 0)
                        return dest.subSequence(dstart, dend);
                    return "";
                }

                return null;

            }
        };
        return filter;
    }

    */
/**
     * This method capitalizes first letter of earch word separated by space in string
     *
     * @param str
     * @return
     *//*

    public static String capitalizeFirstLetter(String str) {

        if (TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            String[] words = str.toLowerCase().split(" ");
            StringBuilder ret = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                ret.append(Character.toUpperCase(words[i].charAt(0)));
                ret.append(words[i].substring(1));
                if (i < words.length - 1) {
                    ret.append(' ');
                }
            }
            return ret.toString();
        }catch (Exception e){
            return str;
        }
    }

    */
/**
     * @return Number of bytes available on Internal storage
     *//*

    public static long getAvailableSpaceOnPhone() {
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        availableSpace = (long) stat.getAvailableBlocks()
                * (long) stat.getBlockSize();

        return availableSpace;
    }

    */
/**
     * @return true if SD Container available on Device
     *//*

    public static boolean isSdCardPresent() {

        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    */
/**
     * @return Number of bytes available on External storage
     *//*

    public static long getAvailableSpaceOnSDCard() {
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
                .getPath());
        availableSpace = (long) stat.getAvailableBlocks()
                * (long) stat.getBlockSize();

        return availableSpace;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static boolean isNetworkAvailable(Context context) {
        if (context == null) return false;
        ConnectivityManager ConnectMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (ConnectMgr == null)
            return false;
        NetworkInfo NetInfo = ConnectMgr.getActiveNetworkInfo();
        if (NetInfo == null)
            return false;

        return NetInfo.isConnected();
    }

    public static String getNetworkType(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2g";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                */
/**
                 From this link https://goo.gl/R2HOjR ..NETWORK_TYPE_EVDO_0 & NETWORK_TYPE_EVDO_A
                 EV-DO is an evolution of the CDMA2000 (IS-2000) standard that supports high data rates.

                 Where CDMA2000 https://goo.gl/1y10WI .CDMA2000 is a family of 3G[1] mobile technology standards for sending voice,
                 data, and signaling data between mobile phones and cell sites.
                 *//*

            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                //Log.d("Type", "3g");
                //For 3g HSDPA , HSPAP(HSPA+) are main  networktype which are under 3g Network
                //But from other constants also it will 3g like HSPA,HSDPA etc which are in 3g case.
                //Some cases are added after  testing(real) in device with 3g enable data
                //and speed also matters to decide 3g network type
                //http://goo.gl/bhtVT
                return "3g";
            case TelephonyManager.NETWORK_TYPE_LTE:
                //No specification for the 4g but from wiki
                //I found(LTE (Long-Term Evolution, commonly marketed as 4G LTE))
                //https://goo.gl/9t7yrR
                return "4g";
            default:
                return "Notfound";
        }
    }

    public static boolean isSimCardAvailable(Context context) {
        boolean isAvailable = false;
        TelephonyManager telMgr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {

            case TelephonyManager.SIM_STATE_READY:
                isAvailable = true;
                break;

            case TelephonyManager.SIM_STATE_ABSENT:
                isAvailable = false;
                break;

            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                isAvailable = false;
                break;

            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                isAvailable = false;
                break;

            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                isAvailable = false;
                break;

            case TelephonyManager.SIM_STATE_UNKNOWN:
                isAvailable = false;
                break;
        }

        return isAvailable;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        */
/* it's on the external media. *//*

        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION},
                null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public static int getResolutionType(Activity activity) {
        if (activity == null || activity.isFinishing()) {
            return 0;
        } else {
            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            int density = dm.densityDpi;
            if (density == DisplayMetrics.DENSITY_LOW) {
                return CJRConstants.SCREEN_DENSITY_LOW;
            } else if (density == DisplayMetrics.DENSITY_MEDIUM) {
                return CJRConstants.SCREEN_DENSITY_MEDIUM;
            } else if (density == DisplayMetrics.DENSITY_HIGH) {
                return CJRConstants.SCREEN_DENSITY_HIGH;
            } else {
                return CJRConstants.SCREEN_DENSITY_EXTRA_HIGH;
            }
        }
    }

    private static String getGridImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.GRID_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.GRID_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.GRID_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.GRID_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getHeaderImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.HEADER_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.HEADER_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.HEADER_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.HEADER_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getCarousel1ImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.CAROUSEL_1_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.CAROUSEL_1_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.CAROUSEL_1_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.CAROUSEL_1_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getCarousel2ImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.CAROUSEL_2_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.CAROUSEL_2_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.CAROUSEL_2_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.CAROUSEL_2_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getProductRowImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.PRODUCT_ROW_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.PRODUCT_ROW_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.PRODUCT_ROW_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.PRODUCT_ROW_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getProductDetailImageParam(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.PRODUCT_DETAIL_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.PRODUCT_DETAIL_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.PRODUCT_DETAIL_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.PRODUCT_DETAIL_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getFooterImage(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.FOOTER_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.FOOTER_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.FOOTER_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.FOOTER_XHDPI_PARAM;

            default:
                return null;
        }
    }

    private static String getMerchantLogo(Activity activity) {
        int resolution = getResolutionType(activity);
        switch (resolution) {
            case CJRConstants.SCREEN_DENSITY_LOW:
                return CJRConstants.MERCHANT_LOGO_LDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_MEDIUM:
                return CJRConstants.MERCHANT_LOGO_MDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_HIGH:
                return CJRConstants.MERCHANT_LOGO_HDPI_PARAM;

            case CJRConstants.SCREEN_DENSITY_EXTRA_HIGH:
                return CJRConstants.MERCHANT_LOGO_XHDPI_PARAM;

            default:
                return null;
        }
    }

    public static String getImageUrl(Activity activity, String defaultImageUrl,
                                     int imageType) {

        switch (imageType) {
            case CJRConstants.IMAGE_TYPE_CAROUSEL_1:
                return createImageUrl(defaultImageUrl,
                        getCarousel1ImageParam(activity));

            case CJRConstants.IMAGE_TYPE_CAROUSEL_2:
                return createImageUrl(defaultImageUrl,
                        getCarousel2ImageParam(activity));

            case CJRConstants.IMAGE_TYPE_PRODUCT_ROW:
                return createImageUrl(defaultImageUrl,
                        getProductRowImageParam(activity));

            case CJRConstants.IMAGE_TYPE_HEADER:
                return createImageUrl(defaultImageUrl,
                        getHeaderImageParam(activity));

            case CJRConstants.IMAGE_TYPE_GRID:
                return createImageUrl(defaultImageUrl, getGridImageParam(activity));

            case CJRConstants.IMAGE_TYPE_PRODUCT_DETAIL:
                return createImageUrl(defaultImageUrl,
                        getProductDetailImageParam(activity));

            case CJRConstants.IMAGE_TYPE_FOOTER:
                return createImageUrl(defaultImageUrl, getFooterImage(activity));

            case CJRConstants.IMAGE_TYPE_MERCHANT_LOGO:
                return createImageUrl(defaultImageUrl, getMerchantLogo(activity));

            default:
                return defaultImageUrl;
        }
    }

    public static String getImageUrl(Context context, String defaultImageUrl,
                                     int imageType, int width, int height) {
        width = Math.abs(width);
        height = Math.abs(height);

        StringBuilder imageDimension = new StringBuilder();
        imageDimension.append(width).append("x").append(height).append("/");

//		if(CJRConstants.IMAGE_TYPE_HOMEPAGE==imageType){
//			return createImageUrlJPG(defaultImageUrl, imageDimension.toString());
//		}

        return createImageUrl(defaultImageUrl, imageDimension.toString());

		*/
/*
         * switch (imageType) { case CJRConstants.IMAGE_TYPE_CAROUSEL_1: return
		 * createImageUrl(defaultImageUrl, getCarousel1ImageParam(activity));
		 *
		 *
		 * case CJRConstants.IMAGE_TYPE_CAROUSEL_2: return
		 * createImageUrl(defaultImageUrl, getCarousel2ImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_PRODUCT_ROW: return
		 * createImageUrl(defaultImageUrl, getProductRowImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_HEADER: return
		 * createImageUrl(defaultImageUrl, getHeaderImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_GRID: return
		 * createImageUrl(defaultImageUrl, getGridImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_PRODUCT_DETAIL: return
		 * createImageUrl(defaultImageUrl,
		 * getProductDetailImageParam(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_FOOTER: return
		 * createImageUrl(defaultImageUrl, getFooterImage(activity));
		 *
		 * case CJRConstants.IMAGE_TYPE_MERCHANT_LOGO: return
		 * createImageUrl(defaultImageUrl, getMerchantLogo(activity));
		 *
		 * default: return defaultImageUrl; }
		 *//*

    }

    private static String createImageUrlJPG(String imageUrl, String resolution) {
        if (imageUrl == null)
            return null;
        StringBuilder image = new StringBuilder(imageUrl);
        image.insert(image.lastIndexOf("/") + 1, resolution);

        return image.toString();
    }

    private static String createImageUrl(String imageUrl, String resolution) {
        if (imageUrl == null)
            return null;
        StringBuilder image = new StringBuilder(imageUrl);
        image.insert(image.lastIndexOf("/") + 1, resolution);

        if (WebPUtils.isWebPSupported()) {
            image.append(IMAGE_FORMAT_WEBP);
        }
        return image.toString();
    }

*/
/*	public static String getReadableTime(long aTimestamp) {
        String date = "";
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(aTimestamp);
		date = cal.get(Calendar.DAY_OF_MONTH) + " "
				+ Utils.getMonthName(cal.get(Calendar.MONTH)) + " "
				+ Utils.getHourStr(cal.get(Calendar.HOUR)) + ":"
				+ Utils.getMinStr(cal.get(Calendar.MINUTE))
				+ (cal.get(Calendar.AM_PM) == Calendar.AM ? " am" : " pm");
		return date;
	}

	public static String getReadableDate(long aTimestamp) {
		String date = "";
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(aTimestamp);
		date = getDayOfWeek(cal.get(Calendar.DAY_OF_WEEK)) + ", "
				+ Utils.getMonthName(cal.get(Calendar.MONTH)) + " "
				+ cal.get(Calendar.DAY_OF_MONTH) + ", ";
		return date;

	}

	public static String getReadableHours(long aTimestamp) {
		String date = "";
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(aTimestamp);
		date = Utils.getHourStr(cal.get(Calendar.HOUR)) + ":"
				+ Utils.getMinStr(cal.get(Calendar.MINUTE))
				+ (cal.get(Calendar.AM_PM) == Calendar.AM ? " AM" : " PM");
		return date;

	}*//*


    public static String getGridImageUrl(String imageUrl) {
        if (imageUrl == null || TextUtils.isEmpty(imageUrl))
            return null;
        StringBuilder image = new StringBuilder(imageUrl);
        if (WebPUtils.isWebPSupported()) {
            image.append(IMAGE_FORMAT_WEBP);
        }
        return image.toString();
    }

    public static String getDayOfWeek(int day) {

        switch (day) {

            case Calendar.MONDAY:
                return "Mon";

            case Calendar.TUESDAY:
                return "Tue";

            case Calendar.WEDNESDAY:
                return "Wed";

            case Calendar.THURSDAY:
                return "Thu";

            case Calendar.FRIDAY:
                return "Fri";

            case Calendar.SATURDAY:
                return "Sat";

            case Calendar.SUNDAY:
                return "Sun";

            default:
                return "";

        }
    }

    public static int getScreenGridUnit(Context context) {
        return getScreenWidth((Activity) context)
                / CJRConstants.NUMBER_OF_BOX_IN_ROW;
    }

    public static int getScreenGridUnitBy32(Context context) {
        return getScreenWidth((Activity) context) / 32;
    }

    public static int getScreenGridUnitInHeight(Context context) {
        return getScreenHeight((Activity) context)
                / CJRConstants.NUMBER_OF_BOX_IN_HEIGHT;
    }

    */
/**
     * Used to add default params in url along with ssotoken
     *
     * @param context
     * @param mainUrl
     * @return
     *//*

    public static String addSSOTokenAndAddDefaultParams(Context context,
                                                        String mainUrl) {
        return CJRDefaultRequestParam.getDefaultParamsForProductDetail(mainUrl,
                context);
    }

    public static String addSSOTokenAndAddDefaultParamsWithCusId(Context context, String mainUrl) {
        mainUrl = CJRDefaultRequestParam.getDefaultParamsForProductDetail(mainUrl, context);

        if (CJRSeatCommonUtils.isUserSignedIn(context)) {

            String userId = CJRAppUtility.getUserId(context);

            if (!TextUtils.isEmpty(userId)) {
                mainUrl += ("&custId=" + userId);
            }
        }

        return mainUrl;
    }

    public static String addDefaultParamsWithoutSSOWithCusId(Context context, String mainUrl) {
        mainUrl = CJRDefaultRequestParam.getDefaultParamsForProductDetailWithoutSSO(mainUrl, context);

        if (CJRSeatCommonUtils.isUserSignedIn(context)) {

            String userId = CJRAppUtility.getUserId(context);

            if (!TextUtils.isEmpty(userId)) {
                mainUrl += ("&custId=" + userId);
            }
        }

        return mainUrl;
    }

    public static String buildUrlString(String url, Map<String, String> params) {
        if (!URLUtil.isValidUrl(url)) return url;
        Uri.Builder urlBuilder = Uri.parse(url).buildUpon();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            urlBuilder.appendQueryParameter(entry.getKey(), entry.getValue());
        }
        return urlBuilder.build().toString();
    }

    public static String addDefaultParamsWithoutSSOWithMovieParams(Context context,
                                                                   String mainUrl, String movieCode, String cinemaID, String city, String sessionID) {
        mainUrl = CJRDefaultRequestParam.getDefaultParamsForProductDetailWithoutSSO(mainUrl,
                context);

        if (CJRSeatCommonUtils.isUserSignedIn(context)) {

            String userId = CJRAppUtility.getUserId(context);

            if (!TextUtils.isEmpty(userId)) {
                mainUrl += ("&custId=" + userId);
            }
        }

        if (!TextUtils.isEmpty(sessionID)) {
            mainUrl += ("&sessionId=" + sessionID);
        }

        if (!TextUtils.isEmpty(movieCode)) {
            mainUrl += ("&movieCode=" + movieCode);
        }

        if (!TextUtils.isEmpty(cinemaID)) {
            mainUrl += ("&cinemaID=" + cinemaID);
        }

//            if (!TextUtils.isEmpty(city)) {
//                mainUrl += ("&city=" + city);
//            }


        return mainUrl;
    }

    public static String addSSOTokenAndAddDefaultParamsWithMovieParams(Context context,
                                                                       String mainUrl, String movieCode, String cinemaID, String city, String sessionID) {
        mainUrl = CJRDefaultRequestParam.getDefaultParamsForProductDetail(mainUrl,
                context);

        if (CJRSeatCommonUtils.isUserSignedIn(context)) {

            String userId = CJRAppUtility.getUserId(context);

            if (!TextUtils.isEmpty(userId)) {
                mainUrl += ("&custId=" + userId);
            }
        }

        if (!TextUtils.isEmpty(sessionID)) {
            mainUrl += ("&sessionId=" + sessionID);
        }

        if (!TextUtils.isEmpty(movieCode)) {
            mainUrl += ("&movieCode=" + movieCode);
        }

        if (!TextUtils.isEmpty(cinemaID)) {
            mainUrl += ("&cinemaID=" + cinemaID);
        }

//            if (!TextUtils.isEmpty(city)) {
//                mainUrl += ("&city=" + city);
//            }


        return mainUrl;
    }

    */
/**
     * Used to add default params in url without ssotoken
     *
     * @param context
     * @param mainUrl
     * @return
     *//*

    public static String addAuthDefaultParams(Context context,
                                              String mainUrl) {
        String authParams = CJRDefaultRequestParam.getAuthDefaultParams(context);
        if (mainUrl.contains("?")) {
            authParams = "&" + authParams.substring(1);
        }
        return mainUrl + authParams;
    }

    public static boolean isValidMobileNo(String MobileNo, Context context,
                                          boolean showDialog) {
        if (TextUtils.isEmpty(MobileNo)) {
            //			if (context != null) {
            //				if (showDialog)
            //					CJRAppUtility.showAlert(
            //							context,
            //							context.getResources().getString(
            //									R.string.message_title),
            //							context.getResources().getString(
            //									R.string.mob_no_empty_error));
            //			}

        } else {
            Pattern p = Pattern.compile(CJRConstants.MOB_NO_REG_EX);
            Matcher m = p.matcher(MobileNo);
            if (m.find()) {
                return true;
            } else {
                //				if (context != null) {
                //					if (showDialog)
                //						CJRAppUtility.showAlert(context, context.getResources()
                //								.getString(R.string.message_title), context
                //								.getResources()
                //								.getString(R.string.mob_no_error));
                //				}
            }
        }
        return false;
    }

    public static boolean isValid5DigitMobileNo(String MobileNo, Context context,
                                                boolean showDialog) {
        if (TextUtils.isEmpty(MobileNo)) {

        } else {
            Pattern p = Pattern.compile(CJRConstants.MOB_NO_REG_EX_5_DIGIT);
            Matcher m = p.matcher(MobileNo);
            if (m.find()) {
                return true;
            } else {

            }
        }
        return false;
    }

    public static boolean isValidEmail(String email, Context context,
                                       boolean showDialog) {
        if (TextUtils.isEmpty(email)) {
            //			if (context != null) {
            //				if (showDialog)
            //					CJRAppUtility.showAlert(
            //							context,
            //							context.getResources().getString(
            //									R.string.message_title),
            //							context.getResources().getString(
            //									R.string.email_empty_error));
            //			}
        } else {
            Pattern p = Pattern.compile(CJRConstants.EMAIL_REG_EX);
            Matcher m = p.matcher(email);
            if (m.find()) {
                return true;
            } else {
                //				if (context != null) {
                //					if (showDialog)
                //						CJRAppUtility
                //								.showAlert(
                //										context,
                //										context.getResources().getString(
                //												R.string.message_title),
                //										context.getResources().getString(
                //												R.string.email_error));
                //				}
            }
        }
        return false;
    }

    public static void setRobotoRegularFont(Context context, TextView view,
                                            int style) {
        if (view == null)
            return;

        if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            view.setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.NORMAL));
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }

    }

    public static void setRobotoRegularFont(Context context, EditText view,
                                            int style) {
        if (view == null)
            return;

        if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            view.setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.NORMAL));
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }

    }

    public static void setRobotoLightFont(Context context, TextView view,
                                          int style) {
        if (view == null)
            return;


        if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            view.setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF_LIGHT, Typeface.NORMAL));
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }
    }

    public static void setRobotoMediumFont(Context context, TextView view,
                                           int style) {
        if (view == null)
            return;


        if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            view.setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF_MEDIUM, Typeface.NORMAL));
        } else {
            view.setTypeface(null, Typeface.NORMAL);
        }


        //view.setTypeface(Typeface.create("sans-serif-condensed", Typeface.NORMAL));

    }

    public static void setRobotoBoldFont(Context context, TextView view,
                                         int style) {
        if (view == null)
            return;


        if (getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            view.setTypeface(Typeface.create(CJRConstants.FONT_FAMILY_SANS_SERIF, Typeface.BOLD));
        } else {
            view.setTypeface(null, Typeface.BOLD);
        }
    }

    public static String getFirstName(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_FIRST_NAME, null);
    }

    public static String getUserImage(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_USER_IMAGE, null);
    }

    public static String getLastName(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_LAST_NAME, null);
    }

    public static String getUserName(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_USER_NAME, null);
    }

    public static int getIsVerifiedMobile(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getInt(CJRConstants.KEY_IS_VERIFIED_MOBILE, 0);
    }

    public static int getIsVerifiedEmail(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getInt(CJRConstants.KEY_IS_VERIFIED_EMAIL, 0);
    }

    public static boolean getIsFetchingUserInfo(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.PREF_KEY_FETCHING_USER_INFO, false);
    }

    public static String getMobile(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_MOBILE, null);
    }

    public static String getEmail(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_EMAIL, null);
    }

    public static String getDob(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_DOB, null);
    }

    public static String getGender(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_GENDER, null);
    }

    public static String getProfileImage(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_PROFILE_IMAGE, null);
    }

    public static String getIrctcUserId(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.IRCTC_USER_ID, null);
    }

    public static String getUserId(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_USER_ID, null);
    }

    public static boolean getIsUserPrime(Context context) {
        if (context != null) {
            CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
            return pref.getBoolean(CJRConstants.KEY_IS_PRIME, false);
        }
        return false;
    }

    public static void setUserPrimeStatus(Context context, boolean status) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            CJRSecureSharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(CJRConstants.KEY_IS_PRIME, status);
            editor.commit();
        }
    }

    public static int getKYCUserStatus(Context context) {
        if (context != null) {
            CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
            return pref.getInt(CJRConstants.KEY_KYC_PRIME_USER_STATUS, -1);
        }
        return -1;
    }

    public static void setKYCUserStatus(Context context, int val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            CJRSecureSharedPreferences.Editor editor = prefs.edit();
            editor.putInt(CJRConstants.KEY_KYC_PRIME_USER_STATUS, val);
            editor.commit();
        }
    }

    */
/**
     * Set the type of kyc been done,i.e., OTP,Manual
     *
     * @param context
     * @param kycType
     *//*

    public static void setKYCType(Context context, String kycType) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            CJRSecureSharedPreferences.Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_KYC_TYPE, kycType);
            editor.commit();
        }
    }

    */
/**
     * Gives type of KYC
     *
     * @param context
     * @return
     *//*

    public static String getKYCType(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_KYC_TYPE, null);
    }


    public static String getUserMerchantType(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_USER_TYPE_SD_MERCHANT, null);
    }

    public static boolean hasUserSignInShown(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_USER_TYPE_SIGN_IN_SHOWN, false);
    }

    */
/*
    Intro screen for travel profile
     *//*

    public static boolean getBusIntroBoolean(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_BUS_INTRO, true);
    }

    public static boolean getTrainIntroBoolean(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_TRAIN_INTRO, true);
    }

    public static boolean getFlightIntroBoolean(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_FLIGHT_INTRO, true);
    }

    public static void setIntroBoolean(Context context, String key) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        CJRSecureSharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, false);
        editor.commit();
    }

    public static String getIndicativePlanDefaultParams(String type,
                                                        String operator, String circle) {
        String params = "";
        if (!TextUtils.isEmpty(type))
            try {
                params = "?type=" + URLEncoder.encode(type, "UTF-8");
                if (!TextUtils.isEmpty(operator))
                    params += "&operator="
                            + URLEncoder.encode(operator, "UTF-8");
                if (!TextUtils.isEmpty(circle))
                    params += "&circle=" + URLEncoder.encode(circle, "UTF-8");
                params += "&client=androidapp";
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        return params;

    }

    public static String getAuthStringParams(Context context) {
        String params = AUTHORIZE_BODY;
        params += AUTHORIZE_CLIENT + VALUE_CLIENT_ID;
        return params;
    }

    public static String getClientId() {
        return VALUE_CLIENT_ID;
    }

    public static String getClientSecret() {
        return VALUE_CLIENT_SECRET_CODE;
    }

    public static String getClientIDSecretParams() {

        return "&" + CJRConstants.CLIENT_ID + "="
                + VALUE_CLIENT_ID + "&"
                + CJRConstants.CLIENT_SECRET_CODE + "="
                + VALUE_CLIENT_SECRET_CODE;
    }

    public static String getAuthorizationValue() {

        return AUTHORIZATION_VALUE;
    }

    public static void setUnreadMsgCount(Context context, int count) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        Editor editor = pref.edit();
        editor.putInt(CJRConstants.KEY_UNREAD_MESSAGE_COUNT, count);
        editor.commit();
    }

    public static boolean isUserSignedIn(Context context) {
        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        String ssoToken = prefs.getString(CJRConstants.SSO_TOKEN, null);
        return ssoToken != null;
    }

    public static void saveUserInfoV2(CJRUserInfoV2 userInfo, Context context) {
        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        Editor editor = prefs.edit();
        CJRUserDefaultInfo defaultInfo = userInfo.getUserDefaultInfo();
        if (defaultInfo == null) {
            return;
        }
        String firstName = defaultInfo.getFirstName();
        String lastName = defaultInfo.getLastName();
        String email = defaultInfo.getEmail();
        String mobile = defaultInfo.getPhone();

        //Creating User name from First and Last name
        String userName = null;
        if (!TextUtils.isEmpty(firstName)) {
            userName = firstName;
            if (!TextUtils.isEmpty(lastName)) {
                userName += " " + lastName;
            }
        }

        String userId = userInfo.getUserId();
        String prevUserId = prefs.getString(CJRConstants.KEY_USER_ID, null);
        String dob = defaultInfo.getDob();
        String profileImage = defaultInfo.getUserPicture();
        String gender = defaultInfo.getGender();
        String dispName = defaultInfo.getDisplayName();
        ArrayList<String> userTypes = userInfo.getUserTypes();

        boolean isUserPrime = defaultInfo.isKyc();

        if (userId != null && userId.trim().length() > 0) {
            editor.putString(CJRConstants.KEY_USER_ID, userId);
            editor.putString(CJRConstants.CUSTOMER_ID_SMS, userId);
            // Set customer id to AppsFlyer SDK
            AppsFlyerLib.getInstance().setCustomerUserId(userId);
        } else {
            editor.putString(CJRConstants.KEY_USER_ID, null);
        }

        if (userInfo.getUserSocialInfoList() != null && userInfo.getUserSocialInfoList().size() > 0) {
            String picture = userInfo.getUserSocialInfoList().get(0).getSocialSiteInfo().getPicture();
            CJRAppUtility.log("PVN", "saving user image url :: " + picture);
            editor.putString(CJRConstants.KEY_USER_IMAGE, picture);
        } else {
            CJRAppUtility.log("PVN", "saving user image url as null ");
            editor.putString(CJRConstants.KEY_USER_IMAGE, null);
        }

        if (userInfo.getPasscode() != null) {
            BankSharedPrefs.setPasscodeSet(context, userInfo.getPasscode().isPassCodeSet());
        }

//        boolean isBankCustomer = false;
//
//        ArrayList<String> userTypeList = userInfo.getUserTypes();
//        for (int i = 0; userTypeList != null && i < userTypeList.size(); i++) {
//
//            if (CJRConstants.BANK_CUSTOMER.equals(userTypeList.get(i))) {
//                isBankCustomer = true;
//                break;
//            }
//        }
//        BankSharedPrefs.setBankCustomer(context, isBankCustomer);

        boolean isSameUser = userId != null && !userId.equalsIgnoreCase(prevUserId);
        if (!(context instanceof AJRMainActivity) || isSameUser) {

            if (firstName != null && firstName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_FIRST_NAME, firstName);
            } else {
                editor.putString(CJRConstants.KEY_FIRST_NAME, null);
            }

            if (lastName != null && lastName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_LAST_NAME, lastName);
            } else {
                editor.putString(CJRConstants.KEY_LAST_NAME, null);
            }

            if (email != null && email.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_EMAIL, email);
            } else {
                editor.putString(CJRConstants.KEY_EMAIL, null);
            }

            if (mobile != null && mobile.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_MOBILE, mobile);
                editor.putString(CJRConstants.KEY_MOBILE_FOR_ROAMING, mobile);
            } else {
                editor.putString(CJRConstants.KEY_MOBILE, null);
            }

            if (userName != null && userName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_USER_NAME, userName);
            } else {
                editor.putString(CJRConstants.KEY_USER_NAME, null);
            }

            if (dob != null && dob.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_DOB, dob);
            } else {
                editor.putString(CJRConstants.KEY_DOB, null);
            }

            if (gender != null && gender.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_GENDER, gender);
            } else {
                editor.putString(CJRConstants.KEY_GENDER, null);
            }
            if (profileImage != null && profileImage.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_PROFILE_IMAGE, profileImage);
            } else {
                editor.putString(CJRConstants.KEY_PROFILE_IMAGE, null);
            }

            if (dispName != null && dispName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_DISPLAY_NAME, dispName);
            } else {
                editor.putString(CJRConstants.KEY_DISPLAY_NAME, null);
            }

            if (userTypes != null) {
                editor.putString(CJRConstants.KEY_USER_TYPE_SD_MERCHANT, CJRConstants.NO_MERCHANT);
                for (int index = 0; index < userTypes.size(); index++) {
                    String userType = userTypes.get(index);
                    if (!TextUtils.isEmpty(userType) && userType.equalsIgnoreCase(CJRConstants.SD_MERCHANT)) {
                        editor.putString(CJRConstants.KEY_USER_TYPE_SD_MERCHANT, userType);
                    }
                }
            } else {
                editor.putString(CJRConstants.KEY_USER_TYPE_SD_MERCHANT, CJRConstants.NO_MERCHANT);
            }

            editor.putBoolean(CJRConstants.KEY_IS_PRIME, isUserPrime);

            editor.putInt(CJRConstants.KEY_IS_VERIFIED_MOBILE, defaultInfo.isPhoneVerificationStatus() ? 1 : 0);
            editor.putBoolean(CJRConstants.PREF_KEY_FETCHING_USER_INFO, false);
            editor.putInt(CJRConstants.KEY_IS_VERIFIED_EMAIL, defaultInfo.isEmailVerificationStatus() ? 1 : 0);
            editor.commit();
        }
        //String apid = PushManager.shared().getAPID();
        //String alias = apid+":"+userId;
        CJRAppUtility.log("tag", "userid:" + userId);
        UAirship.shared().getPushManager().setAlias(userId);
        UAirship.shared().getNamedUser().setId(userId);
    }

    public static void saveUserInfo(CJRUserInfo userInfo, Context context) {
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        Editor editor = prefs.edit();
        //SharedPreferences.Editor editor = prefs.edit();
        String firstName = userInfo.getFirstName();
        String lastName = userInfo.getLastName();
        String email = userInfo.getEmail();
        String mobile = userInfo.getMobile();
        String userName = userInfo.getUserName();
        String userId = userInfo.getId();
        String prevUserId = prefs.getString(CJRConstants.KEY_USER_ID, null);
        String dob = userInfo.getDOB();
        String dispName = userInfo.getDisplayName();
        String profileImage = userInfo.getUserPicture();
        String gender = userInfo.getGender();
        String isUserPrime = userInfo.getIsUserPrime();
        if (userId != null && userId.trim().length() > 0) {
            editor.putString(CJRConstants.KEY_USER_ID, userInfo.getId());
            editor.putString(CJRConstants.CUSTOMER_ID_SMS, userInfo.getId());
        } else {
            editor.putString(CJRConstants.KEY_USER_ID, null);
        }
        if (userInfo.getUserSocialInfoList() != null && userInfo.getUserSocialInfoList().size() > 0) {
            CJRAppUtility.log("PVN", "saving user image url :: " + userInfo.getUserSocialInfoList().get(0).getImageUrl());
            editor.putString(CJRConstants.KEY_USER_IMAGE,
                    userInfo.getUserSocialInfoList().get(0).getImageUrl());
        } else {
            CJRAppUtility.log("PVN", "saving user image url as null ");
            editor.putString(CJRConstants.KEY_USER_IMAGE, null);
        }
        boolean isSameUser = userId != null
                && !userId.equalsIgnoreCase(prevUserId);
        if (!(context instanceof AJRMainActivity) || isSameUser) {

            if (firstName != null && firstName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_FIRST_NAME,
                        userInfo.getFirstName());
            } else {
                editor.putString(CJRConstants.KEY_FIRST_NAME, null);
            }
            if (lastName != null && lastName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_LAST_NAME,
                        userInfo.getLastName());
            } else {
                editor.putString(CJRConstants.KEY_LAST_NAME, null);
            }
            if (email != null && email.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_EMAIL, userInfo.getEmail());
            } else {
                editor.putString(CJRConstants.KEY_EMAIL, null);
            }
            if (mobile != null && mobile.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_MOBILE, userInfo.getMobile());
                editor.putString(CJRConstants.KEY_MOBILE_FOR_ROAMING, userInfo.getMobile());
            } else {
                editor.putString(CJRConstants.KEY_MOBILE, null);
            }
            if (userName != null && userName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_USER_NAME,
                        userInfo.getUserName());
            } else {
                editor.putString(CJRConstants.KEY_USER_NAME, null);
            }
            if (dob != null && dob.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_DOB,
                        userInfo.getDOB());
            } else {
                editor.putString(CJRConstants.KEY_DOB, null);
            }
            if (dispName != null && dispName.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_DISPLAY_NAME, dispName);
            } else {
                editor.putString(CJRConstants.KEY_DISPLAY_NAME, null);
            }
            if (gender != null && gender.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_GENDER,
                        userInfo.getGender());
            } else {
                editor.putString(CJRConstants.KEY_GENDER, null);
            }
            if (profileImage != null && profileImage.trim().length() > 0) {
                editor.putString(CJRConstants.KEY_PROFILE_IMAGE,
                        userInfo.getUserPicture());
            } else {
                editor.putString(CJRConstants.KEY_PROFILE_IMAGE, null);
            }
            if (isUserPrime != null && isUserPrime.trim().length() > 0 && isUserPrime != null && isUserPrime.equalsIgnoreCase("prime")) {
                editor.putBoolean(CJRConstants.KEY_IS_PRIME,
                        true);
            } else {
                editor.putBoolean(CJRConstants.KEY_IS_PRIME,
                        false);
            }

            editor.putInt(CJRConstants.KEY_IS_VERIFIED_MOBILE,
                    userInfo.getIsVerifiedMobile());
            editor.putBoolean(CJRConstants.PREF_KEY_FETCHING_USER_INFO, false);
            editor.putInt(CJRConstants.KEY_IS_VERIFIED_EMAIL,
                    userInfo.getIsVerifiedEmail());
            editor.commit();
        }
        //String apid = PushManager.shared().getAPID();
        //String alias = apid+":"+userId;
        CJRAppUtility.log("tag", "userid:" + userId);
        UAirship.shared().getPushManager().setAlias(userId);
        UAirship.shared().getNamedUser().setId(userId);
    }

    public static CJRUserInfoV2 convertToUserInfoV2(CJRUserInfo userInfo) {
        CJRUserInfoV2 userInfoV2 = new CJRUserInfoV2();
        CJRUserDefaultInfo defaultInfoV2 = new CJRUserDefaultInfo();
        userInfoV2.setUserDefaultInfo(defaultInfoV2);

        userInfoV2.setUserId(userInfo.getId());
        userInfoV2.setMessage(userInfo.getMessage());
        defaultInfoV2.setFirstName(userInfo.getFirstName());
        defaultInfoV2.setLastName(userInfo.getLastName());
        defaultInfoV2.setDisplayName(userInfo.getDisplayName());
        defaultInfoV2.setGender(userInfo.getGender());
        defaultInfoV2.setEmail(userInfo.getEmail());
        defaultInfoV2.setPhone(userInfo.getMobile());
        defaultInfoV2.setDob(userInfo.getDOB());
        defaultInfoV2.setIsKyc(userInfo.getIsUserPrime().equalsIgnoreCase("prime") ? true : false);
        defaultInfoV2.setEmailVerificationStatus(userInfo.getIsVerifiedEmail() == 1 ? true : false);
        defaultInfoV2.setPhoneVerificationStatus(userInfo.getIsVerifiedMobile() == 1 ? true : false);
        defaultInfoV2.setCustomerCreationDate(userInfo.getCreatedAt());
        defaultInfoV2.setCustomerStatus(userInfo.getStatus().equalsIgnoreCase("1") ? "active" : "inactive");//TODO
        defaultInfoV2.setUserPicture(userInfo.getUserPicture());

        return userInfoV2;
    }

    public static CJRUserInfo convertToUserInfo(CJRUserInfoV2 userInfoV2) {
        CJRUserDefaultInfo defaultUser = userInfoV2.getUserDefaultInfo();

        CJRUserInfo userInfo = new CJRUserInfo();
        userInfo.setId(userInfoV2.getUserId());
        userInfo.setMessage((userInfoV2.getMessage()));
        userInfo.setFirstName(defaultUser.getFirstName());
        userInfo.setLastName(defaultUser.getLastName());
        userInfo.setGender(defaultUser.getGender());
        userInfo.setEmail(defaultUser.getEmail());
        userInfo.setMobile(defaultUser.getPhone());
        userInfo.setDOB(defaultUser.getDob());
        userInfo.setIsUserPrime(defaultUser.isKyc() ? "prime" : "regular");
        userInfo.setIsVerifiedEmail(defaultUser.isEmailVerificationStatus() ? 1 : 0);
        userInfo.setmIsVerifiedMobile(defaultUser.isPhoneVerificationStatus() ? 1 : 0);
        userInfo.setCreatedAt(defaultUser.getCustomerCreationDate());
        userInfo.setStatus(defaultUser.getCustomerStatus().equalsIgnoreCase("active") ? "1" : "0");
        userInfo.setUserPicture(defaultUser.getUserPicture());
        userInfo.setDisplayName(defaultUser.getDisplayName());

        return userInfo;
    }

    public static String formatNumber(String number) {
        if (number != null && !number.trim().equalsIgnoreCase("")) {
            NumberFormat formatter = new DecimalFormat("##,##,##,##,###.##");
            float floatNumber = Float.parseFloat(number);
            return formatter.format(floatNumber);
        }
        return "";
    }

    public static String formatNumber(double number) {
        NumberFormat formatter = new DecimalFormat("##,##,##,##,###.##");
        return formatter.format(number);
    }

//    */
/**
//     * Use CJRSessionManager.signout method for common signout code.
//     * @param context
//     *//*

//    @Deprecated
//    public static void clearUserPref(Context context) {
//        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
//        Editor editor = pref.edit();
//        editor.putString(CJRConstants.SSO_TOKEN, null);
//        editor.putString(CJRConstants.KEY_FIRST_NAME, null);
//        editor.putString(CJRConstants.KEY_LAST_NAME, null);
//        editor.putString(CJRConstants.KEY_EMAIL, null);
//        editor.putString(CJRConstants.KEY_MOBILE, null);
//        editor.putString(CJRConstants.KEY_DOB, null);
//        editor.putString(CJRConstants.KEY_GENDER, null);
//        editor.putString(CJRConstants.KEY_USER_IMAGE, null);
//        editor.putString(CJRConstants.KEY_PROFILE_IMAGE, null);
//        editor.putString(CJRConstants.KEY_PAN_NUMBER, null);
//        editor.putString(CJRConstants.KEY_PAN_NUMBER_TIME_STAMP, null);
//        editor.putBoolean(CJRConstants.KEY_PROFILE_PIC_UPDATE, false);
//        editor.putBoolean(CJRConstants.KEY_IS_PRIME, false);
//        editor.putBoolean(CJRConstants.KEY_KYC_USER_STATUS, false);
//        editor.putString(CJRConstants.KEY_ENTITY_MERCHANT_ID, null);
//        editor.putString(CJRConstants.KEY_USER_TYPE_SD_MERCHANT, null);
//        editor.putBoolean(CJRConstants.KEY_ACCEPT_MONEY_BANK_SEL, false);
//        editor.putString(CJRConstants.PREF_MERCHANT_QR_ID, null);
//        editor.putBoolean(CJRConstants.KEY_PAN_ADHAR_VERIFIED, false);
//        editor.commit();
//        try {
//            String selectedFilePath = android.os.Environment.getExternalStorageDirectory().toString() + CJRConstants.USER_PROFILE_PIC_PATH;
//            File file = new File(selectedFilePath);
//            if (file.exists())
//                file.delete();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        CJRFileUtility.deleteFile(context, CJRConstants.FILE_HEADER_FREQUENT_ORDER_LIST);
//    }

    public static String remove(String str, char remove) {
        if (TextUtils.isEmpty(str) || str.indexOf(remove) == -1) {
            return str;
        }
        char[] chars = str.toCharArray();
        int pos = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != remove) {
                chars[pos++] = chars[i];
            }
        }
        return new String(chars, 0, pos);
    }

    public static void handleError(Activity activity, VolleyError error, String targetClassName, Bundle bundle,
                                   boolean isPreProcess) {
        if (activity != null && !activity.isFinishing() && error != null) {
            if (error.getMessage() != null && (error.getMessage().equalsIgnoreCase("401") || error.getMessage().equalsIgnoreCase("410"))) {
                //				executeSignOutApi(activity);
                //				Intent intent = new Intent(activity, AJRAuthActivity.class);
                //				intent.putExtra(CJRConstants.EXTRA_INTENT_SERVER_AUTH_ERROR, true);
                //				if(targetClassName != null) {
                //					intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY, targetClassName);
                //					if(bundle != null) {
                //						intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bundle);
                //					}
                //				}
                //				activity.startActivity(intent);
                //				activity.finish();
                showSessionTimeoutAlert(activity, targetClassName, bundle, error);
            } else {
                String url = error.getFullUrl();
                if (!TextUtils.isEmpty(url)) {
                    if (url.contains(GTMLoader.getInstance(activity).getBaseURLAuth() + "")) {
                        //Auth url
                        String mailId = CJRConstants.AUTH_ERROR_MAIL;
                        CJRAppUtility.reportError(activity, error, mailId);
                    } else if (url.contains(GTMLoader.getInstance(activity).getBaseURLKYC() + "") || url.contains(GTMLoader.getInstance(activity).getBaseURLGoldengate() + "")) {
                        //Kyc
                        String mailId = CJRConstants.KYC_ERROR_MAIL;
                        CJRAppUtility.reportError(activity, error, mailId);
                    } else if (url.contains(GTMLoader.getInstance(activity).getBaseURLWallet() + "")) {
                        //wallet or Passbook
                        String mailId = CJRConstants.WALLET_ERROR_MAIL;
                        CJRAppUtility.reportError(activity, error, mailId);
                    }
                }
            }

        }

    }

    public static void showSessionTimeoutAlert(final Activity activity, final String targetClassName, final Bundle bundle, VolleyError error) {
        if (activity != null && activity.isFinishing() == false) {
            Builder builder = new Builder(activity);
            String title = null;
            String message = null;
            if (error != null) {
                message = error.getAlertMessage();
                title = error.getAlertTitle();
            }
            if (TextUtils.isEmpty(message))
                message = activity.getResources().getString(R.string.message_401_410);
            if (TextUtils.isEmpty(title))
                title = activity.getResources().getString(R.string.title_401_410);
            if (error != null && !TextUtils.isEmpty(error.getUrl())) {
                message = message + "(" + error.getUrl() + " | HTTP " + error.getMessage() + ")";
            }

            String ssoToken = CJRServerUtility.getSSOToken(activity);
            if (!TextUtils.isEmpty(ssoToken)) {
                ssoToken = "/" + ssoToken;
                if (!TextUtils.isEmpty(message) && message.contains(ssoToken)) {
                    message = message.replace(ssoToken, "");
                }
            }
            String cartId = CJRServerUtility.getCartID(activity);
            if (!TextUtils.isEmpty(cartId)) {
                cartId = "/" + cartId;
                if (!TextUtils.isEmpty(message) && message.contains(cartId)) {
                    message = message.replace(cartId, "");
                }
            }
            builder.setTitle(title).setMessage(message).setCancelable(false);
            builder.setPositiveButton(activity.getResources().getString(R.string.ok),
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            CJRSessionManager.signOut(activity, true);
                            Intent intent = new Intent(activity, AJRAuthActivity.class);
                            intent.putExtra(CJRConstants.EXTRA_INTENT_SERVER_AUTH_ERROR, true);
                            if (targetClassName != null) {
                                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY, targetClassName);
                                intent.putExtra(CJRConstants.EXTRA_INTENT_SIGN_IN_WITH_STEP_2, true);
                                if (bundle != null) {
                                    intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bundle);
                                }
                            }
                            intent.putExtra(CJRGTMConstants.GTM_KEY_VERTICAL_NAME, CJRGTMConstants.GTM_VERTICAL_MARKETPLACE);
                            activity.startActivity(intent);
                            if (!(activity instanceof AJRMainActivity)) activity.finish();
                        }
                    });
            builder.show();
        }
    }

//    public static void executeSignOutApi(Context activity) {
//
//        try {
//
//            CJRFileUtility.deleteFile(activity.getApplicationContext(), CJRConstants.FILE_HEADER_FAVORITE_NUMBER_LIST);
//            //log out from twitter
//            if (CJRAppUtility.isNetworkAvailable(activity)) {
//
//
//                String signOutUrl = GTMLoader.getInstance(activity).getSignOutUrl();
//                Map<String, String> header = new HashMap<String, String>();
//                header.put(CJRConstants.AUTHORIZATION, AUTHORIZATION_VALUE);
//
//                if (signOutUrl != null) {
//
//                    String fullUrl = signOutUrl + "/" + CJRServerUtility.getSSOToken(activity);
//                    //CJRAppUtility.log("CJRSlideMenuHelper", "Calling sign out api : " + fullUrl);
//                    CJRVolley.getRequestQueue(activity.getApplicationContext()).add(new CJRGsonPostRequest(fullUrl, null, null,
//                            null, null, header, null,
//                            Request.Method.DELETE));
//                    CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(activity);
//                    Editor editor = pref.edit();
//                    editor.putString(CJRConstants.SSO_TOKEN, null);
//                    editor.putString(CJRConstants.WALLET_SSO_TOKEN, null);
//
//                    editor.putString(WalletSharedPrefs.INSTANCE.USER_DEVICE, null);
//                    editor.putString(WalletSharedPrefs.INSTANCE.VERSION, null);
//                    editor.putString(WalletSharedPrefs.INSTANCE.CLIENT_ID, null);
//                    editor.putLong(WalletSharedPrefs.INSTANCE.TIME_INTERVAL, 60);
//                    editor.putLong(WalletSharedPrefs.INSTANCE.OTP_COUNTER_CACHE, 0);
//                    editor.commit();
//                    //cart info reset. so that old info does not stored if load cart id fails.
//                    CJRServerUtility.setCartCount(activity, 0);
//                    CJRServerUtility.setCartId(activity, null);
//                    CJRJarvisApplication.clearCartDetails();
//                    UAirship.shared().getPushManager().setAlias(null);
//                    //stored user info also cleared.
//
//
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static float getDeviceDensityRatio(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return (float) dm.densityDpi / DisplayMetrics.DENSITY_MEDIUM;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static boolean checkErrorCode(Context inContext, VolleyError inError) {
        String message = null;
        String title = null;
        String errorCode = inError.getMessage();
        if (errorCode.equalsIgnoreCase(CJRConstants.FAILURE_ERROR)) {
            CJRAppUtility.showAlert(inContext, inError.getAlertTitle(), inError.getAlertMessage());
            return true;
        }

        if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
            message = inError.getAlertMessage();
            title = inError.getAlertTitle();
        }
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(message)) {
            if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))) {
                message = inContext.getResources().getString(R.string.message_499);
                title = inContext.getResources().getString(R.string.title_499);
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))) {
                message = inContext.getResources().getString(R.string.message_502);
                title = inContext.getResources().getString(R.string.title_502);
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
                message = inContext.getResources().getString(R.string.message_503);
                title = inContext.getResources().getString(R.string.title_503);
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
                message = inContext.getResources().getString(R.string.message_504);
                title = inContext.getResources().getString(R.string.title_504);
            }

        }
        if (!TextUtils.isEmpty(message)) {
            //error exist
            //message = message + " Do send screenshot of this error to feedback@paytm.com & we'll fix it ASAP.";
            if (!TextUtils.isEmpty(inError.getUrl())) {
                message = message + "(" + inError.getUrl() + " | HTTP " + inError.getMessage() + ")";
            }
            if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
                launchContingency(inContext, message, title);
                return true;
            }
            CJRAppUtility.showAlert(inContext, title, message);
//            showReportErrorDialog(inContext, title, inError, message);
            return true;
        }
        return false;
    }

    public static CJRError getError(Context inContext, VolleyError inError) {
        CJRError error = new CJRError();
        String errorCode = inError.getMessage();
        if (errorCode.equalsIgnoreCase(CJRConstants.FAILURE_ERROR)) {
            error.setTitle(inError.getAlertTitle());
            error.setMessage(inError.getAlertMessage());
            return error;
        }

        if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
            error.setMessage(inError.getAlertMessage());
            error.setTitle(inError.getAlertTitle());
        }
        if (TextUtils.isEmpty(error.getTitle()) || TextUtils.isEmpty(error.getMessage())) {
            if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))) {
                error.setMessage(inContext.getResources().getString(R.string.message_499));
                error.setTitle(inContext.getResources().getString(R.string.title_499));
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))) {
                error.setMessage(inContext.getResources().getString(R.string.message_502));
                error.setTitle(inContext.getResources().getString(R.string.title_502));
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
                error.setMessage(inContext.getResources().getString(R.string.message_503));
                error.setTitle(inContext.getResources().getString(R.string.title_503));
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
                error.setMessage(inContext.getResources().getString(R.string.message_504));
                error.setTitle(inContext.getResources().getString(R.string.title_504));
            }
        }
        if (!TextUtils.isEmpty(error.getMessage())) {
            //error exist
            //message = message + " Do send screenshot of this error to feedback@paytm.com & we'll fix it ASAP.";
            if (!TextUtils.isEmpty(inError.getUrl())) {
                error.setMessage(error.getMessage() + "(" + inError.getUrl() + " | HTTP " + inError.getMessage() + ")");
            }
            if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
                launchContingency(inContext, error.getMessage(), error.getTitle());
                return null;
            }
            return error;
        }
        return null;
    }

    //Defined twice so commenting this method.
//    public static boolean checkErrorCodes(Context inContext, VolleyError inError) {
//        String message = null;
//        String title = null;
//        String errorCode = inError.getMessage();
//
//        if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))
//                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))
//                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))
//                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
//            message = inError.getAlertMessage();
//            title = inError.getAlertTitle();
//        }
//        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(message)) {
//            if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))) {
//                message = inContext.getResources().getString(R.string.message_499);
//                title = inContext.getResources().getString(R.string.title_499);
//            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))) {
//                message = inContext.getResources().getString(R.string.message_502);
//                title = inContext.getResources().getString(R.string.title_502);
//            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
//                message = inContext.getResources().getString(R.string.message_503);
//                title = inContext.getResources().getString(R.string.title_503);
//            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
//                message = inContext.getResources().getString(R.string.message_504);
//                title = inContext.getResources().getString(R.string.title_504);
//            }
//        }
//        if (!TextUtils.isEmpty(message)) {
//            //error exist
//            //message = message + " Do send screenshot of this error to feedback@paytm.com & we'll fix it ASAP.";
//            if (!TextUtils.isEmpty(inError.getUrl())) {
//                message = message + "(" + inError.getUrl() + " | HTTP " + inError.getMessage() + ")";
//            }
//            if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
//                launchContingency(inContext, message, title);
//                return true;
//            }
//            CJRAppUtility.showAlert(inContext, title, message);
//            return true;
//        }
//        return false;
//    }

    private static void launchContingency(final Context context, final String message, final String title) {
        if (context == null) return;
        String url = GTMLoader.getInstance(context).getLeadAPIOnAppLaunchUrl();
        //
        if (url != null) {
            url = CJRAppUtility.addDefaultParamsWithoutSSO(context, url);
            */
/*url = url + CJRDefaultRequestParam.getDefaultParams(
                    context, false);*//*

            String cartId = CJRServerUtility.getCartID(context);
            if (!TextUtils.isEmpty(cartId)) {
                url += "&cart_id=" + cartId;

            }

            HashMap<String, String> headers = new HashMap<>();
            headers = CJRAppUtility.addSSOTokenInHeader(headers, context);

            CJRVolley.getRequestQueue(context).add(
                    new CJRGsonGetRequest(url, new Listener<IJRDataModel>() {
                        @Override
                        public void onResponse(IJRDataModel dataModel) {
                            if (dataModel instanceof CJRContingency) {
                                CJRContingency contingency = (CJRContingency) dataModel;
                                if (contingency != null) {
                                    if (contingency.getStatus()) {
                                        Intent intent = new Intent(context, AJREmbedWebView.class);
                                        intent.putExtra(CJRConstants.URL, contingency.getUrl());
                                        intent.putExtra(CJRConstants.Title, contingency.getMessage());
                                        intent.putExtra(CJRConstants.FROM, CJRConstants.CONTINGENCY);
                                        intent.putExtra(CJRConstants.MAINTENANCE, false);
                                        intent.putExtra(CJRConstants.MAINTAINANCE_ERROR_503, true);
                                        intent.putExtra(CJRConstants.CLOSE, contingency.getClose());
                                        intent.putExtra(CJRConstants.ALERT_TITLE, title);
                                        intent.putExtra(CJRConstants.ALERT_MESSAGE, message);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intent);
                                        ((Activity) context).finish();
                                    } else {
                                        showAlert(context, title, message);
                                    }

                                }
                            }
                        }
                    }, new ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (!TextUtils.isEmpty(error.getMessage())
                                    && error.getMessage().equalsIgnoreCase("503")) {
                                String message = context.getResources().getString(R.string.contingency_503_message);
                                String title = context.getResources().getString(R.string.contingency_503_title);
                                CJRAppUtility.showAlert(context, title, message);

                            }

                        }
                    }, new CJRContingency(), headers));
        } else {
            showAlert(context, title, message);
        }
    }

    public static Intent getIntent(String urlType, Context context, CJRHomePageItem item) {
        Log.i(TAG, "urlType:" + urlType + ",item:" + item);
        if (TextUtils.isEmpty(urlType)) {
            //non-deep link push
            Intent intent = new Intent(context, AJRMainActivity.class);
            intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE,
                    CJRConstants.URL_TYPE_MAIN);
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_HOME_PAGE)) {
            return new Intent(context, AJRSecondaryHome.class);//AJRMainActivity.class
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_SECONDARY_HOME_PAGE)) {
            return new Intent(context, AJRSecondaryHome.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PRODUCT)) {
            return new Intent(context, AJRProductDetail.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_GRID)) {
            return new Intent(context, AJRGridPageContainer.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_PREPAID_1)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_PREPAID_2)) {
            return new Intent(context, AJRRechargeActivity.class);
        }
        if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_POSTPAID_1)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_POSTPAID_2)) {
            return new Intent(context, AJRRechargeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_PREPAID_1)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_PREPAID_2)) {
            return new Intent(context, AJRRechargeActivity.class);
        }
        if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_POSTPAID_1)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_POSTPAID_2)) {
            return new Intent(context, AJRRechargeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DTH_1)) {
            return new Intent(context, AJRRechargeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_TOLLCARD)) {
            return new Intent(context, AJRRechargeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_SEARCH)) {
            Intent intent = new Intent(context, AJRSearchActivity.class);
            intent.putExtra(CJRConstants.EXTRA_INTENT_ITEM, item);
            intent.putExtra(CJRConstants.EXTRA_INTENT_IS_FROM_SEARCH,
                    true);
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_EMBED)) {
            return new Intent(context, AJREmbedWebView.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_RECHARGES)) {
            return new Intent(context, AJRRechargeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_ELECTRICITY)) {
            return new Intent(context, AJRRechargeUtilityActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_GAS)) {
            return new Intent(context, AJRGasActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_LANDLINE)) {
            return new Intent(context, AJRLandlineActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_SMART_LIST)) {
            return new Intent(context, AJRGridPageContainer.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_LIST)) {
            return new Intent(context, AJRGridPageContainer.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_CART)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJRShoppingCartActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRShoppingCartActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_ORDER_SUMMARY)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJROrderSummaryActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJROrderSummaryActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }


        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_HOTEL_ORDER_SUMMARY)) {
            return HotelUtils.getHotelBookingStatusIntent(context, item);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_TRAIN_ORDER_SUMMARY)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJRTrainOrderSummary.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJROrderSummaryActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }


        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_SAVED_CARD) ||
                urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_ADD_CARD)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJRProfileActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRProfileActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }


        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MY_ORDER)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJRItemLevelOrder.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRItemLevelOrder.class.getName());
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PAYTM_CASH_LEDGER)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, PassbookMainActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        PassbookMainActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_BANK_KYC)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, PBKYCRequestInviteActivity.class);
            } else {
                Intent intent = new Intent(context, PBNonKycInfoActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        PBKYCRequestInviteActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PAYTM_P2M_SCAN)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJRQRActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRQRActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PAYTM_ACCEPT_PAYMENT)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AcceptPaymentModeSelectActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AcceptPaymentModeSelectActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DEEPLINK_PASSCODE)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                Intent intent = new Intent(context, AJRSecuritySettings.class);
                intent.putExtra(CJRConstants.URL_TYPE_DEEPLINK_PASSCODE, true);
                return intent;
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRSecuritySettings.class.getName());
                intent.putExtra(CJRConstants.URL_TYPE_DEEPLINK_PASSCODE, true);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_KYC_ADHAR_DEEPLINK)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                Intent intent = new Intent(context, AJRNonKYCDeeplink.class);
                intent.putExtra(CJRConstants.KYC_DEEPLINK_PAN, false);
                return intent;
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRNonKYCDeeplink.class.getName());
                intent.putExtra(CJRConstants.KYC_DEEPLINK_PAN, false);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_KYC_PAN_DEEPLINK)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                Intent intent = new Intent(context, AJRNonKYCDeeplink.class);
                intent.putExtra(CJRConstants.KYC_DEEPLINK_PAN, true);
                return intent;
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRNonKYCDeeplink.class.getName());
                intent.putExtra(CJRConstants.KYC_DEEPLINK_PAN, true);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_CASH_WALLET)
                && CJRWalletConstants.WALLET_TYPE_PAY.equalsIgnoreCase(CJRWalletUtility.getWalletTypeForFeatureType(item.getPushFeatureType()))) {
            String amount = item.getPushCashAdd();
            if (TextUtils.isEmpty(amount)) {
                return new Intent(context, PassbookMainActivity.class);
            } else {
                return new Intent(context, PaySendActivity.class);
            }

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_CASH_WALLET) && CJRConstants.URL_TYPE_PAYTM_REQUEST_MONEY.equalsIgnoreCase(CJRWalletUtility.getWalletTypeForFeatureType(item.getPushFeatureType()))) {
            return new Intent(context, AJRWalletActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_CASH_WALLET)) {
            return new Intent(context, PaySendActivity.class);
        } else if (urlType != null && urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MY_PROFILE)) {
            Intent intent = new Intent(context, AJRMainActivity.class);
            intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE, CJRConstants.URL_TYPE_MY_PROFILE);
            return intent;
        }
        //TODO : uncomment this is profile Section needs to get updated.
        else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PROFILE)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, AJRProfileActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        AJRProfileActivity.class.getName());
                return intent;
            }

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_BUSTICKET)) {
//            return new Intent(context, AJRBusTicketsHomePAge.class);
            return new Intent(context, AJRTravelsHomeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MALL)) {
            Intent intent = new Intent(context, AJRMainActivity.class);
            intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE,
                    CJRConstants.URL_TYPE_MALL);
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_METRO)) {
            return new Intent(context, AJRMetroCardRecharge.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.UTILITY_LOAN_PROVIDER)) {
            return new Intent(context, AJRRechargeUtilityActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.UTILITY_CHALLAN_URL_TYPE)) {
            return new Intent(context, AJRRechargeUtilityActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DMRC)) {
            return new Intent(context, AJRDMRCSourceActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_AE_EMBED)) {
            return new Intent(context, AJREmbedWebView.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_SELLER_STORE)) {
            return new Intent(context, AJRRecyclerBrandStoreActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_HOTELS)) {
            return HotelUtils.getHotelHomeIntent(context);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_HOTEL_RESULTS)) {
            return HotelUtils.getHotelSearchResultsIntent(context);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_WITHDRAW_MONEY)) {
            Intent intent = new Intent(context, MerchantPayActivity.class);
            intent.putExtra(CJRConstants.EXTRA_INTENT_ITEM, item);
            intent.putExtra(CJRConstants.EXTRA_ORIGIN, "pushnotification");
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_WISHLIST)) {
            Intent wishlistIntent = new Intent(context, AJRShoppingCartActivity.class);
            wishlistIntent.putExtra(CJRConstants.EXTRA_INTENT_LOAD_WISHLIST, true);
            wishlistIntent.putExtra(CJRConstants.WISH_LIST_SOURCE, "Account");
            return wishlistIntent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_BRAND_STORE)) {
            return new Intent(context, AJRRecyclerBrandStoreActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UTILITY)) {
            return new Intent(context, AJRRechargeUtilityActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PRIMARY_HOMEPAGE)) {
            Intent intent = new Intent(context, AJRMainActivity.class);
            intent.putExtra(CJRConstants.EXTRA_DEEP_LINK_DATA, item);
            intent.putExtra(CJRConstants.EXTRA_IS_DEEP_LINK_DATA, true);
            intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE,
                    CJRFlavourConstants.URL_TYPE_MAIN);
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_HOTEL_CITY_SEARCH)) {
            return HotelUtils.getHotelSearchIntent(context);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_HOTEL_DETAILS)) { //todo
            return HotelUtils.getHotelDetailIntent(context);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_BUS_SEARCH)) {
            return new Intent(context, AJRBusSearchActivity.class);

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOVIETICKET)) {
            return new Intent(context, AJRMoviesHomePage.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_IMAXPAGE)) {
            return new Intent(context, AJRIMAXCinemasPage.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOVIES_SHOW_TIME)) {
            return new Intent(context, AJRMoviesHomePage.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_CONTACT_US)) {
            if (TextUtils.isEmpty(item.getContactUsIssueParentId()) || TextUtils.isEmpty(item.getContactUsIssueVerticalLabel())) {
                return new Intent(context, AJRCSTContactUs.class);
            } else {
                CJRReplacementReason reasonList = new CJRReplacementReason();
                reasonList.setIssueText(item.getContactUsIssueVerticalLabel());
                try {
                    int parentId = Integer.valueOf(item.getContactUsIssueParentId());
                    reasonList.setId(parentId);
                    IJRDataModel orderObj = null;

                    Intent intent = new Intent(context, AJRCSTOrderIssues.class);
                    intent.putExtra(CJRConstants.INTENT_EXTRA_CST_ORDER_ITEM, orderObj);
                    intent.putExtra(CJRConstants.INTENT_EXTRA_CST_ORDER_REASONS, reasonList);
                    return intent;
                } catch (NumberFormatException numberFormatException) {
                    return new Intent(context, AJRCSTContactUs.class);
                }
            }

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_FLIGHTTICKET)) {
//            return new Intent(context, AJRFlightTicketHomePage.class);
            return new Intent(context, AJRTravelsHomeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_TRAIN)) {
//            return new Intent(context, AJRTrainHomeActivity.class);
            return new Intent(context, AJRTravelsHomeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_EVENTS)) {
            return new Intent(context, AJREventsHomePage.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES)) {
            Intent intent = new Intent(context, AJRMainActivity.class);
            intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE, CJRConstants.URL_TYPE_UPDATES);
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_SIGNIN_PROFILE)) {
            Intent intent = new Intent(context, AJRContactUsSubmit.class);
            intent.putExtra("redirectVertical", "login_signup");
            String utmSource = null;
            if (item != null && !TextUtils.isEmpty(item.getUtmSource())) {
                utmSource = item.getUtmSource();
            }
            intent.putExtra(CJRConstants.UTM_SOURCE, utmSource);
            return intent;

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_AMUSEMENT_PARK) || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_AMPARK_DL)) {
            // TBD
          */
/* if(item!= null  ){
               return new Intent(context, AJRAmParkDetailPage.class);
           }
            else {
               return new Intent(context, AJRAmParkHomePage.class);
           }*//*

            return new Intent(context, AJRAmParkHomePage.class);


        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPGRADE_WALLET)) {

            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                Intent intent = new Intent(context, UpgradeKycActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_ITEM, item);
                return intent;
            } else {
                Intent intent = new Intent(context, UpgraKycInfoActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        UpgraKycInfoActivity.class.getName());
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PAYTM_NEARBY)) {
            if (!TextUtils.isEmpty(CJRServerUtility.getSSOToken(context))) {
                return new Intent(context, NearByMainActivity.class);
            } else {
                Intent intent = new Intent(context, AJRAuthActivity.class);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY,
                        NearByInfoActivity.class.getName());
                Bundle bandle = new Bundle();
                bandle.putSerializable(CJRConstants.EXTRA_INTENT_ITEM, item);
                bandle.putString(CJRConstants.ORIGIN, CJRConstants.ORIGIN_DEEP_LINKING);
                intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_ACTIVITY_BUNDLE, bandle);
                return intent;
            }
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_FREE_MOVIE)) {
            return new Intent(context, AJRMoviesHomePage.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_GIFT_CARD)) {
            return new Intent(context, AJRGiftCardHomePage.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_GOLD)) {
            return new Intent(context, AJRGoldHomeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DIGITAL_GOLD)) {
            return new Intent(context, AJRGoldHomeActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_EXTERNAL) && item != null) {
            String url = item.getURL();

            if (!URLUtil.isValidUrl(url)) {
                return null;
            }

            url = CJRDefaultRequestParam.appendWebUrlParams(context, url, "browser", "1");
            return new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_PAYMENT_BANK)) {
            return new Intent(context, AJRMainActivity.class);
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_CST)) {
            Intent intent = new Intent(context, AJRCSTIssueDetail.class);
            intent.putExtra(CJRConstants.INTENT_EXTRA_ISSUE_TICKET_NUMBER,
                    item.getmTicketId());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_CUSTOM_VIEW)) {
            Intent intent = new Intent(context, AJRFeeds.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_STORY_VIEW)) {
            Intent intent = new Intent(context, AJRStoryFeeds.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_FLIP_VIEW)) {
            Intent intent = new Intent(context, AJRStackCards.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_PAGER_VIEW)) {
            Intent intent = new Intent(context, AJRPagerCards.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_STORY_BLOG_VIEW)) {
            Intent intent = new Intent(context, AJRBlogWebContent.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_HOROSCOPE_DETAIL_VIEW)) {
            Intent intent = new Intent(context, AJRHoroscopeDetailActivity.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_VIDEO_DETAIL_VIEW)) {
            Intent intent = new Intent(context, AJRVideoDetailActivity.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UPDATES_SHOW_COMMENTS)) {
            Intent intent = new Intent(context, AJRComments.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        } else if (urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_FLIGHT_ORDER_SUMMARY)) {
            Intent intent = new Intent(context, NewOrderSummary.class);
            intent.putExtra(CJRConstants.URL, item.getURL());
            return intent;
        }
        return null;
    }

    public static void showDataDisplayError(Context context, String url) {
        if (context == null) return;
        String message = context.getResources().getString(R.string.message_error_data_display);
        if (!TextUtils.isEmpty(url)) message += "(" + url + ")";
        String title = context.getResources().getString(
                R.string.error_data_display);
        CJRAppUtility.showAlert(context, title, message);
    }

    public static CJRError getDataDisplayError(Context context, String url) {
        if (context == null) return null;
        String message = context.getResources().getString(R.string.message_error_data_display);
        if (!TextUtils.isEmpty(url)) message += "(" + url + ")";
        String title = context.getResources().getString(
                R.string.error_data_display);
        CJRError error = new CJRError();
        error.setTitle(title);
        error.setMessage(message);
        return error;
    }

    public static boolean checkRechargeResponseTime(CJRRechargeProductList list) {
        if (list == null) return false;
        Long time = list.getServerResponseTime();
        if (time != null) {
            Long dif = ((new Date().getTime() - time) / 1000) / 60;
            //Recharge data should be stored only for 15 minutes.
            if (dif < 15) return true;

        }
        return false;
    }

    */
/**
     * Create insurance meta data attributes using the @{@link CJRCartProduct}
     *
     * @param context              : @{@link Context} to be used
     * @param rechargeNumber       : rechargeNumber on which recharge is done.
     * @param mainRechargePdtId    : main Product Id being purchased
     * @param rechargePrice        : total price of recharge
     * @param mMasterProduct       : Master product object
     * @param insuranceProductInfo : productInfo @{@link JSONObject}
     * @return
     *//*

    public static String getInsuranceBody(Context context, String rechargeNumber, String mainRechargePdtId, String rechargePrice, CJRCartProduct mMasterProduct, JSONObject insuranceProductInfo) {
        String finalString = "";
        JSONObject requestJSON = new JSONObject();
        try {
            JSONObject rechargeData = new JSONObject();
            rechargeData.put("recharge_number", rechargeNumber);
            rechargeData.put("product_id", mainRechargePdtId);
            rechargeData.put("amount", rechargePrice);
            requestJSON.put("custId", CJRAppUtility.getUserId(context));
            requestJSON.put("rechargeInfo", rechargeData);
            requestJSON.put("productInfo", insuranceProductInfo);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            finalString = requestJSON.toString();
        }
        return finalString;
    }

    public static boolean containsError(CJRRechargeCart rechargeCart, final Context context) {

        ArrayList<CJRCartProduct> cartProductList = rechargeCart.getCart().getCartItems();
        for (CJRCartProduct cartProduct : cartProductList) {
            if (cartProduct.getError() != null) {
                String title = cartProduct.getErrorTitle();
                if (title != null && title.trim().length() > 0) {
                    CJRAppUtility.showAlert(context, title, cartProduct.getError());
                } else {
                    CJRAppUtility.showAlert(context, context.getResources().getString(R.string.unable_to_proceed),
                            cartProduct.getError());
                }
                return true;
            }
        }
        if (rechargeCart.getCart().getError() != null) {
            if (rechargeCart != null && rechargeCart.getCart() != null) {
                String title = rechargeCart.getCart().getErrorTitle();
                title = (title != null && title.trim().length() > 0) ? title : CJRConstants.UTILITY_HIDE_TITLE;
                CJRAppUtility.showAlert(context, title, rechargeCart.getCart().getError());

            } else {
                CJRAppUtility.showAlert(context, context.getResources().getString(R.string.network_error_heading),
                        context.getResources().getString(R.string.network_error_message));
            }
            return true;
        }
        return false;
    }

    public static boolean containsErrorRecharge(CJRRechargeCart rechargeCart, final Context context) {

        ArrayList<CJRCartProduct> cartProductList = rechargeCart.getCart().getCartItems();
        for (CJRCartProduct cartProduct : cartProductList) {
            if (cartProduct.getError() != null) {
                String title = cartProduct.getErrorTitle();
                if (title != null && title.trim().length() > 0) {
                    CJRAppUtility.showAlert(context, title, cartProduct.getError());
                } else {
                    CJRAppUtility.showAlert(context, context.getResources().getString(R.string.error),
                            cartProduct.getError());
                }
                return true;
            }
        }
        if (rechargeCart.getCart().getError() != null) {
            if (rechargeCart != null && rechargeCart.getCart() != null) {
                String title = rechargeCart.getCart().getErrorTitle();
                title = (title != null && title.trim().length() > 0) ? title : CJRConstants.UTILITY_HIDE_TITLE;
                CJRAppUtility.showAlert(context, title, rechargeCart.getCart().getError());

            } else {
                CJRAppUtility.showAlert(context, context.getResources().getString(R.string.network_error_heading),
                        context.getResources().getString(R.string.network_error_message));
            }
            return true;
        }
        return false;
    }

    public static String formatTime(String time, String inputPattern, String outputPattern) {
        try {
            if (inputPattern.equals(CJRConstants.BUS_SEARCH_INPUT_TIME_FORMAT)) {
                if (time.length() != 4) {
                    time = StringUtils.leftPad(time, 4, '0');
                }
            }
            final SimpleDateFormat sdf = new SimpleDateFormat(inputPattern);
            Date dateObj;
            dateObj = sdf.parse(time);
            String resultantTime = new SimpleDateFormat(outputPattern).format(dateObj);
            return resultantTime;
        } catch (Exception e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        }
    }

    public static String formatDateInEnglish(Context mContext, String inputDate, String inputFormat, String outputFormat) {
        try {
            DateFormat originalFormat = null;
            DateFormat targetFormat = null;
            originalFormat = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
            targetFormat = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
            Date dateObject = originalFormat.parse(inputDate);
            String formattedDate = targetFormat.format(dateObject);
            return formattedDate;
        } catch (ParseException e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        } catch (Exception e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        }
    }

    public static String formatDate(Context mContext, String inputDate, String inputFormat, String outputFormat) {
        try {
            Locale appLocale = new Locale(LocaleHelper.getDefaultLanguage());
            DateFormat originalFormat = new SimpleDateFormat(inputFormat, appLocale);
            DateFormat targetFormat = new SimpleDateFormat(outputFormat);
            Date dateObject = originalFormat.parse(inputDate);
            String formattedDate = targetFormat.format(dateObject);
            return formattedDate;
        } catch (ParseException e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        } catch (Exception e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        }
    }

    public static boolean isValidFormat(String checkIndate, String dateFormat) {

        if (checkIndate != null && !TextUtils.isEmpty(checkIndate)) {
            Date date = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                date = sdf.parse(checkIndate);
                if (!checkIndate.equals(sdf.format(date))) {
                    date = null;
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            return date != null;
        }
        return false;
    }

    public static String convert24Hrto12HrViceVersa(String timeValue, String inputFormat, String outputFormat) {

        if(null != timeValue && !TextUtils.isEmpty(timeValue.trim())) {
            try {
                SimpleDateFormat hourSdFObj1 = new SimpleDateFormat(inputFormat);
                SimpleDateFormat hourSdFObj2 = new SimpleDateFormat(outputFormat);
                Date hourDateObj = hourSdFObj1.parse(timeValue);
                return hourSdFObj2.format(hourDateObj);
            } catch (Exception e) {
                return StringUtils.EMPTY;
            }
        }
        return StringUtils.EMPTY;
    }

    public static String formatDateCustom(Context mContext, String inputDate, String inputFormat, String outputFormat) {
        try {
            Locale appLocale = new Locale(LocaleHelper.getDefaultLanguage());
            DateFormat originalFormat = new SimpleDateFormat(inputFormat, appLocale);
            originalFormat.setLenient(true);
            DateFormat targetFormat = new SimpleDateFormat(outputFormat);
            targetFormat.setLenient(true);
            Date dateObject = originalFormat.parse(inputDate);
            String formattedDate = targetFormat.format(dateObject);
            return formattedDate;
        } catch (ParseException e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        } catch (Exception e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            return "";
        }
    }

    public static String convertDateToLocale(Context mContxt, String inputDate, String inputFormat, String outputFormat, long mPreviouslySelectedDate) {
        String dateTimeFormat = outputFormat;
        SimpleDateFormat sdfgmt = new SimpleDateFormat(dateTimeFormat, new Locale(LocaleHelper.getDefaultLanguage(), "IN"));
        return (sdfgmt.format(new Date(mPreviouslySelectedDate)));
    }

    public static String convertDateToLocaleValue(Date inputDate, String inputFormat, String outputFormat) {
       */
/* //String timeOrg = "Mon Apr 18 22:56:10 GMT+05:30 2016";
       depdate = "2017-04-27T06:00:00+00:00"
        String dateTimeFormatOrg = inputFormat;
        SimpleDateFormat sdfgmtOrg = new SimpleDateFormat(dateTimeFormatOrg);
        Date time = null;
        try {
            time = sdfgmtOrg.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }*//*


        String dateTimeFormat = outputFormat;
        SimpleDateFormat sdfgmt = new SimpleDateFormat(dateTimeFormat, new Locale(LocaleHelper.getDefaultLanguage(), "IN"));
        //sdfgmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return (sdfgmt.format(inputDate));
    }

    public static String formatNumber(double price, String outputPattern) {
        try {
            NumberFormat formatter = new DecimalFormat(outputPattern);
            return formatter.format(price);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isGooglePlayServicesAvailable(Context context) {
        try {
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
            return resultCode == ConnectionResult.SUCCESS;
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            return false;
        }
    }

    public static boolean hasGPSDevice(Context context) {
        try {
            final LocationManager mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (mgr == null) {
                return false;
            }

            final List<String> providers = mgr.getAllProviders();
            if (providers == null) {
                return false;
            }

            return providers.contains(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            return false;
        }
    }

    public static String getSignedExpressCheckoutURL(String data, String httpMethod) {
        StringBuffer mData = new StringBuffer();
        String mFinalUrl = null;
        Long secondsTillEpoch;
        if (data != null) {
            secondsTillEpoch = System.currentTimeMillis() / 1000;
            mData.append(httpMethod + "\n" + secondsTillEpoch + "\n" + data);
            CJRAppUtility.log("K-MSG", "" + mData);
            try {
                SecretKeySpec signingKey = new SecretKeySpec(
                        HMAC_CLIENT_SECRET.getBytes(),
                        HMAC_SHA1_ALGORITHM);
                Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
                mac.init(signingKey);
                byte[] rawHmac = mac.doFinal(mData.toString().getBytes());
                String mBase64String = Base64.encodeToString(rawHmac, Base64.NO_WRAP);
                CJRAppUtility.log("K-HMAC", "" + "HEX:" + rawHmac.toString() + "BASE64:" + mBase64String + "ENCOADED URL:" + URLEncoder.encode(mBase64String, "UTF-8"));
                mFinalUrl = data + "&timestamp=" + secondsTillEpoch + "&signature=" + URLEncoder.encode(mBase64String, "UTF-8");
                CJRAppUtility.log("FinalUrl", "" + mFinalUrl);
            } catch (InvalidKeyException e) {
                if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            } catch (IllegalStateException e) {
                if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            } catch (Exception e) {
                if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            }
        }
        return mFinalUrl;
    }

    public static String getOtpFromSms(String from, String smsBody) {
        String receivedOtp = null;
        try {
            if (from != null && smsBody != null && from.toLowerCase().contains("paytm")) {
                Pattern p = Pattern.compile("(?<!\\d)\\d{6}(?!\\d)");
                Matcher m = p.matcher(smsBody);
                while (m.find()) {
                    receivedOtp = m.group();
                    break;
                }
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
        return receivedOtp;
    }

    public static String getAadhaarOtpFromSms(String from, String smsBody) {
        String receivedOtp = null;
        try {
            if (from != null && smsBody != null && (from.toLowerCase().contains("aadhaar") ||
                    from.toLowerCase().contains("adhaar") || from.toLowerCase().contains("aadhar"))) {
                Pattern p = Pattern.compile("(?<!\\d)\\d{6}(?!\\d)");
                Matcher m = p.matcher(smsBody);
                while (m.find()) {
                    receivedOtp = m.group();
                    break;
                }
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
        return receivedOtp;
    }

    public static String getMobileNumberFromMsg(String msg) {
        String mobileNumber = null;
        try {
            if (msg != null) {
                Pattern p = Pattern.compile("(?<!\\d)\\d{10}(?!\\d)");
                Matcher m = p.matcher(msg);
                while (m.find()) {
                    mobileNumber = m.group();
                    break;
                }
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
        return mobileNumber;
    }

    public static String getOtpFromSmsIRCTC(String from, String smsBody) {
        String receivedOtp = null;
        try {
            if (from != null && smsBody != null && from.toLowerCase().contains("irctci")) {
                Pattern p = Pattern.compile("(?<!\\d)\\d{6}(?!\\d)");
                Matcher m = p.matcher(smsBody);
                while (m.find()) {
                    receivedOtp = m.group();
                    break;
                }
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
        return receivedOtp;
    }

    public static String parseOtpFromSmsIRCTC(String from, String smsBody) {
        String receivedOtp = null;
        String passwordText = "password is :";
        try {
            if (from != null && smsBody != null && from.toLowerCase().contains("irctci")) {
                if (!TextUtils.isEmpty(smsBody) && smsBody.toLowerCase().contains(passwordText)) {
                    int index = smsBody.toLowerCase().indexOf(passwordText);
                    index += passwordText.length();
                    if (index < smsBody.length()) {
                        receivedOtp = smsBody.substring(index);
                        if (!TextUtils.isEmpty(receivedOtp)) {
                            receivedOtp = receivedOtp.trim();
                        }
                    }
                }
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
        return receivedOtp;
    }


    public static void collapseAnimation(final View v) {
        try {
            if (CJRAppUtility.getOSVersion() >= Build.VERSION_CODES.HONEYCOMB) {
                final int initialHeight = v.getMeasuredHeight();

                Animation a = new Animation() {
                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                        if (interpolatedTime == 1) {
                            v.setVisibility(View.GONE);
                        } else {
                            v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                            v.requestLayout();
                        }
                    }

                    @Override
                    public boolean willChangeBounds() {
                        return true;
                    }
                };

                // 1dp/ms
                a.setDuration((int) (initialHeight / (v.getContext().getResources().getDisplayMetrics().density * 4)));
                v.startAnimation(a);
            } else {
                v.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
    }

    public static void expandAnimation(final View v) {
        try {
            if (CJRAppUtility.getOSVersion() >= Build.VERSION_CODES.HONEYCOMB) {
                v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                final int targetHeight = v.getMeasuredHeight();

                v.getLayoutParams().height = 0;
                v.setVisibility(View.VISIBLE);
                Animation a = new Animation() {
                    @Override
                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                        v.getLayoutParams().height = interpolatedTime == 1
                                ? LinearLayout.LayoutParams.WRAP_CONTENT
                                : (int) (targetHeight * interpolatedTime);
                        v.requestLayout();
                    }

                    @Override
                    public boolean willChangeBounds() {
                        return true;
                    }
                };

                // 1dp/ms
                a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
                v.startAnimation(a);
            } else {
                v.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        boolean appInstalled = false;

        if (!TextUtils.isEmpty(packageName)) {
            PackageManager pm = context.getPackageManager();
            try {
                pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
                appInstalled = true;
            } catch (PackageManager.NameNotFoundException e) {
                appInstalled = false;
            } catch (Exception e) {
                appInstalled = false;
            }
            return appInstalled;
        }
        return appInstalled;
    }

    public static String getUserAgent(Context context) {
        String userAgent = null;
        try {
            userAgent = new WebView(context).getSettings().getUserAgentString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userAgent;
    }

    public static long convertDurationToSeconds(String inDuration) {
        long time_in_seconds = 0;
        int ihours = 0, iMinute = 0;
        ihours = Integer.parseInt((inDuration.substring(0, inDuration.lastIndexOf("h"))).trim());
        iMinute = Integer.parseInt((inDuration.substring(inDuration.lastIndexOf("h") + 1, inDuration.lastIndexOf("m"))).trim());
        time_in_seconds = ihours * 60 * 60 + iMinute * 60;
        return time_in_seconds;
    }

    public static long convertTimeToSeconds(String inTime) {
        long time_in_seconds = 0;
        int ihours = 0, iMinute = 0;
        ihours = Integer.parseInt((inTime.substring(0, inTime.lastIndexOf(":"))).trim());
        iMinute = Integer.parseInt((inTime.substring(inTime.lastIndexOf(":") + 1, inTime.length())).trim());
        time_in_seconds = ihours * 60 * 60 + iMinute * 60;
        return time_in_seconds;
    }

    public static long getTimeInMilliSeconds(String inDateAndTime) {
        long time_in_millis = 0;
        Date date = null;
        SimpleDateFormat SDF = new SimpleDateFormat(
                CJRConstants.FLIGHT_LIST_SEARCH_INPUT_TIME_FORMAT);
        try {
            date = SDF.parse(inDateAndTime);
        } catch (ParseException e) {
            e.printStackTrace();
        } // from String to date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        time_in_millis = cal.getTimeInMillis();
        return time_in_millis;
    }

    public static String convertSecondsToDuration(long inTimeInSec) {
        String Duration = "";
        int ihours = 0;
        int iminute = 0;
        if (inTimeInSec >= 3600) {
            ihours = (int) inTimeInSec / 3600;
        }
        iminute = ((int) inTimeInSec % 3600) / 60;

        if (ihours > 0)
            Duration = ihours + "h";
        else
            Duration = "0h";
        Duration += iminute + "m";

        return Duration;
    }

    public static String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        try {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null)
                return;

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            int totalHeight = 0;
            View view = null;
            for (int i = 0; i < listAdapter.getCount(); i++) {
                view = listAdapter.getView(i, view, listView);
                if (i == 0)
                    view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, RelativeLayout.LayoutParams.WRAP_CONTENT));

                view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += view.getMeasuredHeight() + 1;
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getTextWidth(TextView txtView) {
        Rect bounds = new Rect();
        Paint textPaint = txtView.getPaint();
        String text = txtView.getText().toString();
        textPaint.setTypeface(txtView.getTypeface());
        float textSize = txtView.getTextSize();
        textPaint.setTextSize(textSize);
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        int width = bounds.width() + (2 * 10);
        return width;
    }

    public static void saveReferrerFromDeeplink(Context context, String referrer) {
        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        Editor editor = prefs.edit();
        if (referrer != null && referrer.trim().length() > 0) {
            editor.putString(CJRConstants.KEY_REFERRER, referrer);
        }
        editor.commit();
    }

    public static String getReferrer(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_REFERRER, null);
    }

    public static long getMilliSecondsFromDate(String inputDate, String inputDatePattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(inputDatePattern);
            sdf.setTimeZone(TimeZone.getTimeZone(CJRConstants.INDIAN_TIME_ZONE));
            Date date = sdf.parse(inputDate);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String getCapitalizedString(String inputString) {
        try {
            if (!TextUtils.isEmpty(inputString)) {
                if (inputString.length() == 1) {
                    return inputString.toUpperCase();
                } else {
                    return inputString.substring(0, 1).toUpperCase() + inputString.substring(1).toLowerCase();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputString;
    }

    public static String getDateFromMiliSeconds(Long timestamp, String format) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(timestamp);
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String modifyDateFormat(String dateString, String srcFormat, String targetFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(srcFormat);
            Date d = sdf.parse(dateString);
            sdf.applyPattern(targetFormat);
            return sdf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void setMovieTimeSlotFlag(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        Editor editor = pref.edit();
        editor.putBoolean(CJRConstants.KEY_MOVIE_TIME_SLOT_FLAG, true);
        editor.commit();
    }

    public static boolean getMovieTimeSlotFlag(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_MOVIE_TIME_SLOT_FLAG, false);
    }

    public static String toCamelCase(String inputString) {
        String result = "";
        if (inputString.length() == 0) {
            return result;
        }
        char firstChar = inputString.charAt(0);
        char firstCharToUpperCase = Character.toUpperCase(firstChar);
        result = result + firstCharToUpperCase;
        for (int i = 1; i < inputString.length(); i++) {
            char currentChar = inputString.charAt(i);
            char previousChar = inputString.charAt(i - 1);
            if (previousChar == ' ') {
                char currentCharToUpperCase = Character.toUpperCase(currentChar);
                result = result + currentCharToUpperCase;
            } else {
                char currentCharToLowerCase = Character.toLowerCase(currentChar);
                result = result + currentCharToLowerCase;
            }
        }
        return result;
    }

    public static int checkDeviceDensity(Context context) {
        int density = context.getResources().getDisplayMetrics().densityDpi;
        int deviceResolution = 4;
        switch (density) {
            case DisplayMetrics.DENSITY_MEDIUM:
            case DisplayMetrics.DENSITY_LOW:
                deviceResolution = 1;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                deviceResolution = 2;
                break;
            case DisplayMetrics.DENSITY_280:
            case DisplayMetrics.DENSITY_XHIGH:
                deviceResolution = 3;
                break;
            case DisplayMetrics.DENSITY_360:
            case DisplayMetrics.DENSITY_400:
            case DisplayMetrics.DENSITY_420:
            case DisplayMetrics.DENSITY_XXHIGH:
                deviceResolution = 4;
                break;
            case DisplayMetrics.DENSITY_560:
            case DisplayMetrics.DENSITY_XXXHIGH:
                deviceResolution = 5;
                break;
        }
        return deviceResolution;
    }

    public static String getShowTime(String time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date dateObj;
            dateObj = sdf.parse(time);

            SimpleDateFormat sdfOutput = new SimpleDateFormat("hh:mm a");
            //			sdfOutput.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
            String resultantTime = sdfOutput.format(dateObj);

            return resultantTime;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //This method takes the input time in UTC and outputs in IST
    public static String getShowDateTime(String time) {

        String timeStampPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        SimpleDateFormat dateFormat = new SimpleDateFormat(timeStampPattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = dateFormat.parse(time);
        } catch (ParseException e) {
            PaytmLogs.e(TAG, "Error Parsing TimeString", e);
        }

        String resultantTime = null;
        if (date != null) {
            dateFormat = new SimpleDateFormat("EEE d MMM yyyy, hh:mm a");
            dateFormat.setTimeZone(TimeZone.getDefault());
            resultantTime = dateFormat.format(date);
        }

        return resultantTime;

    }

    public static String getShowDate(String time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date dateObj;
            dateObj = sdf.parse(time);

            SimpleDateFormat sdfOutput = new SimpleDateFormat("EEE d MMM yyyy");
            //			sdfOutput.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
            String resultantDate = sdfOutput.format(dateObj);

            return resultantDate;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSlot(String inTimeSlot) {

        String[] split = inTimeSlot.trim().replace(":", ".").split(" ");

        float time = Float.valueOf(split[0]);
        String am_pm = split[1];

        boolean is12am = (am_pm.equalsIgnoreCase("AM") && (int) time == 12);

        if ((am_pm.equalsIgnoreCase("PM") && (int) time != 12) || is12am) {

            time += 12;
        }

        if (time >= 6 && time < 12) {
            return "Morning";
        } else if (time >= 12 && time < 15) {
            return "Afternoon";
        } else if (time >= 15 && time < 18) {
            return "Late Afternoon";
        } else if (time >= 18 && time < 21) {
            return "Evening";
        }

        return "Night";
    }

    public static Bitmap blurRenderScript(Bitmap bitmap, int radius, Context context) {
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                try {
                    bitmap = RGB565toARGB888(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Bitmap newBitmap = Bitmap.createBitmap(
                        bitmap.getWidth(), bitmap.getHeight(),
                        Bitmap.Config.ARGB_8888);

                RenderScript renderScript = RenderScript.create(context);

                Allocation blurInput = Allocation.createFromBitmap(renderScript, bitmap);
                Allocation blurOutput = Allocation.createFromBitmap(renderScript, newBitmap);

                ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                        Element.U8_4(renderScript));
                blur.setInput(blurInput);
                blur.setRadius(radius); // radius must be 0 < r <= 25
                blur.forEach(blurOutput);

                blurOutput.copyTo(newBitmap);
                renderScript.destroy();

                return newBitmap;
            } catch (Exception e) {
            }
        }
        return bitmap;

    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    public static String getDayOfMonthSuffix(int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String getAdvertisingID(Context context) {
        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        String adid = prefs.getString(CJRConstants.KEY_ADVERTISING_ID, null);
        return adid;
    }

    public static Map<String, String> postRequestHeaderForV2() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        return headers;
    }

    public static String postRequestBodyForV2(Context context, String prevPage, String currPage) {
        return createRequestBodyForV2(context, prevPage, currPage, null);
    }

    public static String createRequestBodyForV2(Context context, String prevPage, String currPage, CJRSelectCityModel selectedLocation) {

        JSONObject jsonUserId = new JSONObject();
        JSONObject jsonContext = new JSONObject();
        JSONObject jsonGeo = new JSONObject();
        JSONObject jsonDevice = new JSONObject();
        JSONObject mjsonData = new JSONObject();
        JSONObject jsonTracking = new JSONObject();

        String mlat, mlong;

        if(selectedLocation != null && !TextUtils.isEmpty(selectedLocation.getLatitude()) && !TextUtils.isEmpty(selectedLocation.getLongitude())) {
            mlat = selectedLocation.getLatitude();
            mlong = selectedLocation.getLongitude();
        } else {
            mlat = CJRAppCommonUtility.getLatitudeFromPref(context);
            mlong = CJRAppCommonUtility.getLongitudeFromPref(context);
        }

        String manufacturer = android.os.Build.MANUFACTURER;
        String model = android.os.Build.MODEL;
        String osVersion = Build.VERSION.RELEASE;
        String connectionType = CJRAppCommonUtility.getNetworkType(context);
        String hardwareVersion = android.os.Build.DEVICE;
        String userAgent = System.getProperty("http.agent");
        String carrier = CJRAppCommonUtility.getDeviceSimCarrier(context);
        String advertisingID = getAdvertisingID(context);
        String device_uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String ipv4 = getIp(context);
        String ipV6 = "";

        String appVersion = null;
        try {
            appVersion = context.getPackageManager().getPackageInfo(context
                    .getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
        }

        long userID = 0L;
        if (CJRAppUtility.isUserSignedIn(context)) {
            String userIdString = getUserId(context);

            if (!TextUtils.isEmpty(userIdString) && TextUtils.isDigitsOnly(userIdString)) {
                try {
                    userID = Long.parseLong(userIdString);
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                }

            }
        }

        try {
            jsonUserId.put("user_id", userID);
            if (advertisingID != null) jsonUserId.put("ga_id", advertisingID);
            jsonUserId.put("experiment_id", CJRConstants.EXPERIMENT_ID);

            if (mlat != null) jsonGeo.put("lat", mlat);
            if (mlong != null) jsonGeo.put("long", mlong);

            if (userAgent != null) jsonDevice.put("ua", userAgent);
            if (ipv4 != null) jsonDevice.put("ip", ipv4);
            if (ipV6 != null) jsonDevice.put("ip_v6", ipV6);
            if (manufacturer != null) jsonDevice.put("make", manufacturer);
            if (model != null) jsonDevice.put("model", model);
            if (osVersion != null) jsonDevice.put("osv", osVersion);
            if (hardwareVersion != null) jsonDevice.put("hwv", hardwareVersion);
            if (connectionType != null) jsonDevice.put("connection_type", connectionType);
            if (carrier != null) jsonDevice.put("carrier", carrier);
            if (advertisingID != null) jsonDevice.put("aaid", advertisingID);
            if (device_uuid != null) jsonDevice.put("browser_uuid", device_uuid);
            jsonDevice.put("device_type", "PHONE");
            jsonDevice.put("os", "Android");

            jsonContext.put("user", jsonUserId);
            if (appVersion != null) jsonContext.put("version", appVersion);
            jsonContext.put("geo", jsonGeo);
            jsonContext.put("device", jsonDevice);
            jsonContext.put("channel", "APP");

            if (prevPage != null) jsonTracking.put("prev_page", prevPage);
            if (currPage != null) jsonTracking.put("current_page", currPage);
            jsonTracking.put("referer_ui_element", "");

            mjsonData.put("context", jsonContext);
            mjsonData.put("tracking", jsonTracking);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return mjsonData.toString();
    }

    public static String getIp(Context context) {
        */
/*WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        String ipAddress = Formatter.formatIpAddress(wifiMan.getConnectionInfo().getIpAddress());
		return ipAddress;*//*


        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i(TAG, "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(TAG, ex.toString());
        }
        return null;
    }

    public static String getContainerInstanceId(CJRHomePageLayoutV2 pageLayout) {

        String containerInstanceID = "";
        if (pageLayout != null && pageLayout.getDatasources() != null && pageLayout.getDatasources().size() > 0)
            containerInstanceID = pageLayout.getDatasources().get(0).getmContainerInstanceID();
        else containerInstanceID = "";

        return containerInstanceID;
    }

    public static boolean isFinishActivitiesOptionEnabled(Context context) {
        int result;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
            result = Settings.System.getInt(context.getContentResolver(), Settings.Global
                    .ALWAYS_FINISH_ACTIVITIES, 0);
        } else {
            result = Settings.Global.getInt(context.getContentResolver(), Settings.System
                    .ALWAYS_FINISH_ACTIVITIES, 0);
        }
        return result == 1;
    }

    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toLowerCase().toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();

    }

    public static CJRFrequentOrder getMaxOrderByDate(ArrayList<CJRFrequentOrder> frequentOrderList) {
        CJRFrequentOrder maxOrder = null;
        try {
            if (frequentOrderList != null) {
                maxOrder = Collections.max(frequentOrderList, new DateComparator());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return maxOrder;
    }

    public static int getHour(Context context) {

        inTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        deviceID = CJRAppCommonUtility.getUniqueDeviceId(context, inTelephonyManager);

        String hour = null;

        if (deviceID != null && deviceID.length() >= 5) hour = deviceID.substring(0, 5);

        int imei = 0;
        if (hour != null && !hour.isEmpty() && TextUtils.isDigitsOnly(hour)) {
            imei = Integer.parseInt(hour);
        } else {
            imei = 86400 % 24;
        }
        return imei % 24;

    }

    public static int getMinute() {


        String minute = null;

        if (deviceID != null && deviceID.length() >= 10) minute = deviceID.substring(5, 10);

        int imei = 0;
        if (minute != null && !minute.isEmpty() && TextUtils.isDigitsOnly(minute)) {
            imei = Integer.parseInt(minute);
        } else {
            imei = 86400 % 60;
        }
        return imei % 60;
    }

    public static long getAlarmSetTimeinMiliSecond(Context context) {

        TelephonyManager inTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String deviceID = CJRAppCommonUtility.getUniqueDeviceId(context, inTelephonyManager);

        String second = null;
        if (deviceID != null) second = deviceID;

        long imei = 0, alarmTime = 0;
        if (!TextUtils.isEmpty(second) && TextUtils.isDigitsOnly(second)) {
            imei = Long.parseLong(second);
            alarmTime = imei % 86400;
            alarmTime = alarmTime * 1000;
        } else {
            Random rand = new Random();
            alarmTime = rand.nextInt(86400);
            alarmTime = alarmTime * 1000;
        }
        return alarmTime;

    }

    //TODO(Bheemesh). Please add SHOW_DONT_KEEP_ACTIVITIES_WARNING_ALERT
    public static boolean showFinishActivitiesOptionsEnabledWarning(Context context) {
        boolean isDontKeepActivitiesWarningEnable = GTMLoader.getInstance(context)
                .isDontKeepActivitiesWarningEnabled();
        if (isDontKeepActivitiesWarningEnable && !CJRConstants.isDontKeepActivitiesWarningShown &&
                isFinishActivitiesOptionEnabled(context)) {
            CJRConstants.isDontKeepActivitiesWarningShown = true;
            return true;
        }
        return false;
    }

    //TODO(Ramachandran) moving flightSearchActivity to utils for multiple place require
    public static ArrayList<CJRFlightSearchTabItem> getTabItems(Context mContext) {
        Resources resources = mContext.getResources();
        ArrayList<CJRFlightSearchTabItem> tabItems = new ArrayList<CJRFlightSearchTabItem>();


        //Departure
        CJRFlightSearchTabItem timeTabItem = new CJRFlightSearchTabItem();
        timeTabItem.setTitle(resources.getString(R.string.train_departure_tab));
        timeTabItem.setSortByFlight(CJRConstants.KEY_FLIGHT_SEARCH_SORT_BY_DEPARTURE_TIME);
        timeTabItem.setOrderByFlight(CJRConstants.KEY_FLIGHT_SEARCH_ASCENDING_ORDER);
        tabItems.add(timeTabItem);

        //duration
        CJRFlightSearchTabItem durationTabItem = new CJRFlightSearchTabItem();
        durationTabItem.setTitle(resources.getString(R.string.duration));
        durationTabItem.setSortByFlight(CJRConstants.KEY_FLIGHT_SEARCH_SORT_BY_DURATION);
        durationTabItem.setOrderByFlight(CJRConstants.KEY_FLIGHT_SEARCH_ASCENDING_ORDER);
        tabItems.add(durationTabItem);

        //price
        CJRFlightSearchTabItem priceTabItem = new CJRFlightSearchTabItem();
        priceTabItem.setTitle(resources.getString(R.string.price));
        priceTabItem.setSortByFlight(CJRConstants.KEY_FLIGHT_SEARCH_SORT_BY_FARE);
        priceTabItem.setOrderByFlight(CJRConstants.KEY_FLIGHT_SEARCH_ASCENDING_ORDER);
        tabItems.add(priceTabItem);


        return tabItems;
    }

    public static boolean isFromRechargeSection(String urlType) {
        return urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_PREPAID_1) || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_POSTPAID_1)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_PREPAID_1) || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_POSTPAID_1)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_PREPAID_2) || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_MOBILE_POSTPAID_2)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_PREPAID_2) || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DATACARD_POSTPAID_2)
                || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_DTH_1) || urlType.equalsIgnoreCase(CJRConstants.URL_TYPE_UTILITY);

    }

    public static String retrieveCategoryId(CJRItem item) {

        String categoryId = "";

        try {
            if (item != null && !TextUtils.isEmpty(item.getURL())) {

                String url = item.getURL();
                int startIndex = url.lastIndexOf("/");
                if (startIndex != -1) {
                    int lastIndex = url.indexOf("?", startIndex + 1);

                    if (lastIndex == -1) {
                        lastIndex = url.length();
                    }
                    categoryId = url.substring(startIndex + 1, lastIndex);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return categoryId;

    }

    public static String getIrctcRegistredId(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.IRCTC_REGISTERED_USER_ID, null);
    }

    public static void refreshAppToPreventLocaleIssue(Context context) {
        Intent intent = new Intent();
        intent.setClassName(context.getApplicationContext().getPackageName(), AJRMainActivity.class.getName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(CJRConstants.CLEAR_MOBILE_DATA, true);
        intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE, CJRConstants.URL_TYPE_FEATURED);
        intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_POSITION, 0);
        intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE, CJRConstants.URL_TYPE_MAIN);
        context.startActivity(intent);
    }

    */
/**
     * method used to clear the train recent searched data from preferences
     *//*

    public static void clearRecentSearchedData(Context context) {
        SharedPreferences sharedPrefs;
        try {
            sharedPrefs = context.getSharedPreferences(CJRTrainConstants.TRAIN_MOST_PREF_KEY, 0);
            if (sharedPrefs != null) {
                SharedPreferences.Editor editor = sharedPrefs.edit();
                if (editor != null) {
                    editor.remove(CJRTrainConstants.RECENT_SEARCH_TAB_PREF_KEY);
                    editor.remove(CJRTrainConstants.TRAIN_SOURCE_SEARCHED_CITY_PREF_KEY);
                    editor.remove(CJRTrainConstants.TRAIN_DESTINATION_SEARCHED_CITY_PREF_KEY);
                    editor.apply();
                }

            }
        } catch (Exception e) {

        }
    }

    public static String calenderFormatDate(Activity mContext, String inputDate, String inputFormat, String outputFormat) {
        try {
            Locale appLocale = new Locale(LocaleHelper.getDefaultLanguage());
            DateFormat originalFormat = new SimpleDateFormat(inputFormat, appLocale);
            DateFormat targetFormat = new SimpleDateFormat(outputFormat);
            Date dateObject = originalFormat.parse(inputDate);
            String formattedDate = targetFormat.format(dateObject);
            return formattedDate;
        } catch (ParseException e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            refreshAppToUpdateLocale(mContext);
            return "";
        } catch (Exception e) {
            if (CJRAppCommonUtility.isDebug) {
                e.printStackTrace();
            }
            refreshAppToUpdateLocale(mContext);
            return "";
        }
    }

    public static void refreshAppToUpdateLocale(Activity context) {
        if (context == null) return;
        String language = LocaleHelper.getLanguage(context.getApplicationContext());
        LocaleHelper.setLocale(context.getApplicationContext(), language);
        Intent intent = new Intent();
        intent.setClassName(context.getApplicationContext().getPackageName(), AJRMainActivity.class.getName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(CJRConstants.CLEAR_MOBILE_DATA, true);
        intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE, CJRConstants.URL_TYPE_FEATURED);
        intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_POSITION, 0);
        intent.putExtra(CJRConstants.EXTRA_INTENT_RESULTANT_FRAGMENT_TYPE, CJRConstants.URL_TYPE_MAIN);
        context.startActivity(intent);
        context.finish();
    }

    public static boolean isPANUpdateRequired(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        long savedTime = pref.getLong(CJRConstants.KEY_PAN_NUMBER_TIME_STAMP, 0);

        return savedTime < System.currentTimeMillis();
    }

    public static void setPanTimestamp(Context context) {

        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        Editor editor = prefs.edit();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 7);

        editor.putLong(CJRConstants.KEY_PAN_NUMBER_TIME_STAMP, calendar.getTimeInMillis());
        editor.commit();
    }

    public static String getPanNumber(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_PAN_NUMBER, null);
    }

    public static void setPanNumber(Context context, String pan) {
        CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
        Editor editor = prefs.edit();

        if (pan != null) {
            editor.putString(CJRConstants.KEY_PAN_NUMBER, pan);
        } else {
            editor.putString(CJRConstants.KEY_PAN_NUMBER, null);
        }
        editor.commit();
        setPanTimestamp(context);

    }

    public static boolean isProfileUpdated(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
//if true dont update else update
        return pref.getBoolean(CJRConstants.KEY_PROFILE_PIC_UPDATE, false);
    }

    public static void setProfileUpdated(Context context, Boolean val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
//if update dont update else update
            Editor editor = prefs.edit();
            editor.putBoolean(CJRConstants.KEY_PROFILE_PIC_UPDATE, val);
            editor.commit();
        }

    }

    public static String convertingToHtmlTags(String s) {
        StringBuilder builder = new StringBuilder();
        boolean previousWasASpace = false;
        for (char c : s.toCharArray()) {
            if (c == ' ') {
                if (previousWasASpace) {
                    builder.append("&nbsp;");
                    previousWasASpace = false;
                    continue;
                }
                previousWasASpace = true;
            } else {
                previousWasASpace = false;
            }
            switch (c) {
                case '<':
                    builder.append("&lt;");
                    break;
                case '>':
                    builder.append("&gt;");
                    break;
                case '&':
                    builder.append("&amp;");
                    break;
                case '"':
                    builder.append("&quot;");
                    break;
                case '\n':
                    builder.append("<br>");
                    break;
                // We need Tab support here, because we print StackTraces as HTML
                case '\t':
                    builder.append("&nbsp; &nbsp; &nbsp;");
                    break;
                default:
                    if (c < 128) {
                        builder.append(c);
                    } else {
                        builder.append("&#").append((int) c).append(";");
                    }
            }
        }
        return builder.toString();
    }

    public static void setPatternLockSession() {
        WalletSharedPrefs.INSTANCE.setLockPatternSession(false);
    }

    public static void setSecPrmoptShownInSession() {
        WalletSharedPrefs.INSTANCE.setSecPromptShownInSession(false);
    }

    public static void setMerchantID(Context context, String key) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_ENTITY_MERCHANT_ID, key);
            editor.commit();
        }

    }

    public static String getMerchantID(Context context) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            return prefs.getString(CJRConstants.KEY_ENTITY_MERCHANT_ID, null);
        }
        return null;
    }

    public static void setAcceptMoneyBankSelected(Context context, Boolean key) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putBoolean(CJRConstants.KEY_ACCEPT_MONEY_BANK_SEL, key);
            editor.commit();
        }
    }

    public static Boolean getAcceptMoneyBankSelected(Context context) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            return prefs.getBoolean(CJRConstants.KEY_ACCEPT_MONEY_BANK_SEL, false);
        }
        return false;
    }

    */
/**
     * update amount type face with different fornt after decimal and before decimal
     *
     * @param amount
     * @param txt
     *//*

    public static void updateAmount(String amount, TextView txt) {
        Typeface font = null;
        Typeface font2 = null;
        try {
            if (txt.getTypeface() == null || (txt.getTypeface() != null)) {
                font = Typeface.create("sans-serif-medium", Typeface.BOLD);
            }

            if (txt.getTypeface() == null || (txt.getTypeface() != null)) {
                font2 = Typeface.create("sans-serif-light", Typeface.NORMAL);
            }


            SpannableStringBuilder SS = new SpannableStringBuilder(amount);
            String amountarray[] = amount.split("\\.");
            if (amountarray == null || amountarray.length < 2) {
                return;
            }
            int beforedeimal = amountarray[0].length();
            int totalStringLength = amount.length();
            SS.setSpan(new CustomTypefaceSpan("", font), 0, beforedeimal, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan("", font2), beforedeimal, totalStringLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            txt.setText(SS);
        } catch (Exception e) {
            // In case of exception does not set type face.
            e.printStackTrace();
        }
    }

    public static String formatDate(String currentDate, String curentdateformat, String targetDateFormat) {
        String formattedDate = currentDate;
        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(curentdateformat, Locale.ENGLISH);
            SimpleDateFormat targetFormat = new SimpleDateFormat(targetDateFormat);
            Date date = originalFormat.parse(currentDate);
            currentDate = targetFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentDate;
    }

    public static void setBankCustomerStatus(Context context, String status) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            CJRSecureSharedPreferences.Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_BANK_CUSTOMER_STATUS, status);
            editor.commit();
        }

    }

    public static String getBankCustomerStatus(Context context) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            return prefs.getString(CJRConstants.KEY_BANK_CUSTOMER_STATUS, null);
        }
        return null;
    }

    private static String getMailBody(String errorUrl, String errorCode, Context context) {
        String deviceManufacturer = android.os.Build.MANUFACTURER;
        String deviceName = android.os.Build.MODEL;
        String version = Build.VERSION.RELEASE;
        String appVersion = null;
        if (context != null) {
            try {
                appVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        String mailBody = context.getString(R.string.error_reporting_mail_body, deviceManufacturer, deviceName, version, appVersion, errorUrl, errorCode);
        return mailBody;
    }

    public static void sendErrorMail(String errorUrl, String errorCode, Context context, String errorReportingAddress) {
        String mailBody = getMailBody(errorUrl, errorCode, context);
        Intent intent = new Intent(Intent.ACTION_SEND);

        String mailAddress = "error@paytm.com";
        if (errorReportingAddress != null) {
            mailAddress = errorReportingAddress;
        }
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mailAddress});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Something went wrong");
        intent.putExtra(Intent.EXTRA_TEXT, mailBody);

        intent.setType("message/rfc822");

        context.startActivity(Intent.createChooser(intent, "Select Email Sending App :"));
    }

    public static void showReportErrorDialog(final Context context, final VolleyError inError, final String errorReportingAddress) {
        if (context == null || inError == null) return;

        String title = context.getString(R.string.error_dialog_title);
        String message = context.getString(R.string.error_dialog_message);

        String ssoToken = CJRServerUtility.getSSOToken(context);
        if (!TextUtils.isEmpty(ssoToken)) {
            ssoToken = "/" + ssoToken;
            if (message.contains(ssoToken)) {
                message = message.replace(ssoToken, "");
            }
        }
        String cartId = CJRServerUtility.getCartID(context);
        if (!TextUtils.isEmpty(cartId)) {
            cartId = "/" + cartId;
            if (message.contains(cartId)) {
                message = message.replace(cartId, "");
            }
        }
        CustomDialog.showReportErrorDialog(context, title, message, new CustomDialog.OnDialogDismissListener() {
            @Override
            public void onDialogDismissed() {
                sendErrorMail(inError.getUrl(), inError.getMessage(), context, errorReportingAddress);
            }
        });
    }

    */
/**
     * @param t   String to insert every 'num' characters
     * @param s   String to format
     * @param num Group size
     * @return
     * @brief Insert arbitrary string at regular interval into another string
     *//*

    public static String addPadding(String t, String s, int num) {
        StringBuilder retVal;

        if (null == s || 0 >= num) {
            return "";
        }

        if (s.length() <= num) {
            //String to small, do nothing
            return s;
        }

        retVal = new StringBuilder(s);

        for (int i = retVal.length(); i > 0; i -= num) {
            retVal.insert(i, t);
        }
        return retVal.toString();
    }

    public static void showProgressDialog(Context context, String message) {
        if (context == null)
            return;
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = new ProgressDialog(context);

        try {
            // mProgressDialog.setIcon(R.drawable.paytm_logo);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage(message);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        } catch (IllegalArgumentException e) {
            if (net.one97.paytm.common.utility.CJRAppCommonUtility.isDebug) e.printStackTrace();
            */
/*
             * When we dismiss dialog and finish the activity, sometimes
			 * activity get finished before dialog successfully dismisses. So
			 * that dialog is no longer attached to the view of destroyed
			 * activity.
			 *//*

        } catch (Exception e) {
            */
/*
             * When we dismiss dialog and finish the activity, sometimes
			 * activity get finished before dialog successfully dismisses. So
			 * that dialog is no longer attached to the view of destroyed
			 * activity.
			 *//*

        }
    }

    public static void removeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public static void displayLocationSettingsRequest(final Context context, final int requestId) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            if (context instanceof AJREventsHomePage) {
                                status.startResolutionForResult(((AJREventsHomePage) context), requestId);
                            } else if (context instanceof AJRAmParkHomePage) {
                                status.startResolutionForResult(((AJRAmParkHomePage) context), requestId);
                            } else if (context instanceof AJRLocationSelectionActivity) {
                                status.startResolutionForResult(((AJRLocationSelectionActivity) context), requestId);
                            } else if (context instanceof AJRlocationSelectionDialog) {
                                status.startResolutionForResult(((AJRlocationSelectionDialog) context), requestId);
                            }
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    */
/**
     * This method is used to calculate months between two given dates
     * If difference is 8 months 1 days it will return 9 months.
     *
     * @param from Start date
     * @param to   End date
     * @return Number of months between two given dates or -1 if From date is after To date
     *//*

    public static int monthsBetween(Date from, Date to) {
        if (to.before(from)) {
            return -1;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(from);
        int months = 0;
        while (cal.getTime().before(to)) {
            cal.add(Calendar.MONTH, 1);
            months++;
        }
        return months;
    }

    */
/**
     * This method is used to calculate total days between two given dates
     *
     * @param from Start Date
     * @param to   End Date
     * @return Number of days between two given dates or -1 if From date is after To date
     *//*

    public static int daysBetween(Date from, Date to) {
        if (to.before(from)) {
            return -1;
        }
        long diff = to.getTime() - from.getTime();
        int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        return days + 1;
    }

    public static boolean reportError(Context inContext, VolleyError inError, String errorReportingAddress) {
        String message = null;
        String title = null;
        String errorCode = inError.getMessage();
        if (errorCode.equalsIgnoreCase(CJRConstants.FAILURE_ERROR)) {
            if ((inError != null) && (inError.getAlertTitle() != null) && (inError.getAlertMessage() != null)) {
                CJRAppUtility.showAlert(inContext, inError.getAlertTitle(), inError.getAlertMessage());
                return true;
            }
            return false;
        }

        if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))
                || errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
            message = inError.getAlertMessage();
            title = inError.getAlertTitle();
        }
        if (TextUtils.isEmpty(title) || TextUtils.isEmpty(message)) {
            if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_499))) {
                message = inContext.getResources().getString(R.string.message_499);
                title = inContext.getResources().getString(R.string.title_499);
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_502))) {
                message = inContext.getResources().getString(R.string.message_502);
                title = inContext.getResources().getString(R.string.title_502);
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
                message = inContext.getResources().getString(R.string.message_503);
                title = inContext.getResources().getString(R.string.title_503);
            } else if (inError.getMessage().equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_504))) {
                message = inContext.getResources().getString(R.string.message_504);
                title = inContext.getResources().getString(R.string.title_504);
            }

        }
        if (!TextUtils.isEmpty(message)) {
            if (errorCode.equalsIgnoreCase(String.valueOf(CJRConstants.ERROR_CODE_503))) {
                launchContingency(inContext, message, title);
                return true;
            }
            showReportErrorDialog(inContext, inError, errorReportingAddress);
            return true;
        }
        return false;
    }

    public static String getAppDPI() {
        return CJRJarvisApplication.getAppContext().getString(R.string.dpi);
    }

    public static Drawable getDrawableBasedOnSDK(int drawableId, Context context) {
        Drawable drawable = null;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable = context.getResources().getDrawable(drawableId, context.getTheme());
        } else {
            drawable = context.getResources().getDrawable(drawableId);
        }
        return drawable;
    }

    public static String getDisplayName(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_DISPLAY_NAME, null);
    }

    public static String getAdhaarNo(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KYC_AADHAR, null);
    }

    public static void setAdhaarNo(Context context, String adhar) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KYC_AADHAR, adhar);
            editor.commit();
        }
    }

    public static String getPanNo(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KYC_PAN, null);
    }

    public static void setPanNo(Context context, String pan) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KYC_PAN, pan);
            editor.commit();
        }
    }

    public static Boolean isAdhaarVerified(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_IS_ADHAR_VERIFIED, false);
    }

    public static void setAdhaarVerified(Context context, Boolean val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putBoolean(CJRConstants.KEY_IS_ADHAR_VERIFIED, val);
            editor.commit();
        }
    }

    public static Boolean isPanVerified(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getBoolean(CJRConstants.KEY_IS_PAN_VERIFIED, false);
    }

    public static void setPanVerified(Context context, Boolean val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putBoolean(CJRConstants.KEY_IS_PAN_VERIFIED, val);
            editor.commit();
        }
    }

    public static String getKYCFirstName(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_KYC_FIRST_NAME, null);
    }

    public static void setKYCFirstName(Context context, String val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_KYC_FIRST_NAME, val);
            editor.commit();
        }
    }

    public static String getKYCLastName(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_KYC_LAST_NAME, null);
    }

    public static void setKYCLastName(Context context, String val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_KYC_LAST_NAME, val);
            editor.commit();
        }
    }

    public static String getKYCGender(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_KYC_GENDER, null);
    }

    public static void setKYCGender(Context context, String val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_KYC_GENDER, val);
            editor.commit();
        }
    }

    public static String getKYCDOB(Context context) {
        CJRSecureSharedPreferences pref = new CJRSecureSharedPreferences(context);
        return pref.getString(CJRConstants.KEY_KYC_DOB, null);
    }

    public static void setKYCDOB(Context context, String val) {
        if (context != null) {
            CJRSecureSharedPreferences prefs = new CJRSecureSharedPreferences(context);
            Editor editor = prefs.edit();
            editor.putString(CJRConstants.KEY_KYC_DOB, val);
            editor.commit();
        }
    }

    public static String getProfilePicParentPath(Context context) {
        if (context != null) {
            String path = CJRConstants.USER_PROFILE_PIC_BASE_PATH + context.getPackageName();
            return path;
        }
        return null;
    }

    public static String addDefaultParamsWithoutSSO(Context context, String mainUrl) {
        return CJRDefaultRequestParam.getDefaultParamsForProductDetailWithoutSSO(mainUrl, context);
    }

    public static Date convertIntoDateObj(String currentDate, String curentdateformat) {

        try {
            SimpleDateFormat originalFormat = new SimpleDateFormat(curentdateformat, Locale.ENGLISH);
            return originalFormat.parse(currentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getDate(Date dateObj) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateObj);

        return cal.get(Calendar.DATE);
    }

    public static String getMonth(Date dateObj, int longOrShort) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateObj);

        return cal.getDisplayName(Calendar.MONTH, longOrShort, Locale.getDefault());
    }

    */
/**
     * Method to check valid card number
     *
     * @param ccNumber {@link String}
     *//*

    public static boolean luhnCheck(String ccNumber) {
        int sum = 0;
        boolean alternate = false;
        for (int i = ccNumber.length() - 1; i >= 0; i--) {
            try {
                int n = Integer.parseInt(ccNumber.substring(i, i + 1));
                if (alternate) {
                    n *= 2;
                    if (n > 9) {
                        n = (n % 10) + 1;
                    }
                }
                sum += n;
                alternate = !alternate;
            } catch (Exception e) {
                return false;
            }

        }
        return (sum % 10 == 0);
    }

    public static String addCustomerIdToUrl(Context context, String url) {
        if (!TextUtils.isEmpty(url)) {
            if (CJRAppUtility.isUserSignedIn(context)) {
                StringBuilder str = new StringBuilder(url);
                if (url.contains("?")) {
                    str.append("&");
                } else {
                    str.append("?");
                }
                str.append("customer_id=" + getUserId(context));
                url = str.toString();
            }
        }

        return url;
    }

    public static HashMap<String, String> addSSOTokenInHeader(HashMap<String, String> headers, Context context) {
        headers.put(CJRConstants.KEY_SSO_TOKEN_NEW, CJRServerCommonUtility.getSSOToken(context));
        return headers;
    }

    public static boolean openOSWebClient(Context context, String url) {

        boolean handleExternal = false;

        if (!TextUtils.isEmpty(url)) {

            Uri uri = Uri.parse(url);
            String url_open = uri.getQueryParameter("url_open");
            if ((!TextUtils.isEmpty(url_open)) && (url_open.equalsIgnoreCase("ext_browser"))) {

                handleExternal = true;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(browserIntent);
            }
        }
        return handleExternal;
    }

}

*/
